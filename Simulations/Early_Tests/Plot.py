#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.interpolate as si


def load_files(dir):
    '''Loads the coordinates.npz, T.npy, ne.npy and v.npy files from a given directory.'''
    directory = dir

    coords = np.load(directory+'/coordinates.npz')
    T = np.load(directory+'/t.npy')
    ne = np.load(directory+'/ne.npy')
    v = np.load(directory+'/v.npy')

    return coords, T, ne, v


def plot_old(coords, data):
    '''Old plotting function that should not be used because it is slow'''
    plt.xlabel('length along the loop s')
    plt.ylabel('time')
    plt.pcolormesh(coords['s'], coords['t'], data)
    plt.show()


def reshape_simu(t, s, data, target_shape):
    new_nt, new_ns = target_shape
    new_t = np.linspace(t.min(), t.max(), new_nt)
    new_s = np.linspace(s.min(), s.max(), new_ns)
    new_points = np.array(np.meshgrid(new_t, new_s)).T
    new_data = si.interpn((t, s), data, new_points)
    return new_t, new_s, new_data


def plot_new(t, s, data):
    tmin = t.min()/3600  # in hours
    tmax = t.max()/3600  # in hours
    smin = s.min()
    smax = s.max()
    datamin = data.min()
    datamax = data.max()

    fig, ax = plt.subplots()

    img = ax.imshow(data, origin='lower', extent=[smin, smax, tmin, tmax], aspect=smax/tmax)
    ax.set_xlabel('Length along the loop [Mm]')
    ax.set_ylabel('Time [h]')
    ax.set_title('Temperature')

    fig.colorbar(img)
    plt.show()

def plot_multiple(t, s, data, rows, columns):
    for i in range(len(t)):
        tmin_i = t[i].min()/3600  # in hours
        tmax_i = t[i].max() / 3600  # in hours
        if i == 0:
            tmin = tmin_i
            tmax = tmax_i
        else:
            tmin = min(tmin, tmin_i)
            tmax = max(tmax, tmax_i)

    for i in range(len(s)):
        smin_i = s[i].min()
        smax_i = s[i].max()
        if i == 0:
            smin = smin_i
            smax = smax_i
        else:
            smin = min(smin, smin_i)
            smax = max(smax, smax_i)

    for i in range(len(data)):
        datamin_i = data[i].min()
        datamax_i = data[i].max()
        if i == 0:
            datamin = datamin_i
            datamax = datamax_i
        else:
            datamin = min(datamin, datamin_i)
            datamax = max(datamax, datamax_i)

    fig, axes = plt.subplots(rows, columns, sharex=True, sharey=True)

    i = 0
    for ax in axes.flat:
        img = ax.imshow(data[i], origin='lower', extent=[smin, smax, tmin, tmax], aspect=smax/tmax)
        i = i+1

    cbar = fig.colorbar(img, ax=axes.ravel().tolist())
    cbar.set_label('Temperature [MK]')
    return fig, axes




if __name__ == '__main__':
    coords0, T0, ne0, v0 = load_files('Test_0')
    coords1, T1, ne1, v1 = load_files('Test_1')
    coords2, T2, ne2, v2 = load_files('Test_2')
    coords3, T3, ne3, v3 = load_files('Test_3')

    # plot_old(coords, T)

    t0, s0, data0 = reshape_simu(coords0['t'], coords0['s'], T0, [500, 500])
    t1, s1, data1 = reshape_simu(coords1['t'], coords1['s'], T1, [500, 500])
    t2, s2, data2 = reshape_simu(coords2['t'], coords2['s'], T2, [500, 500])
    t3, s3, data3 = reshape_simu(coords3['t'], coords3['s'], T3, [500, 500])

    t = (t0, t1, t2, t3)
    s = (s0, s1, s2, s3)
    data = (data0, data1, data2, data3)

    rows = 2
    columns = 2
    fig, axes1 = plot_multiple(t, s, data, rows, columns)

    axes1[0, 0].set_title(r'$\lambda_{1} = 0.02L, \lambda_{2} = 0.06L$')
    axes1[0, 1].set_title(r'$\lambda_{1} = 0.04L, \lambda_{2} = 0.06L$')
    axes1[1, 0].set_title(r'$\lambda_{1} = 0.06L, \lambda_{2} = 0.06L$')
    axes1[1, 1].set_title(r'$\lambda_{1} = 0.08L, \lambda_{2} = 0.06L$')

    fig.text(0.5, 0.04, 'Position along the loop [Mm]', ha='center', va='center')
    fig.text(0.06, 0.5, 'Time [h]', ha='center', va='center', rotation='vertical')

    plt.subplots_adjust(right=0.725, wspace=0.25, hspace=0.25)

    # plt.savefig('early_test-circular-500x500.svg')
    # plt.savefig('early_test-circular-500x500.png')
    plt.show()


