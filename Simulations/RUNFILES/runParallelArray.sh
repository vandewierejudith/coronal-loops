#!/bin/sh
#SBATCH --job-name=Geom1dot0_test2
#SBATCH --account=ivsusers
#SBATCH --time=1200
#SBATCH --output=./std/stdout.%A-%a.log
#SBATCH --error=./std/stderr.%A-%a.log
#SBATCH --mem=6144
#SBATCH --partition=normal
#SBATCH --qos=normal

#SBATCH --array=1-100%24

lambda1=(7.34 14.68 22.02 29.36 36.7 44.04 51.38 58.72 66.06 73.4)
lambda1Name=(0dot02 0dot04 0dot06 0dot08 0dot10 0dot12 0dot14 0dot16 0dot18 0dot20)
lambda2=(7.34 14.68 22.02 29.36 36.7 44.04 51.38 58.72 66.06 73.4)
lambda2Name=(0dot02 0dot04 0dot06 0dot08 0dot10 0dot12 0dot14 0dot16 0dot18 0dot20)

npoints=10000
simtime=1
geometryFile="geometry1.dat"

j=$((($SLURM_ARRAY_TASK_ID - 1)/10))
i=$((($SLURM_ARRAY_TASK_ID - 1)%10))
/usr/bin/time -v srun --mem=256 --output=./out${j}${i}.log --error=./err${j}${i}.log ./mhd1_loop_runner.py Geom_0dot5_Lambda1_${lambda1Name[${i}]}L_Lambda2_${lambda2Name[${j}]}L --geometry-file ./${geometryFile} --n-points ${npoints} --tmax ${simtime} --lambda1 ${lambda1[${i}]} --lambda2 ${lambda2[${j}]}
mv out${j}${i}.log Geom_0dot5_Lambda1_${lambda1Name[${i}]}L_Lambda2_${lambda2Name[${j}]}L
mv err${j}${i}.log Geom_0dot5_Lambda1_${lambda1Name[${i}]}L_Lambda2_${lambda2Name[${j}]}L
