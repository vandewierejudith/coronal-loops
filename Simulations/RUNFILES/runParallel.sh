#!/bin/sh
#SBATCH --job-name=Geom1
#SBATCH --account=ivsusers
#SBATCH --time=1200
#SBATCH --output=./stdout.log
#SBATCH --error=./stderr.log
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=24
#SBATCH --mem=6144
#SBATCH --partition=normal
#SBATCH --qos=normal

lambda1=(7.34 14.68 22.02 29.36 36.7 44.04 51.38 58.72 66.06 73.4)
lambda1Name=(0dot02 0dot04 0dot06 0dot08 0dot10 0dot12 0dot14 0dot16 0dot18 0dot20)
lambda2=(7.34 14.68 22.02 29.36 36.7 44.04 51.38 58.72 66.06 73.4)
lambda2Name=(0dot02 0dot04 0dot06 0dot08 0dot10 0dot12 0dot14 0dot16 0dot18 0dot20)

for j in ${!lambda2[*]}
do
for i in ${!lambda1[*]}
do
/usr/bin/time -v srun --ntasks=1 --mem=256 --output=./out${j}${i}.log --error=./err${j}${i}.log ./mhd1_loop_runner.py Geom_1dot0_Lambda1_${lambda1Name[${i}]}L_Lambda2_${lambda2Name[${j}]}L --geometry-file ./geometry1.dat --n-points 10000 --tmax 72 --lambda1 ${lambda1[${i}]} --lambda2 ${lambda2[${j}]} &
done
done
wait

for j in ${!lambda2[*]}
do
for i in ${!lambda1[*]}
do
mv out${j}${i}.log Geom_1dot0_Lambda1_${lambda1Name[${i}]}L_Lambda2_${lambda2Name[${j}]}L
mv err${j}${i}.log Geom_1dot0_Lambda1_${lambda1Name[${i}]}L_Lambda2_${lambda2Name[${j}]}L
done
done