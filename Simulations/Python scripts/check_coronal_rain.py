import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.interpolate as si
import os

def load_files(dir):
    '''Loads the coordinates.npz, T.npy, ne.npy and v.npy files from a given directory.'''
    directory = dir

    coords = np.load(directory+'/coordinates.npz')
    T = np.load(directory+'/t.npy')
    ne = np.load(directory+'/ne.npy')
    v = np.load(directory+'/v.npy')

    return coords, T, ne, v



if __name__ == '__main__':

    # load and save files correctly
    # ----------
    coords_dict = {}
    T_dict = {}
    ne_dict = {}
    v_dict = {}

    path = "D:/Documents/1_Unif_HDD/Master_Astrophysics/Research Project/Simulations/Analysation/Geometry0dot75"
    os.chdir(path)
    directories = [name for name in os.listdir(".") if os.path.isdir(name)]

    t = ()
    s = ()
    data = ()

    for i in range(len(directories)):
        # load all data
        coords_dict["coords{0}".format(i)], \
        T_dict["T{0}".format(i)], \
        ne_dict["ne{0}".format(i)], \
        v_dict["v{0}".format(i)] = load_files(directories[i])

        # put data in tuple for plot function
        t = t + (coords_dict["coords{0}".format(i)]['t'],)
        s = s + (coords_dict["coords{0}".format(i)]['s'],)
        data = data + (T_dict['T{0}'.format(i)],)
    # ----------

    # Actual new code for analysis
    
    t_sliced = ()
    data_sliced = ()
    hours = 10  # cutoff time, only data after this time will be checked
    for i in range(len(directories)):
        # cut begin of the simulation
        # ---------------------------
        timeindex = np.argmax(t[i] > hours*3600)  # Find index of time that is at 10h
        t_sliced = t[i][timeindex:]  # slice time array so it only contains 10 - 72h
        data_sliced = data[i][timeindex:]  # slice data array so it contains data between 10 - 72h

        # Check for cycles
        # ----------------
            # Here is some more code needed to check for periodic behaviour with fourrier transform things
            # ...

        # Check for coronal rain
        # ----------------------
            # Here some more code is needed to check if the temperature drops enough to form coronal rain (T < 0.5MK)
            # ...
