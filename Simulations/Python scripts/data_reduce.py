#!/usr/bin/env python3
import os
import numpy as np
import scipy.interpolate as si


def reshape_simu(t, s, data, target_shape):

    new_nt, new_ns = target_shape
    new_t = np.linspace(t.min(), t.max(), new_nt)
    new_s = np.linspace(s.min(), s.max(), new_ns)
    new_points = np.array(np.meshgrid(new_t, new_s)).T
    new_data = si.interpn((t, s), data, new_points)
    return new_t, new_s, new_data


def dat_to_npy(file_in, run_dir):

    filename = os.path.join(run_dir, file_in)

    if os.path.isfile(filename):  # check if file does exist
        with open(filename) as f:
            f.readline()  # number of dimensions (skip)
            ns = int(f.readline())  # number of points along the loop
            nt = int(f.readline())  # number of time snapshots
            f.readline()  # whether scales are included (skip)
            data = []
            for line in f.readlines():  # read all remaining lines
                data += line.strip().split()
        data = np.array(data, dtype=float)  # convert strings floats

        s = data[:ns]  # coordinate along the loop (first ns values)
        t = data[ns:ns+nt]  # snapshot time (next nt values)
        data = data[ns+nt:]  # data (last ns*nt values)

        data = data.reshape((nt, ns))
        t_new, s_new, data_new = reshape_simu(t, s, data, [500, 500])

        file = file_in[:len(file_in)-3]  # prepare output filename
        file_out = os.path.join(run_dir, (file+'npy'))  # prepare output path

        np.save(file_out, data_new)  # save .npy file
        os.remove(filename)  # Remove .dat file

        coords_out = os.path.join(run_dir, 'coordinates.npz')  # prepare coordinate.npz output
        if not(os.path.isfile(coords_out)):  # check if file does not exists
            np.savez(coords_out, s=s_new, t=t_new)  # Save Coordinates

    return


if __name__ == '__main__':
    # Reshape, Resave and Delete files
    directory = 'D:/Documents/1_Unif_HDD/Master_Astrophysics/Research Project/coronal-loops/Simulations/Crashed/Geom_1dot25_Lambda1_0dot14L_Lambda2_0dot16L'
    dat_to_npy('ne.dat', directory)
    dat_to_npy('t.dat', directory)
    dat_to_npy('v.dat', directory)
