#!/usr/bin/env python3

import argparse
import os
import subprocess

import jinja2

import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as si

class MASNorm:
    ''' Normalization units used in MHD1_loop '''
    length = 6.96e10  # [cm]
    time = 1445.87  # [s]
    velocity = 481.3711  # [km/s]
    electron_density = 1e8  # [/cm^3]
    mass_density = 1.6726e-16  # [g/cm^3]
    pressure = 0.3875717  # [dyn/cm^2]
    temperature = 2.807067e7  # [K]
    magnetic_field = 2.2068908  # [Gauss]
    electric_current_density = 2.523259e-7  # [A/m^2]
    electric_current = 1.222307e11  # [A]
    resistivity = 0.0467796  # [s]
    resistivity_over_mu0 = 3.3503427e8  # [km^2/s]
    volumetric_heating_rate = 2.6805e-4  # [erg/cm^3/s]
    heat_flux = 1.865628e7  # [erg/cm^2/s]
    magnetic_energy = 1.306710e32  # [erg]
    kinetic_energy = 1.306710e32  # [erg]


def get_parsed_args():
    ''' Configure and run MHD1_loop '''
    parser = argparse.ArgumentParser(
        description='Run MHD1_loop with a subset of arguments')
    parser.add_argument(
        'run_dir',
        help="directory to run MHD1_loop in")
    parser.add_argument(
        '--mhd1-loop-exec',
        default='./mhd1_loop',
        help=("Path to MHD1_loop executable "
              "(default: ./mhd1_loop)"))
    parser.add_argument(
        '--template-file',
        default='mhd1_loop.dat.j2',
        help=("MHD1_loop template configuration "
              "(default: mhd1_loop.dat.j2)"))
    parser.add_argument(
        '--tmax',
        type=float,
        default=72,
        help=("maximum time to integrate until [hours] "
              "(default: 72h)"))
    parser.add_argument(
        '--n-points',
        type=int,
        default=10000,
        help=("number of mesh points to use for the loop length coordinate "
              "(default: 10000)"))
    parser.add_argument(
        '--geometry-file',
        default='geometry.dat',
        help=("flux tube geometry file name "
              "(default: assume a semi-circular flux tube)"))
    parser.add_argument(
        '--h0',
        type=float,
        default=1e-6,
        help=("uniform heating value [erg cm-3 s-1] "
              "(default: 1e-6)"))
    parser.add_argument(
        '--h1',
        type=float,
        default=1.28e-3,
        help=("stratified heating value [erg cm-3 s-1] "
              "(default: 1.28e-3)"))
    parser.add_argument(
        '--lambda1',
        type=float,
        default=25.68,
        help=("heating scale height at s=0 [Mm] "
              "(default: 25.68)"))
    parser.add_argument(
        '--lambda2',
        type=float,
        default=25.68,
        help=("heating scale height at s=L [Mm] "
              "(default: 25.68)"))
    parser.add_argument(
        '--delta',
        type=float,
        default=5,
        help=("heating offset "
              "(default: 5)"))
    parser.add_argument(
        '--heating-input-file',
        default=' ',
        help=("heating to add from an input file [normalized units] "
              "(default: do not add)"))
    return parser.parse_args()


def args_to_params(args):
    ''' Convert command line arguments to MHD1_loop config parameters '''
    return dict(
        tmax=args.tmax * 3600 / MASNorm.time,
        n_points=args.n_points,
        geometry_file=args.geometry_file,
        h0=args.h0,
        h1=args.h1,
        lambda1=args.lambda1 * 1e8 / MASNorm.length,
        lambda2=args.lambda2 * 1e8 / MASNorm.length,
        delta=args.delta * 1e8 / MASNorm.length,
        heating_input_file=args.heating_input_file,
        )


def reshape_simu(t, s, data, target_shape):

    new_nt, new_ns = target_shape
    new_t = np.linspace(t.min(), t.max(), new_nt)
    new_s = np.linspace(s.min(), s.max(), new_ns)
    new_points = np.array(np.meshgrid(new_t, new_s)).T
    new_data = si.interpn((t, s), data, new_points)
    return new_t, new_s, new_data


def dat_to_npy(file_in):
    
    filename = os.path.join(args.run_dir, file_in)
    with open(filename) as f:
        f.readline()  # number of dimensions (skip)
        ns = int(f.readline())  # number of points along the loop
        nt = int(f.readline())  # number of time snapshots
        f.readline()  # whether scales are included (skip)
        data = []
        for line in f.readlines():  # read all remaining lines
            data += line.strip().split()
    data = np.array(data, dtype=float)  # convert strings floats

    s = data[:ns]  # coordinate along the loop (first ns values)
    t = data[ns:ns+nt]  # snapshot time (next nt values)
    data = data[ns+nt:]  # data (last ns*nt values)

    data = data.reshape((nt, ns))
    t_new, s_new, data_new = reshape_simu(t, s, data, [500, 500])    

    file = file_in[:len(file_in)-3] # prepare output filename 
    file_out = os.path.join(args.run_dir,(file+'npy')) # prepare output path
    
    np.save(file_out, data_new) # save .npy file
    os.remove(filename) # Remove .dat file
    
    coords_out = os.path.join(args.run_dir,'coordinates.npz') # prepare coordinate.npz output
    if not(os.path.isfile(coords_out)): #check if file does not exists
        np.savez(coords_out, s=s_new, t=t_new) # Save Coordinates

    return

if __name__ == '__main__':

    # Parse command line arguments
    args = get_parsed_args()

    # Create directory in which MHD1_loop will be run
    os.makedirs(args.run_dir)

    # Link geometry.dat file to run dir
    if args.geometry_file != ' ':
        os.symlink(os.path.abspath(args.geometry_file),
                   os.path.join(args.run_dir, 'geometry.dat'))
        args.geometry_file = 'geometry.dat'

    # Transform args to MHD1_loop config parameters
    params = args_to_params(args)

    # Create mhd1_loop.dat configuration in run dir
    with open(args.template_file) as f:
        template = jinja2.Template(f.read(), undefined=jinja2.StrictUndefined)
    with open(os.path.join(args.run_dir, 'mhd1_loop.dat'), 'w') as f:
        f.write(template.render(**params))

    # Run MHD1_loop
    mhd1_loop_exec = os.path.abspath(args.mhd1_loop_exec)
    subprocess.run([mhd1_loop_exec], check=True, cwd=args.run_dir)
    
    # Reshape, Resave and Delete files
    dat_to_npy('ne.dat')
    dat_to_npy('t.dat')
    dat_to_npy('v.dat')