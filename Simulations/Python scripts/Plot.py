#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.interpolate as si
import os


def load_files(dir):
    '''Loads the coordinates.npz, T.npy, ne.npy and v.npy files from a given directory.'''
    directory = dir

    coords = np.load(directory+'/coordinates.npz')
    T = np.load(directory+'/t.npy')
    ne = np.load(directory+'/ne.npy')
    v = np.load(directory+'/v.npy')

    return coords, T, ne, v


def plot_old(coords, data):
    '''Old plotting function that should not be used because it is slow'''
    plt.xlabel('length along the loop s')
    plt.ylabel('time')
    plt.pcolormesh(coords['s'], coords['t'], data)
    plt.show()


def reshape_simu(t, s, data, target_shape):
    '''Reshapes the data into an nxm array. Only needed if not already done during the simulation'''
    new_nt, new_ns = target_shape
    new_t = np.linspace(t.min(), t.max(), new_nt)
    new_s = np.linspace(s.min(), s.max(), new_ns)
    new_points = np.array(np.meshgrid(new_t, new_s)).T
    new_data = si.interpn((t, s), data, new_points)
    return new_t, new_s, new_data


def plot_single(t, s, data, title='Temperature', cmap='viridis'):
    tmin = t.min()/3600  # in hours
    tmax = t.max()/3600  # in hours
    smin = s.min()
    smax = s.max()
    datamin = data.min()
    datamax = data.max()

    fig, ax = plt.subplots()

    colormap = plt.get_cmap(cmap)

    img = ax.imshow(data, origin='lower', extent=[smin, smax, tmin, tmax], aspect=smax/tmax, cmap=colormap)
    ax.set_xlabel('Length along the loop [Mm]')
    ax.set_ylabel('Time [h]')
    ax.set_title(title)

    cbar = fig.colorbar(img)
    cbar.set_label('Temperature [MK]')


def plot_multiple(t, s, data, rows, columns, cmap='viridis'):
    for i in range(len(t)):
        tmin_i = t[i].min()#/3600  # in hours
        tmax_i = t[i].max()#/3600  # in hours
        if i == 0:
            tmin = tmin_i
            tmax = tmax_i
        else:
            tmin = min(tmin, tmin_i)
            tmax = max(tmax, tmax_i)

    for i in range(len(s)):
        smin_i = s[i].min()
        smax_i = s[i].max()
        if i == 0:
            smin = smin_i
            smax = smax_i
        else:
            smin = min(smin, smin_i)
            smax = max(smax, smax_i)

    for i in range(len(data)):
        datamin_i = data[i].min()
        datamax_i = data[i].max()
        if i == 0:
            datamin = datamin_i
            datamax = datamax_i
        else:
            datamin = min(datamin, datamin_i)
            datamax = max(datamax, datamax_i)

    # Make the figure, ready to add all the little plots
    fig, axes = plt.subplots(rows, columns, sharex=True, sharey=True, figsize=(12, 10))

    # Pick the colormap of your choice, see https://matplotlib.org/tutorials/colors/colormaps.html
    colormap = plt.get_cmap(cmap)  # viridis plasma gnuplot2

    # loop through all the little plots
    actual_rows = int(len(data)/rows)
    i = 0
    for y in range(columns):
        for x in range(actual_rows):
            # check the tmax to see if we have a crashed simulation
            tmax_i = t[i].max() / 3600  # in hours
            if tmax_i < 72:
                tmax_plot = tmax_i*3600

                # old code if we just want empty plots instead of half filled plots {
                #i = i+1
                #continue
                #}
            else:
                tmax_plot = tmax

            # actually plot 1 little plot
            img = axes[x, y].imshow(data[i], origin='lower', extent=[smin, smax, tmin, tmax_plot], aspect=smax / tmax, cmap=colormap)
            i = i+1
            if i == len(data):
                break

    # make the colorbar
    cbar = fig.colorbar(img, ax=axes.ravel().tolist())
    cbar.set_label('Temperature [MK]')

    return fig, axes

def open_new_figure(event):
    # this checks if the plots are clicked
    if event.inaxes is not None:
        if event.dblclick:  # check for double click
            ax = event.inaxes
            allaxes = fig.get_axes()  # get all axes
            for i in range(len(allaxes)):
                if ax == allaxes[i]:  # check which axis matches
                    # convert the axis number to plot number (swap digits, so 23 becomes 32)
                    j = (i // 10)+(i % 10)*10
                    plotnumber = str(j)
                    # make title
                    lambda1 = 0.02*((i % 10)+1)
                    lambda2 = 0.02*((i//10)+1)
                    title = r'$\lambda_1 = {0}*L; \lambda_2 = {1}*L$'.format(lambda1, lambda2)

                    # Plot it!
                    plot_single(coords_dict['coords'+plotnumber]['t'], coords_dict['coords'+ plotnumber]['s'], T_dict['T'+plotnumber], title=title)
                    plt.show()




if __name__ == '__main__':

    # load and save files correctly
    # ----------
    coords_dict = {}
    T_dict = {}
    ne_dict = {}
    v_dict = {}

    path = "D:/Documents/1_Unif_HDD/Master_Astrophysics/Research Project/Simulations/Analysation/Geometry0dot75"
    os.chdir(path)
    directories = [name for name in os.listdir(".") if os.path.isdir(name)]

    t = ()
    s = ()
    data = ()

    for i in range(len(directories)):
        # load all data
        coords_dict["coords{0}".format(i)], \
        T_dict["T{0}".format(i)], \
        ne_dict["ne{0}".format(i)], \
        v_dict["v{0}".format(i)] = load_files(directories[i])

        # put data in tuple for plot function
        t = t + (coords_dict["coords{0}".format(i)]['t'],)
        s = s + (coords_dict["coords{0}".format(i)]['s'],)
        data = data + (T_dict['T{0}'.format(i)],)
    # ----------


    rows = 10
    columns = 10
    fig, axes1 = plot_multiple(t, s, data, rows, columns)

    # Add labels left and under the plots
    lambda1 = [0.02, 0.04, 0.06, 0.08, 0.10, 0.12, 0.14, 0.16, 0.18, 0.2]
    i = 0
    j = 0
    for ax in axes1.flat:
        ax.set_xlabel(lambda1[i], fontsize=8)
        ax.set_ylabel(lambda1[j], fontsize=8)
        i = i+1
        if i == 10:
            i = 0
            j = j+1

    for ax in axes1.flat:
        ax.label_outer()  # disable labels for aal plot except the outer ones
        ax.set_xticks([])  # disable x-ticks and labels
        ax.set_yticks([])  # disable y-ticks and labels

    plt.subplots_adjust(right=0.725, wspace=0.25, hspace=0.25)

    # add large subplot, needed for nice axis-lables
    fig.add_subplot(111, frameon=False, zorder=-1)
    plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
    plt.xlabel(r'$\lambda_{1}$')
    plt.ylabel(r'$\lambda_{2}$')

    # save and show the figures
    # plt.savefig('early_test-circular-500x500.svg')
    # plt.savefig('early_test-circular-500x500.png')

    # some fancy code to select a subplot
    cid = fig.canvas.mpl_connect('button_press_event', open_new_figure)


    plt.show()

    # plot only 1 graph (uncomment if needed) {
    # plotnumber = '0'
    # plot_single(coords_dict['coords'+plotnumber]['t'], coords_dict['coords'+plotnumber]['s'], T_dict['T'+plotnumber])
    # plt.show()
    #}

