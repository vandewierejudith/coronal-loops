# Coronal Loops

## TODO
- [x] midterm presentation
- [x] discuss feedback midterm presentations and think about the Questions
    - why these assumptions?
    - how do you know that the geometry of coronal loops can change that way?
    - question of Andrew (out of comfortzone), how can we relate this to observations of far away stars
    - why can these results be important?
- [ ] simulations for all lambdas and geometries
    - [ ] b = 0.875 (Dante)
    - [ ] b = 1.123 (Judith)
- [ ] further develop the plotting script to make a 10 x 10 grid of plots 
- [ ] make a script, to see if there is coronal rain 
    - fix the fourier transform (Dante)
- [ ] starting to write the report (Intro: Judith;  aim + method: Giorgi)


## Questions (for Gabriel)
- [x] How to share or storage our simulations?
- [ ] How do we see if there is coronal rain?
- [ ] Periods of our simulations don't match with those in Froment
- [x] feedback midterm presentation

13/11
- [ ] what to do with the crashed simulations?
- [ ] cluster is behaving weird

16/11
- [ ] some troubles with fourier transform


## Feedback
- don't use ku leuven template (because it's boring if you have to look several such presentations)
- figures/plots
    - large enough font
    - label axes (don't forget units)
    - colorbar 
- stick to the time schedule


## simulations
- [ ] Geometry 0.5 - lambda 1 = 0.02L to 0.2L --> crached
- [x] Geometry 1 - lambda 1 = 0.02L to 0.2L
    - [x] Lambda 2 = 0.02L ; 0.04L
    - [x] Lambda 2 = 0.06L ; 0.08L
    - [x] Lambda 2 = 0.10L ; 0.12L
    - [x] Lambda 2 = 0.14L ; 0.16L
    - [x] Lambda 2 = 0.18L ; 0.20L
- [x] Geometry 1.25 - lambda 1 = 0.02L to 0.2L
    - [x] Lambda 2 = 0.02L ; 0.04L
    - [x] Lambda 2 = 0.06L ; 0.08L
    - [x] Lambda 2 = 0.10L ; 0.12L
    - [x] Lambda 2 = 0.14L ; 0.16L
    - [x] Lambda 2 = 0.18L ; 0.20L
    - Lambda 1 = 0.14L, Lambda = 0.16L crashed   
- [ ] Geometry 1.5 - lambda 1 = 0.02L to 0.2L--> crached
- [ ] Geometry 2 - lambda 1 = 0.02L to 0.2L --> will probably crash too
