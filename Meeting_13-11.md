# Meeting 7 (13/11)

## Recap previous week
- Tried to run simulations for b = 0.5 and 1.5
	- Changing Boundary conditions did not help
- Lots of crashes
	- Crashes to do with the cluster being not so nice
	- Crashes due to boundary conditions gave negative pressure at one of the footpoints
		- Cause of P<0: Condensation formed in the loop is too large, 
		  => falls through chromosphere instead of stopping at the chromosphere,
		  => reaches boundary domain and causes problems
		- possible solution (DO NOT TRY, THERE IS LITTLE TIME) : increase the density of the chromosphere
- plot of crashed simulations: two options
	- remove entirely from plot
	- change plot-script to plot only to the simulated time 

## Meeting Subject: Analysis of the data
How to check for coronal rain in 3 easy steps
1. Remove first 10 (or so) hours of the simulation
	- these are needed to establish the simulation
	- after this either an equilibrium or a limit cycle is reached
		- equilibrium = nothing happens
		- limit cycle = periodic changing in predictable way (is this correct??)
2. Check for **cycles**
	- Look at T at the Apex of the loop (or an average of several point in the apex)
	- Do a Fourrier transformation to see if there is a periodic cycle
		- This might be more difficult to do than it seems, but it is worth a try
		- if it does not work, just look at the simulations and pick the once with periodic behaviour
3. Check if it is **Coronal Rain**
	- See if T drops below 0.5 MK
	- The domain needs to be limited (because the T at the footpoints is below 0.5 MK)
		- look only at the part of the loop that is higher than a certain value (for example z>10Mm, Fromant 2018)

Futher concerns with the model
	- in a perfect world, the heating and other parameters are constant. Thins means that the loop would either reach a thermal equilibrium or it reaches cyclic behaviour forever
	- the sun is far from perfect and thus wil probably never reach this state
	- **This fact is the biggest limitation of the model** (constant parameters that are not constant IRL)
		
		
## TODO
- Fix Cluster and run 0.75 (Dante?)
- Analyse data
- start writing report

## Dates and Deadlines
- **20/11** new meeting at 11h. If we find an earlier moment it would be great.
- **25/11 or 26/11** Deadline report
- **1/12** Presentation (probably)

