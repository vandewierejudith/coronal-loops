c
c-----------------------------------------------------------------------
c
c ****** Solve the 1D equations along a coronal loop.
c
c-----------------------------------------------------------------------
c
c ****** Updates and bug fixes:
c
c        08/13/98, ZM, Version 1.00:
c
c         - Original version of program.
c         - Derived from MHD1 version 1.32 of 01/21/98.
c
c        09/22/98, ZM, Version 1.01:
c
c         - Updated this version from MHD1_LOOP_XYZ,
c           Version 1.01 of 09/03/98.
c
c        01/15/99, ZM, Version 1.02:
c
c         - Added the ability to use the loop length
c           from the loop file in the calculation.
c           Use a negative S1 to use the loop length
c           from the file.
c
c        09/29/2000, ZM, Version 1.03:
c
c         - Changed the code to use REAL*8 variables.
c           This is necessary to do cases with highly resolved
c           meshes (with thousands of mesh points).
c           This requires the compiler to force all REAL variables
c           to REAL*8.  This can be done, for example, with PGF90
c           with the "-r8" option.
c
c        10/16/2000, ZM, Version 1.04:
c
c         - Changed the initialization.  Use a uniform temperature
c           and a density chosen so that radiation loss balances
c           the applied heating.
c
c        10/17/2000, ZM, Version 1.05:
c
c         - Fixed GETBC0 so that supersonic inflow is allowed.
c
c        04/03/2001, ZM, Version 1.06:
c
c         - Changed IGETMEM to use the default REAL type in
c           assigning memory.  This is a safer way to go.  The code
c           can now be compiled using REAL*4 or REAL*8 as the
c           default REAL type.
c         - Added variable TCONDCUT which can be used to modify
c           the Spitzer thermal conductivity at low temperatures.
c           The thermal conductivity is set to a uniform value for
c           temperatures below TCONDCUT (specified in degrees K).
c           To leave kappa unmodified, use TCONDCUT=1.
c
c        04/18/2001, ZM and RL, Version 1.07:
c
c         - Added in a momentum diagnostic.
c         - Added the ability to control the splitting of the
c           viscosity advance in the momentum equation.
c
c        10/09/2001, ZM and RL, Version 1.07a:
c
c         - Added restart file capability. It restarts from
c           files named restart1.dat (t,ne) and restart2.dat (v).
c           The mhd1.out and mhd2.out files from a previous run
c           can be used.
c
c        10/09/2003, RL, Version 1.07b:
c
c         - Added heating proportional to B^2
c
c        04/04/2004, RL and ZM, Version 1.08:
c
c         - Added the ability to select the radiation law at
c           run time.
c         - Added the fix to treat the splitting of the viscous term
c           more accurately in the momentum equation
c           (as regards the semi-implicit term time levels).
c         - Added an option to use a thermal conductivity cutoff that
c           is smoother as a function of temperature.
c
c        05/25/2004, ZM, Version 1.09:
c
c         - Put in a rapid decrease in the Rosner radiation law
c           near 20,000K, just like in the other radiation laws.
c           This ought to yield chromospheres with a temperature
c           near 20,000K.
c
c        06/01/2004, ZM, Version 1.10:
c
c         - Put in a time step limit to ensure positivity of
c           the temperature in the radiation law advance.
c         - Fixed a slight anomaly in the hydrostatic equilibrium
c           initialization in which the boundary density was not
c           being initialized to exactly the requested value.
c
c        01/24/2005, ZM, Version 1.11:
c
c         - Added an improved way to broaden the scale length
c           in the transition region.  This is done by boosting
c           kappa and reducing Q in such a way as to keep kappa*Q
c           constant.  This combination keeps the coronal solution
c           invariant to changes in the transition region scale
c           length.
c
c        08/08/2005, ZM, Version 1.12:
c
c         - Changed the way heating is specified.  The input file
c           for this version is NOT compatible with previous
c           versions.
c
c        10/19/2006, ZM, Version 1.13:
c
c         - Added a dump of the initial heating profile to an
c           output file named "heating.out".
c
c        03/27/2007, ZM, Version 1.14:
c
c         - Updated version 1.13 with Yung Mok's updates from version
c           1.12ym.  The code can now handle time-dependent heating,
c           as well as the old way of specifying a static heating,
c           which we now call "classic" heating.  To use "classic"
c           heating, set I_HEATING_PROFILE=0.
c         - Added a check on the number of lines in the input file
c           to minimize errors.
c         - Implemented Yung's scheme for selecting the type of loop
c           initialization when an initial file name is not supplied.
c           The user can specify '1', '2', '3', or '4' instead of the
c           file name to select among the preset initializations.
c         - Updated IGETMEM so that it works regardless of the
c           size of REALs.  The code ought to work for single or
c           double precision, although double precision is recommended
c           for accuracy.
c         - Added variable TMAX to exit the code when the integration
c           time equals or exceeds TMAX.
c
c        06/15/2007, ZM, Version 1.15:
c
c         - Added the ability to apply impulsive heating.
c         - Added the ability to dump the heating profile.
c
c        03/24/2011, ZM, Version 2.00:
c
c         - Performed a comprehensive cleanup of the code.
c           This new version uses FORTRAN90.  A lot of the legacy
c           code that was outdated has been removed.
c         - This is a complete top-to-bottom revamp of the code.
c         - The user can select which quantities to output vs. time
c           directly to HDF files.
c         - Changed the normalization of the output.  The HDF output
c           files and many of the diagnostic files are now written
c           in physical units to help in the physical interpretation
c           of the results.
c         - Assembled the input file fields into logical groups,
c           and added comment headers.  The input file in this
c           version is thus NOT compatible with previous versions.
c         - Modified the format of the loop geometry file to remove
c           unused columns.  The new format has the following columns:
c
c             s    z    A    B    gnorm
c
c           Note that this is not compatible with previous versions
c           of the code.
c         - Added the ability to use an internally generated loop
c           when the loop geometry file is not specified.  This
c           is a semi-circular loop.  This will help to test ideas
c           rapidly on a generic loop.
c         - Added a NAMELIST for the extended heating model.
c           This will hopefully prevent frequent changes to the
c           input file format when new heating models are
c           implemented.
c
c        06/14/2011, ZM, Version 2.01:
c
c         - Added the ability to include a z-dependent profile
c           in the heating function for the Rappazzo heating model.
c           This can be used to reduce the heating at the loop
c           footpoints.
c
c        08/24/2012, ZM, Version 2.02:
c
c         - Fixed a small bug in the error checking during
c           selection of a heating model (found by Yung).  This
c           did not affect the computations.
c
c        10/12/2012, ZM, Version 2.03:
c
c         - Reduced the convergence criterion that was used to
c           determine the radiation balance temperature in
c           routine SET_RAD_BALANCE_TEMP from 1e-6 to 1e-15,
c           and added a check to keep the temperature greater
c           than T_CHROMO during the iteration.
c           The lack of accuracy was producing a slight blip in
c           the heat flux near the boundaries.  The iteration
c           converges rapidly, so this should not affect the
c           efficiency of the code much.  Revisit this decision
c           if at some point we decide to call SET_RAD_BALANCE_TEMP
c           at every time step (for example, to maintain the balance
c           of radiation loss and heating when H changes with time).
c
c        10/21/2012, ZM, Version 2.04:
c
c         - Implemented new radiation functions that make Q go
c           to zero at the base of the chromosphere linearly
c           with temperature.  This may result in a more robust
c           treatment of the chromosphere.
c         - This has necessitated a change in the way the
c           radiation law is initialized.
c         - Changed the iteration scheme in SET_RAD_BALANCE_TEMP
c           to use the more robust method of bisection.
c
c        01/25/2013, ZM, Version 2.05:
c
c         - Added the ability to stop a run early by placing
c           a file named "STOPRUN" in the run directory.
c         - Added the ability to get an intermediate dump of
c           the HDF fields by placing a file named "DUMPHDF in
c           the run directory.
c
c        02/07/2013, ZM, Version 2.06:
c
c         - Updated the CHIANTI radiative loss functions to
c           those from CHIANTI v7.1.  The version 6 functions
c           were suspect at high temperatures (approaching
c           10^7 K or more), as determined by comparing with
c           Enrico Landi's version.  It is not known how this
c           discrepancy came about.  In any case, the version
c           7.1 functions ought to be more accurate.
c
c        02/15/2013, ZM, Version 2.07:
c
c         - Corrected a bug in the way that fields on the main
c           mesh were output to the HDF dump files.  The s mesh
c           in these files was incorrect.  Previously, the fields
c           on the main mesh incorrectly used the SH mesh rather
c           than the S mesh.
c
c        05/20/2013, ZM, Version 2.08:
c
c         - Changed the time differencing of the implicit radiation
c           term so it is advanced a full time step ahead, rather
c           than half a time step.  Increased the safety factor
c           in the radiation time step limit (i.e., the time step
c           is now smaller).  This seems to improve stability.
c
c        06/01/2015, ZM, Version 2.08_TXT:
c
c         - Took out the HDF dump routines to simplify the installation
c           of the code in a location where the HDF4 library is not
c           easily available.  The HDF output files were replaced
c           with plain text output files (in SDS text format).
c
c#######################################################################
      module ident
c
c-----------------------------------------------------------------------
c ****** Code name.
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      character(*), parameter :: code_name   ='MHD1_LOOP'
      character(*), parameter :: code_version='2.08_TXT'
      character(*), parameter :: code_update ='06/01/2015'
c
      end module
c#######################################################################
      module globals
c
c-----------------------------------------------------------------------
c ****** Number of mesh points.
c-----------------------------------------------------------------------
c
      implicit none
c
      integer :: ns
      integer :: nsm1
      integer :: nsm2
c
      end module
c#######################################################################
      module mesh
c
c-----------------------------------------------------------------------
c ****** Storage for mesh quantities.
c-----------------------------------------------------------------------
c
      use number_types
      use globals
c
      implicit none
c
c ****** Mesh bounds and loop length.
c
      real(r_typ) :: s0,s1,sl
c
c ****** Mesh arrays.
c
      real(r_typ), dimension(:), allocatable :: s
      real(r_typ), dimension(:), allocatable :: sh
      real(r_typ), dimension(:), allocatable :: ds
      real(r_typ), dimension(:), allocatable :: dsh
      real(r_typ), dimension(:), allocatable :: area
      real(r_typ), dimension(:), allocatable :: areah
      real(r_typ), dimension(:), allocatable :: z
      real(r_typ), dimension(:), allocatable :: zh
c
      end module
c#######################################################################
      module fields
c
c-----------------------------------------------------------------------
c ****** Storage for the primary fields.
c-----------------------------------------------------------------------
c
      use number_types
      use globals
c
      implicit none
c
      real(r_typ), dimension(:), pointer :: p
      real(r_typ), dimension(:), pointer :: ne
      real(r_typ), dimension(:), pointer :: rho
      real(r_typ), dimension(:), pointer :: v
      real(r_typ), dimension(:), pointer :: temp
      real(r_typ), dimension(:), pointer :: b
      real(r_typ), dimension(:), pointer :: g
c
      real(r_typ), dimension(:), pointer :: heating
      real(r_typ), dimension(:), pointer :: heating_static
c
      real(r_typ), dimension(:), pointer :: kappa
      real(r_typ), dimension(:), pointer :: visc
c
      real(r_typ), dimension(:), pointer :: sifac
c
      end module
c#######################################################################
      module normalization_parameters
c
c-----------------------------------------------------------------------
c ****** Values that are used to normalize code variables and to
c ****** convert to physical units.
c-----------------------------------------------------------------------
c
      use number_types
c
      implicit none
c
      real(r_typ), parameter :: pi=3.14159265358979323846_r_typ
c
c ****** Solar radius [cm].
c
      real(r_typ), parameter :: rsun=6.96e10_r_typ
c
c ****** Solar gravity at r=RSUN [cm/s**2].
c
      real(r_typ), parameter :: g0phys=0.274e5_r_typ
c
c ****** Number density corresponding to a normalized number density
c ****** of 1 [/cm**3].
c
      real(r_typ), parameter :: fn0phys=1.e8_r_typ
c
c ****** Proton mass [g].
c
      real(r_typ), parameter :: m_proton=1.6726e-24_r_typ
c
c ****** Boltzmann's constant [erg/K].
c
      real(r_typ), parameter :: boltz=1.3807e-16_r_typ
c
c ****** Normalization quantities to convert between normalized
c ****** and physical units.
c
      real(r_typ) :: he_frac
      real(r_typ) :: he_rho
      real(r_typ) :: he_p
      real(r_typ) :: he_np
c
      real(r_typ) :: fnorml
      real(r_typ) :: fnormt
      real(r_typ) :: fnormm
c
      real(r_typ) :: fn_n
      real(r_typ) :: fn_rho
      real(r_typ) :: fn_p
      real(r_typ) :: fn_t
      real(r_typ) :: fn_v
      real(r_typ) :: fn_b
      real(r_typ) :: fn_qrad
      real(r_typ) :: fn_kappa
      real(r_typ) :: fn_q0
      real(r_typ) :: fn_heat
c
c ****** Spitzer thermal conductivity coefficient [CGS units].
c
      real(r_typ) :: kappa0_spitzer
c
c ****** Radiative balance BC normalization constants.
c
      real(r_typ) :: fn_radbc1
      real(r_typ) :: fn_radbc2
c
      end module
c#######################################################################
      module boundary_conditions
c
c-----------------------------------------------------------------------
c ****** Boundary conditions.
c-----------------------------------------------------------------------
c
      use number_types
c
      implicit none
c
      character(16) :: tbc0type,tbc1type,
     &                 nbc0type,nbc1type,
     &                 vbc0type,vbc1type
c
c ****** Symbolic constants to define boundary condition types.
c
      integer, parameter :: IBC_FIXED   =1
      integer, parameter :: IBC_ISOLATED=2
      integer, parameter :: IBC_RADBC   =3
      integer, parameter :: IBC_ZERO    =4
      integer, parameter :: IBC_CHARBC  =5
c
c ****** Boundary conditions for the main variables.
c
      integer :: ibc_t_0
      integer :: ibc_t_1
      integer :: ibc_ne_0
      integer :: ibc_ne_1
      integer :: ibc_v_0
      integer :: ibc_v_1
c
c ****** Flags to determine boundary conditions on T to
c ****** balance heating with radiative loss.
c
      logical :: set_rad_balance_t0=.false.
      logical :: set_rad_balance_t1=.false.
c
c ****** Boundary values for the main variables.
c
      real(r_typ) :: ne0,ne1
      real(r_typ) :: tbc0,tbc1
      real(r_typ) :: vbc0,vbc1
c
      real(r_typ) :: ne_target0,ne_target1
      real(r_typ) :: ne_advance_time_const
      real(r_typ) :: ne_advance_frac_lim
c
      end module
c#######################################################################
      module radiative_loss_law
c
c-----------------------------------------------------------------------
c ****** Radiative loss law parameters.
c-----------------------------------------------------------------------
c
      use number_types
c
      implicit none
c
c ****** Define the radiative loss law types.
c
      integer, parameter :: n_rad_law=5
c
c ****** Radiative loss law index definitions.
c ****** This list must start at index 1 and must end with index
c ****** value N_RAD_LAW.
c
      integer, parameter :: RAD_LAW_ATHAY         =1
      integer, parameter :: RAD_LAW_ROSNER        =2
      integer, parameter :: RAD_LAW_RTV           =3
      integer, parameter :: RAD_LAW_CHIANTI_CORONA=4
      integer, parameter :: RAD_LAW_CHIANTI_PHOTO =5
c
c ****** Radiative loss law name definitions.
c ****** The order must correspond to the above declarations.
c
      character(16), dimension(n_rad_law) :: rad_law_name
c
      data rad_law_name/'ATHAY',
     &                  'ROSNER',
     &                  'RTV',
     &                  'CHIANTI_CORONA',
     &                  'CHIANTI_PHOTO'/
c
c ****** The index of the radiative loss law selected.
c
      integer :: rad_law_index=0
c
c ****** Flag to indicate that the radiative loss law has been
c ****** initialized.
c
      logical :: rad_law_initialized=.false.
c
c ****** Flag to apply the inverse boost to Q.
c ****** (This keeps the kappa*Q constant when kappa is modified
c ****** at low temperatures to broaden the TR.)
c
      logical :: apply_inverse_boost_to_q
c
c ****** Flag to set Q to zero at the base of the chromosphere.
c
      logical :: zero_q_at_chromo_base
c
c ****** Temperatures that define the region where Q is modified
c ****** to go to zero (if requested), and the constants used
c ****** in the transition.
c
      real(r_typ) :: t_zqc_0
      real(r_typ) :: t_zqc_1
      real(r_typ) :: a_zqc
      real(r_typ) :: b_zqc
c
c ****** Flag to apply the legacy reduction of Q at the base of the
c ****** chromosphere (developed by Yung Mok).  This option is not
c ****** recommended; it is provided for backward compatibility.
c
      logical :: legacy_q_chromo_reduction=.false.
c
      end module
c#######################################################################
      module chianti_rad_loss_corona
c
c-----------------------------------------------------------------------
c ****** Parameters for the CHIANTI radiative loss function with
c ****** coronal abundances.
c-----------------------------------------------------------------------
c
      use number_types
c
      implicit none
c
c ****** This radiative loss law was obtained from CHIANTI
c ****** version 7.1 using SolarSoft routine RAD_LOSS.PRO.
c
c ****** Abundance model: coronal abundances
c ******   (Feldman et al. 1992, ApJSS, 81, 387;
c ******    Landi, Feldman, & Dere 2002, ApJ, 139, 281;
c ******    Grevesse & Sauval 1998, Space Sci. Rev., 85, 161)
c ****** Abundance file: sun_coronal_ext.abund
c
c ****** Ionization equilibrium model: CHIANTI
c ****** Ionization equilibrium file: chianti.ioneq
c
c ****** Evaluated at a constant pressure of 0.5 [dyn/cm^2/s].
c
c ****** Version of CHIANTI used.
c
      character(8), parameter :: chianti_version='7.1'
c
c ****** Number of values in the table.
c
      integer, parameter :: n_elem=101
c
c ****** The table assumes that the radiative loss function
c ****** Q(T) is expressed in terms of uniform increments in
c ****** log10(T), with T in [K], and Q in [erg-cm^3/s].
c
      real(r_typ), parameter :: log10_T_min=4._r_typ
      real(r_typ), parameter :: log10_T_max=9._r_typ
c
c ****** Inverse of the (uniform) increment in log10(T).
c
      real(r_typ), parameter :: log10_dt_inv=(n_elem-1)
     &                                       /( log10_T_max
     &                                         -log10_T_min)
c
c ****** Table that has the values of log10(Q[erg-cm^3/s]).
c
      real(r_typ), dimension(n_elem) :: log10_Q_table
c
      data log10_Q_table/
     & -23.053720,-22.608917,-22.201519,-21.869802,-21.684013,
     & -21.659719,-21.730042,-21.830411,-21.910629,-21.940114,
     & -21.925158,-21.882720,-21.819293,-21.739567,-21.645444,
     & -21.541707,-21.437104,-21.347542,-21.293833,-21.269791,
     & -21.258449,-21.270308,-21.287414,-21.279126,-21.253218,
     & -21.230150,-21.219972,-21.216386,-21.224706,-21.276223,
     & -21.380011,-21.472548,-21.504925,-21.493113,-21.466189,
     & -21.443690,-21.430127,-21.415454,-21.394676,-21.374235,
     & -21.363539,-21.366170,-21.376353,-21.391410,-21.417227,
     & -21.465230,-21.550128,-21.673484,-21.807932,-21.926742,
     & -22.018263,-22.077592,-22.106909,-22.112793,-22.103273,
     & -22.085571,-22.065103,-22.046422,-22.034324,-22.033451,
     & -22.048419,-22.083934,-22.144429,-22.226347,-22.315033,
     & -22.394977,-22.458936,-22.506446,-22.539409,-22.560491,
     & -22.572118,-22.576383,-22.575024,-22.569357,-22.560548,
     & -22.549457,-22.536845,-22.523306,-22.509245,-22.494902,
     & -22.480377,-22.465586,-22.450447,-22.434802,-22.418578,
     & -22.401694,-22.384121,-22.365890,-22.347013,-22.327555,
     & -22.307556,-22.287064,-22.266142,-22.244831,-22.223163,
     & -22.201198,-22.178951,-22.156461,-22.133751,-22.110846,
     & -22.087767/
c
      end module
c#######################################################################
      module chianti_rad_loss_photo
c
c-----------------------------------------------------------------------
c ****** Parameters for the CHIANTI radiative loss function with
c ****** photospheric abundances.
c-----------------------------------------------------------------------
c
      use number_types
c
      implicit none
c
c ****** This radiative loss law was obtained from CHIANTI
c ****** version 7.1 using SolarSoft routine RAD_LOSS.PRO.
c
c ****** Abundance model: photospheric abundances
c ******   (Grevesse, Asplund, & Sauval 2007,
c ******    Space Sci. Rev., 130, 105)
c ****** Abundance file: sun_photospheric_2007_grevesse.abund
c
c ****** Ionization equilibrium model: CHIANTI
c ****** Ionization equilibrium file: chianti.ioneq
c
c ****** Evaluated at a constant pressure of 0.5 [dyn/cm^2/s].
c
c ****** Version of CHIANTI used.
c
      character(8), parameter :: chianti_version='7.1'
c
c ****** Number of values in the table.
c
      integer, parameter :: n_elem=101
c
c ****** The table assumes that the radiative loss function
c ****** Q(T) is expressed in terms of uniform increments in
c ****** log10(T), with T in [K], and Q in [erg-cm^3/s].
c
      real(r_typ), parameter :: log10_T_min=4._r_typ
      real(r_typ), parameter :: log10_T_max=9._r_typ
c
c ****** Inverse of the (uniform) increment in log10(T).
c
      real(r_typ), parameter :: log10_dt_inv=(n_elem-1)
     &                                       /( log10_T_max
     &                                         -log10_T_min)
c
c ****** Table that has the values of log10(Q[erg-cm^3/s]).
c
      real(r_typ), dimension(n_elem) :: log10_Q_table
c
      data log10_Q_table/
     & -23.278543,-22.760442,-22.301559,-21.942126,-21.751012,
     & -21.738208,-21.823982,-21.936337,-22.031817,-22.085563,
     & -22.104446,-22.101736,-22.072176,-22.012507,-21.925388,
     & -21.817613,-21.699178,-21.588129,-21.509493,-21.467012,
     & -21.450323,-21.464954,-21.488495,-21.487622,-21.468062,
     & -21.450199,-21.445180,-21.447295,-21.462569,-21.526248,
     & -21.656325,-21.794838,-21.879875,-21.912466,-21.921077,
     & -21.932902,-21.956533,-21.973765,-21.972744,-21.962177,
     & -21.955532,-21.958838,-21.967740,-21.980043,-22.001315,
     & -22.041697,-22.112266,-22.211626,-22.317631,-22.413759,
     & -22.492288,-22.547303,-22.578962,-22.591815,-22.591543,
     & -22.583270,-22.570960,-22.558138,-22.548678,-22.546376,
     & -22.554703,-22.576570,-22.613490,-22.660351,-22.705494,
     & -22.739902,-22.761695,-22.772821,-22.775718,-22.772496,
     & -22.764719,-22.753542,-22.739824,-22.724179,-22.707109,
     & -22.688972,-22.670071,-22.650633,-22.630814,-22.610714,
     & -22.590384,-22.569815,-22.549002,-22.527913,-22.506544,
     & -22.484888,-22.462946,-22.440742,-22.418286,-22.395607,
     & -22.372720,-22.349648,-22.326410,-22.303024,-22.279501,
     & -22.255861,-22.232111,-22.208263,-22.184328,-22.160312,
     & -22.136225/
c
      end module
c#######################################################################
      module params
c
c-----------------------------------------------------------------------
c ****** Main parameters.
c-----------------------------------------------------------------------
c
      use number_types
c
      implicit none
c
c ****** Time-step related parameters.
c
      real(r_typ) :: time=0.
      integer :: itime=0
c
      integer :: max_time_steps
      real(r_typ) :: tmax
      real(r_typ) :: dt,dtmax
      real(r_typ) :: dtmax_initial,t_small_dt
c
c ****** Gravity.
c
      logical :: use_gravity
      real(r_typ) :: g0
      real(r_typ) :: phi0,phi1
c
c ****** Gamma.
c
      real(r_typ) :: gamma
c
c ****** Radiative loss law to use.
c
      character(32) :: rad_law_type
c
c ****** Temperature at the base of the chromosphere.
c
      real(r_typ) :: t_chromo
c
c ****** Numerical factors.
c
      logical :: use_predictor
      logical :: use_flow_in_predictor
      logical :: semi_implicit
      real(r_typ) :: simult
      real(r_typ) :: upwind,cfl
      real(r_typ) :: betapc_v
      real(r_typ) :: betapc_t
      real(r_typ) :: fac_cflv
c
c ****** Diagnostic quantities.
c
      real(r_typ) :: tmass
c
c ****** Diagnostic parameters.
c
      integer :: idebug
      integer :: iprint
c
c ****** Flag to indicate that the s mesh was read in.
c
      logical :: mesh_was_read_in
c
c ****** Flags to end the run.
c
      logical :: ifabort=.false.
      logical :: ifend=.false.
c
      end module
c#######################################################################
      module heating_parameters
c
c-----------------------------------------------------------------------
c ****** Heating parameters.
c-----------------------------------------------------------------------
c
      use number_types
c
      implicit none
c
c ****** Define the heating model types.
c
      integer, parameter :: n_heat_model=3
c
c ****** Heating model index definitions.
c ****** This list must start at index 1 and must end with index
c ****** value N_HEAT_MODEL.
c
      integer, parameter :: HEAT_MODEL_B_POWER  =1
      integer, parameter :: HEAT_MODEL_IMPULSIVE=2
      integer, parameter :: HEAT_MODEL_RAPPAZZO =3
c
c ****** Heating model name definitions.
c ****** The order must correspond to the above declarations.
c
      character(32), dimension(n_heat_model) :: heat_model_name
c
      data heat_model_name/'IMPULSIVE',
     &                     'B_POWER',
     &                     'RAPPAZZO'/
c
c ****** Variables to select the heating model.
c
      character(32) :: heating_model_type
c
      integer :: heating_model_index
c
c ****** Multiplier for radiative loss.
c
      real(r_typ) :: radloss
c
c ****** Multiplier for thermal conductivity.
c
      real(r_typ) :: tcond
c
c ****** Factors that control the broadening of the
c ****** transition region.
c
      real(r_typ) :: p_cutoff
      real(r_typ) :: t_cutoff
c
c ****** Uniform heating value.
c
      real(r_typ) :: h0_uniform
c
c ****** Exponential heating parameters.
c
      real(r_typ) :: h0_l_1,hlen_l_1
      real(r_typ) :: h0_l_2,hlen_l_2
      real(r_typ) :: h_s_offset_l
c
      real(r_typ) :: h0_r_1,hlen_r_1
      real(r_typ) :: h0_r_2,hlen_r_2
      real(r_typ) :: h_s_offset_r
c
c ****** Heating profile file name.
c
      character(512) :: heating_input_file
c
c ****** Heating expressed as a heat flux (as a diagnostic).
c
      real(r_typ) :: q0,q1
c
c-----------------------------------------------------------------------
c ****** Parameters for the extended heating model.
c-----------------------------------------------------------------------
c
c ****** Extended heating parameters NAMELIST input file name.
c
      character(512) :: heating_model_params_file
c
      logical :: extended_heating_model
c
c ****** Parameters for model HEAT_MODEL_B_POWER.
c
      real(r_typ) :: h_bpower_h0=0.
      real(r_typ) :: h_bpower_power=1._r_typ
      real(r_typ) :: h_bpower_mask_b0=1._r_typ
      real(r_typ) :: h_bpower_mask_db=.1_r_typ
c
c ****** Parameters for model HEAT_MODEL_IMPULSIVE.
c
      real(r_typ) :: h_impulsive_h0=0.
      real(r_typ) :: h_impulsive_s0=.1_r_typ
      real(r_typ) :: h_impulsive_ds=.02_r_typ
      real(r_typ) :: h_impulsive_t0=1._r_typ
      real(r_typ) :: h_impulsive_dt=.1_r_typ
c
c ****** Parameters for model HEAT_MODEL_RAPPAZZO.
c
      character(512) :: h_rappazzo_loop_length_file=' '
c
      real(r_typ) :: h_rappazzo_alpha=2._r_typ
      real(r_typ) :: h_rappazzo_h0=0.
      logical :: h_rappazzo_use_wrong_l_power=.false.
c
      logical :: h_rappazzo_use_z_profile=.false.
      real(r_typ) :: h_rappazzo_z_prof_z0=.004_r_typ
      real(r_typ) :: h_rappazzo_z_prof_dz=.002_r_typ
      real(r_typ) :: h_rappazzo_z_prof_h0=5.e-4_r_typ
c
c ****** Namelist for the extended heating parameters.
c
      namelist /heating_params/ heating_model_type,
     &                          h_bpower_h0,
     &                          h_bpower_power,
     &                          h_bpower_mask_b0,
     &                          h_bpower_mask_db,
     &                          h_impulsive_h0,
     &                          h_impulsive_s0,
     &                          h_impulsive_ds,
     &                          h_impulsive_t0,
     &                          h_impulsive_dt,
     &                          h_rappazzo_loop_length_file,
     &                          h_rappazzo_alpha,
     &                          h_rappazzo_h0,
     &                          h_rappazzo_use_wrong_l_power,
     &                          h_rappazzo_use_z_profile,
     &                          h_rappazzo_z_prof_z0,
     &                          h_rappazzo_z_prof_dz,
     &                          h_rappazzo_z_prof_h0
c
      end module
c#######################################################################
      module initial_state
c
c-----------------------------------------------------------------------
c ****** Parameters that control the initial state.
c-----------------------------------------------------------------------
c
      use number_types
c
      implicit none
c
c ****** Initial state parameters.
c
      real(r_typ) :: hs_t_apex
      real(r_typ) :: hs_n_e_base
      real(r_typ) :: hs_chromo_thickness
c
c ****** Magnetic field profile for the internally generated loop.
c
      real(r_typ) :: b_uniform
      real(r_typ) :: b0_l,blen_l
      real(r_typ) :: b0_r,blen_r
c
c ****** Initial state file name.
c
      character(512) :: initial_state_file
c
      end module
c#######################################################################
      module file_names
c
c-----------------------------------------------------------------------
c ****** File names.
c-----------------------------------------------------------------------
c
      implicit none
c
      character(512) :: mesh_file
      character(512) :: mesh_output_file
      character(512) :: geometry_file
      character(512) :: final_state_file
      character(512) :: final_state_phys_units_file
      character(512) :: momentum_diagnostic_file
      character(512) :: energy_diagnostic_file
c
      end module
c#######################################################################
      module profile_def
c
c-----------------------------------------------------------------------
c ****** Define a profile structure.
c-----------------------------------------------------------------------
c
      use number_types
c
      implicit none
c
c ****** Maximum number of nodes in a profile definition.
c
      integer, parameter :: nodes_max=50
c
c ****** Define the structure to hold a profile.
c
      type :: prof
        integer :: n
        real(r_typ), dimension(nodes_max) :: x
        real(r_typ), dimension(nodes_max) :: f
      end type
c
      end module
c#######################################################################
      module viscosity_profile
c
c-----------------------------------------------------------------------
c ****** Storage for the viscosity definition.
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use profile_def
c
      implicit none
c
c ****** Viscosity profile.
c
      type(prof) :: visc_prof
c
c ****** Flag to use viscosity.
c
      logical :: use_viscosity
c
c ****** Flag to split the viscosity advance.
c
      logical :: split_visc_advance
c
c ****** Viscosity profile input file name.
c
      character(512) :: viscosity_input_file
c
c ****** File name to write the viscosity profile to.
c
      character(512) :: viscosity_output_file
c
      end module
c#######################################################################
      module flux_tube_geometry
c
c-----------------------------------------------------------------------
c ****** Buffers to store the flux tube geometry that is read in.
c-----------------------------------------------------------------------
c
      use number_types
c
      implicit none
c
      integer :: nft
c
      real(r_typ), dimension(:), allocatable :: s_ft
      real(r_typ), dimension(:), allocatable :: z_ft
      real(r_typ), dimension(:), allocatable :: a_ft
      real(r_typ), dimension(:), allocatable :: b_ft
      real(r_typ), dimension(:), allocatable :: g_ft
c
      end module
c#######################################################################
      module dump_buf_def
c
c-----------------------------------------------------------------------
c ****** Define a structure to hold a 1D and a 2D pointer.
c-----------------------------------------------------------------------
c
      use number_types
c
      implicit none
c
      type :: dump_buf
        real(r_typ) :: scaling_factor
        real(r_typ), dimension(:), pointer :: source
        real(r_typ), dimension(:,:), pointer :: f
      end type
c
      end module
c#######################################################################
      module diagnostic_output
c
c-----------------------------------------------------------------------
c ****** Diagnostic dumps and time histories.
c-----------------------------------------------------------------------
c
      use number_types
      use dump_buf_def
c
      implicit none
c
c ****** The last time when diagnostic intervals were checked.
c
      real(r_typ) :: t_last_check=0.
c
c ****** Flag to indicate a history collection step.
c
      logical :: history_step=.false.
c
c ****** Flag to indicate a plotting step.
c
      logical :: plotting_step=.false.
c
c-----------------------------------------------------------------------
c ****** Time histories.
c-----------------------------------------------------------------------
c
c ****** Time history collection intervals.
c
      integer :: hist_int_step
      real(r_typ) :: hist_int_time
c
c ****** Number of time histories collected.
c
      integer :: ihist=0
c
c ****** Mesh index at which to collect time histories.
c
      integer :: s_mesh_hist_index
c
c ****** History buffer size.
c
      integer, parameter :: n_hist_max=20000
c
c ****** Time history buffers.
c
      real(r_typ), dimension(n_hist_max) :: hist_t,
     &                                      hist_ne,
     &                                      hist_temp,
     &                                      hist_v,
     &                                      hist_ke,
     &                                      hist_ie
c
c-----------------------------------------------------------------------
c ****** Plotting diagnostics (i.e., HDF dumps).
c-----------------------------------------------------------------------
c
c ****** Spatial diagnostic plot intervals.
c
      integer :: plot_int_step
      real(r_typ) :: plot_int_time
c
c ****** Define the diagnostic fields that can be plotted.
c
      integer, parameter :: n_diag_fields=9
c
c ****** Diagnostic field index definitions.
c ****** This list must start at index 1 and must end with index
c ****** value N_DIAG_FIELDS.
c
      integer, parameter :: DIAG_FLD_T   =1
      integer, parameter :: DIAG_FLD_NE  =2
      integer, parameter :: DIAG_FLD_V   =3
      integer, parameter :: DIAG_FLD_P   =4
      integer, parameter :: DIAG_FLD_H   =5
      integer, parameter :: DIAG_FLD_QR  =6
      integer, parameter :: DIAG_FLD_QC  =7
      integer, parameter :: DIAG_FLD_PV  =8
      integer, parameter :: DIAG_FLD_Q_TC=9
c
c ****** Diagnostic field name definitions.
c ****** The order must correspond to the above declarations.
c
      character(16), dimension(n_diag_fields) :: diag_fld_name
c
      data diag_fld_name/'T',
     &                   'ne',
     &                   'v',
     &                   'p',
     &                   'H',
     &                   'Qr',
     &                   'Qc',
     &                   'pv',
     &                   'q_tc'/
c
c ****** Number of fields to plot.
c
      integer :: nplot=0
c
c ****** Indices of fields to plot.
c
      integer, dimension(n_diag_fields) :: plot_index=0
c
c ****** Input list of fields to plot.
c
      character(16), dimension(n_diag_fields) :: plot_list=' '
c
c-----------------------------------------------------------------------
c ****** Plot buffers to hold the fields being plotted.
c-----------------------------------------------------------------------
c
c ****** Buffer size.  This expands automatically as needed.
c ****** The initial size is specified here.
c
      integer :: n_dump_buffer=100
c
c ****** Buffers to hold the fields.
c
      real(r_typ), dimension(:), pointer :: dump_t
      type(dump_buf), dimension(:), allocatable :: dump_fld_buffer
c
c ****** Dump counter.
c
      integer :: n_dump=0
c
      end module
c#######################################################################
      module meshdef
c
c-----------------------------------------------------------------------
c ****** Variables that define the mesh-point distribution.
c-----------------------------------------------------------------------
c
      use number_types
c
      implicit none
c
c ****** Maximum number of mesh segments.
c
      integer, parameter :: n_seg_max=50
c
      real(r_typ), dimension(n_seg_max) :: dsratio=0.
      real(r_typ), dimension(n_seg_max) :: sfrac=0.
c
      integer :: nfilt_smesh=5
c
      end module
c#######################################################################
      module si_term_velocities
c
c-----------------------------------------------------------------------
c ****** Storage for the semi-implicit term intermediate velocities.
c-----------------------------------------------------------------------
c
      use number_types
c
      implicit none
c
      real(r_typ), dimension(:), allocatable :: v_si_old1
      real(r_typ), dimension(:), allocatable :: v_si_old2
c
      end module
c#######################################################################
      module energy_diagnostics
c
c-----------------------------------------------------------------------
c ****** Storage for terms in the energy diagnostic.
c-----------------------------------------------------------------------
c
      use number_types
c
      implicit none
c
      real(r_typ), dimension(:), pointer :: h_h
      real(r_typ), dimension(:), pointer :: h_qr
      real(r_typ), dimension(:), pointer :: h_qc
      real(r_typ), dimension(:), pointer :: h_pv
      real(r_typ), dimension(:), pointer :: h_q_tc
c
      end module
c#######################################################################
      module momentum_diagnostics
c
c-----------------------------------------------------------------------
c ****** Storage for terms in the momentum diagnostic.
c-----------------------------------------------------------------------
c
      use number_types
c
      implicit none
c
      real(r_typ), dimension(:), allocatable :: vdgv
      real(r_typ), dimension(:), allocatable :: delsq
      real(r_typ), dimension(:), allocatable :: v_previous_step
c
      end module
c#######################################################################
      module rp1d_def
c
c-----------------------------------------------------------------------
c ****** Define a structure to hold a REAL 1D pointer.
c-----------------------------------------------------------------------
c
      use number_types
c
      implicit none
c
      type :: rp1d
        real(r_typ), dimension(:), pointer :: f
      end type
c
      end module
c#######################################################################
      module sds_def
c
c-----------------------------------------------------------------------
c ****** Definition of the SDS data structure.
c-----------------------------------------------------------------------
c
      use number_types
      use rp1d_def
c
      implicit none
c
      integer, parameter, private :: mxdim=3
c
      type :: sds
        integer :: ndim
        integer, dimension(mxdim) :: dims
        logical :: scale
        logical :: hdf32
        type(rp1d), dimension(mxdim) :: scales
        real(r_typ), dimension(:,:,:), pointer :: f
      end type
c
      end module
c#######################################################################
      module assign_ptr_1d_interface
      interface
        subroutine assign_ptr_1d (from,to)
        use number_types
        implicit none
        real(r_typ), dimension(:), target :: from
        real(r_typ), dimension(:), pointer :: to
        end
      end interface
      end module
c#######################################################################
      module assign_ptr_3d_interface
      interface
        subroutine assign_ptr_3d (from,to)
        use number_types
        implicit none
        real(r_typ), dimension(:,:,:), target :: from
        real(r_typ), dimension(:,:,:), pointer :: to
        end
      end interface
      end module
c#######################################################################
      program MHD1_LOOP
c
c-----------------------------------------------------------------------
c
      use number_types
      use ident
      use globals
      use mesh
      use meshdef
      use fields
      use initial_state
      use normalization_parameters
      use params
      use file_names
      use radiative_loss_law
      use viscosity_profile
      use heating_parameters
      use boundary_conditions
      use diagnostic_output
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
c ****** The number of input lines required in the input file.
c
      integer, parameter :: input_file_lines_required=192
c
c-----------------------------------------------------------------------
c
      integer :: ierr,i,nlines
      character(32) :: input_file
c
c-----------------------------------------------------------------------
c
c ****** Begin the main program.
c
      write (*,*) '### ',code_name,
     &            ' Version ',code_version,
     &            ' of ',code_update,'.'
c
c ****** Check that the input file has the correct number of lines
c ****** to reduce the chance of using an invalid input file.
c
      input_file='mhd1_loop.dat'
c
      call ffopen (1,input_file,'r',ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in ',code_name,':'
        write (*,*) '### Could not read the input file:'
        write (*,*) 'File name: ',trim(input_file)
        call exit (1)
      end if
c
      nlines=0
      do
        read (1,*,iostat=ierr)
        if (ierr.lt.0) exit
        nlines=nlines+1
      enddo
c
      close (1)
c
      if (nlines.ne.input_file_lines_required) then
        write (*,*)
        write (*,*) '### ERROR in ',code_name,':'
        write (*,*) '### The input file does not have the'//
     &              ' correct number of lines:'
        write (*,*) 'Number of lines required = ',
     &              input_file_lines_required
        write (*,*) 'Number of lines present  = ',nlines
        call exit (1)
      end if
c
c ****** Read input parameters.
c
      call ffopen (1,input_file,'r',ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in ',code_name,':'
        write (*,*) '### Could not read the input file:'
        write (*,*) 'File name: ',trim(input_file)
        call exit (1)
      end if
c
      read (1,*) !------------------------------------------------------
      read (1,*) ! Parameters that control the run.
      read (1,*) !------------------------------------------------------
      read (1,*)
      read (1,*) idebug
      read (1,*)
      read (1,*) max_time_steps
      read (1,*)
      read (1,*) tmax
      read (1,*)
      read (1,*) dtmax
      read (1,*)
      read (1,*) dtmax_initial
      read (1,*)
      read (1,*) t_small_dt
      read (1,*) !------------------------------------------------------
      read (1,*) ! Mesh and flux tube geometry.
      read (1,*) !------------------------------------------------------
      read (1,*)
      read (1,*) mesh_file
      read (1,*)
      read (1,*) nsm1
      read (1,*)
      read (1,*) s0
      read (1,*)
      read (1,*) s1
      read (1,*)
      read (1,*) sfrac
      read (1,*)
      read (1,*) dsratio
      read (1,*)
      read (1,*) nfilt_smesh
      read (1,*)
      read (1,*) geometry_file
      read (1,*) !------------------------------------------------------
      read (1,*) ! Initial state parameters.
      read (1,*) !------------------------------------------------------
      read (1,*)
      read (1,*) initial_state_file
      read (1,*)
      read (1,*) hs_t_apex
      read (1,*)
      read (1,*) hs_n_e_base
      read (1,*)
      read (1,*) hs_chromo_thickness
      read (1,*)
      read (1,*) b_uniform
      read (1,*)
      read (1,*) b0_l,blen_l
      read (1,*)
      read (1,*) b0_r,blen_r
      read (1,*) !------------------------------------------------------
      read (1,*) ! Coronal heating parameters.
      read (1,*) !------------------------------------------------------
      read (1,*)
      read (1,*) tcond
      read (1,*)
      read (1,*) t_cutoff
      read (1,*)
      read (1,*) p_cutoff
      read (1,*)
      read (1,*) radloss
      read (1,*)
      read (1,*) rad_law_type
      read (1,*)
      read (1,*) t_chromo
      read (1,*)
      read (1,*) h0_uniform
      read (1,*)
      read (1,*) h0_l_1,hlen_l_1
      read (1,*)
      read (1,*) h0_l_2,hlen_l_2
      read (1,*)
      read (1,*) h_s_offset_l
      read (1,*)
      read (1,*) h0_r_1,hlen_r_1
      read (1,*)
      read (1,*) h0_r_2,hlen_r_2
      read (1,*)
      read (1,*) h_s_offset_r
      read (1,*)
      read (1,*) heating_input_file
      read (1,*)
      read (1,*) extended_heating_model
      read (1,*)
      read (1,*) heating_model_params_file
      read (1,*) !------------------------------------------------------
      read (1,*) ! Viscosity.
      read (1,*) !------------------------------------------------------
      read (1,*)
      read (1,*) use_viscosity
      read (1,*)
      read (1,*) visc_prof%n
      read (1,*)
      read (1,*) (visc_prof%x(i),i=1,min(nodes_max,visc_prof%n))
      read (1,*)
      read (1,*) (visc_prof%f(i),i=1,min(nodes_max,visc_prof%n))
      read (1,*)
      read (1,*) viscosity_input_file
      read (1,*)
      read (1,*) split_visc_advance
      read (1,*) !------------------------------------------------------
      read (1,*) ! Physical parameters.
      read (1,*) !------------------------------------------------------
      read (1,*)
      read (1,*) gamma
      read (1,*)
      read (1,*) use_gravity
      read (1,*)
      read (1,*) g0
      read (1,*)
      read (1,*) he_frac
      read (1,*) !------------------------------------------------------
      read (1,*) ! Input/output.
      read (1,*) !------------------------------------------------------
      read (1,*)
      read (1,*) final_state_file
      read (1,*)
      read (1,*) final_state_phys_units_file
      read (1,*)
      read (1,*) energy_diagnostic_file
      read (1,*)
      read (1,*) momentum_diagnostic_file
      read (1,*)
      read (1,*) viscosity_output_file
      read (1,*)
      read (1,*) mesh_output_file
      read (1,*) !------------------------------------------------------
      read (1,*) ! Time histories and diagnostic output.
      read (1,*) !------------------------------------------------------
      read (1,*)
      read (1,*) hist_int_step
      read (1,*)
      read (1,*) hist_int_time
      read (1,*)
      read (1,*) s_mesh_hist_index
      read (1,*)
      read (1,*) plot_list
      read (1,*)
      read (1,*) plot_int_step
      read (1,*)
      read (1,*) plot_int_time
      read (1,*)
      read (1,*) iprint
      read (1,*) !------------------------------------------------------
      read (1,*) ! Numerical factors.
      read (1,*) !------------------------------------------------------
      read (1,*)
      read (1,*) upwind
      read (1,*)
      read (1,*) use_predictor
      read (1,*)
      read (1,*) use_flow_in_predictor
      read (1,*)
      read (1,*) betapc_v
      read (1,*)
      read (1,*) betapc_t
      read (1,*)
      read (1,*) cfl
      read (1,*)
      read (1,*) semi_implicit
      read (1,*)
      read (1,*) simult
      read (1,*)
      read (1,*) fac_cflv
      read (1,*) !------------------------------------------------------
      read (1,*) ! Boundary conditions.
      read (1,*) !------------------------------------------------------
      read (1,*)
      read (1,*) tbc0type
      read (1,*)
      read (1,*) tbc1type
      read (1,*)
      read (1,*) nbc0type
      read (1,*)
      read (1,*) nbc1type
      read (1,*)
      read (1,*) vbc0type
      read (1,*)
      read (1,*) vbc1type
      read (1,*)
      read (1,*) tbc0
      read (1,*)
      read (1,*) tbc1
      read (1,*)
      read (1,*) ne_target0
      read (1,*)
      read (1,*) ne_target1
      read (1,*)
      read (1,*) ne_advance_time_const
      read (1,*)
      read (1,*) ne_advance_frac_lim
c
      close (1)
c
c ****** Set up the run.
c
      call setup
      call start
c
c ****** Time step loop.
c
      write (*,*)
c
      do
c
        itime=itime+1
        time=time+dt
c
c ****** Set the variables that control collection of histories.
c
        call history_check
c
        if (mod(itime-1,iprint).eq.0) then
          write (*,900) 'it=',itime,',t=',time,',dt=',dt,
     &                  ',Q0=',q0,',Q1=',q1,
     &                  ',M=',tmass
  900     format (a,i8,a,f10.4,a,1pe9.3,2(a,1pe10.4),a,1pe11.5)
        end if
c
        call load_heating
        call advance_t
        call advance_ne
        call set_p
        call advance_v
c
c ****** Set logical flag IFEND=.true. when the run is over.
c
        if (itime.ge.max_time_steps.or.time.ge.tmax) ifend=.true.
c
c ****** Check if the user has requested to stop the run
c ****** prematurely.
c
        call check_if_stoprun
c
c ****** Collect diagnostics.
c
        call diags
c
c ****** Check if the user has requested an intermediate dump of
c ****** the fields that are being stored for final output to
c ****** HDF files.
c
        call check_if_dumphdf
c
c ****** End the run on a recoverable abort (IFABORT=.true.) or
c ****** if the run has completed.
c
        if (ifabort.or.ifend) exit
c
c ****** Set the time step for the next step.
c
        call setdt
c
      enddo
c
c ****** Write spatial diagnostics, time histories, and exit.
c
      call final_diags
c
      end
c#######################################################################
      subroutine setup
c
c-----------------------------------------------------------------------
c
c ****** Set up the run.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use initial_state
      use normalization_parameters
      use heating_parameters
      use params
      use diagnostic_output
      use boundary_conditions
      use file_names
      use lcase_interface
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      integer :: i,j
      real(r_typ) :: t_chromo_plus
c
c-----------------------------------------------------------------------
c
      integer, external :: match
c
c-----------------------------------------------------------------------
c
c ****** Read the file containing the extended heating model
c ****** parameters if it was requested.
c
      if (extended_heating_model) then
        if (heating_model_params_file.ne.' ') then
          call read_heating_params (heating_model_params_file)
        end if
      end if
c
c ****** Read the mesh file if it was specified.
c
c ****** This is done right away since it defines the number
c ****** of mesh points, which is needed to allocate memory.
c
      if (mesh_file.ne.' ') then
        call read_mesh (mesh_file)
        mesh_was_read_in=.true.
      else
        mesh_was_read_in=.false.
      end if
c
c ****** Define the number of points in the s mesh.
c
      ns=nsm1+1
      nsm2=nsm1-1
c
c ****** Allocate memory.
c
      call allocate_memory
c
c ****** Define the normalization variables.
c
      call define_normalization
c
c ****** Convert the lower boundary temperature, TBC0, and the
c ****** upper boundary temperature, TBC1, which
c ****** are read in in degrees K, to normalized units.
c
      tbc0=tbc0/fn_t
      tbc1=tbc1/fn_t
c
c ****** Convert the midpoint loop temperature, HS_T_APEX, which
c ****** is read in in degrees K, to normalized units.
c
      hs_t_apex=hs_t_apex/fn_t
c
c ****** Convert the specified heating rates from physical units
c ****** [erg/cm^3/s] to normalized units.
c
      h0_uniform=h0_uniform/fn_heat
      h0_l_1=h0_l_1/fn_heat
      h0_l_2=h0_l_2/fn_heat
      h0_r_1=h0_r_1/fn_heat
      h0_r_2=h0_r_2/fn_heat
c
c ****** Convert the history time interval and the plotting time
c ****** interval from seconds to code units.
c
      hist_int_time=hist_int_time/fnormt
      plot_int_time=plot_int_time/fnormt
c
c ****** Initialize the radiative loss law.
c
c ****** Use a modification that sets Q to zero at the base of the
c ****** chromosphere (assumed to be at temperature T_CHROMO in
c ****** degrees [K]).  Q is set to zero for temperatures below
c ****** T_CHROMO.  The modification is carried out for temperatures
c ****** below T_CHROMO_PLUS (in degrees [K]).
c
      t_chromo_plus=1.5_r_typ*t_chromo
c
      call initialize_qrad (rad_law_type,.true.,
     &                      t_chromo,t_chromo_plus)
c
c ****** If extended heating is being used, check that the heating
c ****** model requested is valid.
c
      if (extended_heating_model) then
c
        heating_model_index=0
        do i=1,n_heat_model
          if (lcase(heating_model_type).eq.
     &        lcase(heat_model_name(i))) then
            heating_model_index=i
            exit
          end if
        enddo
c
        if (heating_model_index.eq.0) then
          write (*,*)
          write (*,*) '### ERROR in SETUP:'
          write (*,*) '### The requested heating model is not'//
     &                ' valid: ',trim(heating_model_type)
          write (*,*)
          write (*,*) '### The heating model must be one'//
     &                ' of the following:'
          do i=1,n_heat_model
            write (*,*) ''''//trim(heat_model_name(i))//''''
          enddo
          call exit (1)
        end if
c
        write (*,*)
        write (*,*) '### Heating model selected: ',
     &              trim(heating_model_type)
c
      end if
c
c ****** Build the list of diagnostic quantities to plot.
c
      nplot=0
      do i=1,n_diag_fields
        j=match(plot_list(i),n_diag_fields,diag_fld_name)
        if (j.gt.0) then
          nplot=nplot+1
          plot_index(nplot)=j
        end if
      enddo
c
      if (nplot.gt.0) then
        write (*,*)
        write (*,*) '### Quantities to plot:'
        do i=1,nplot
          write (*,*) trim(diag_fld_name(plot_index(i)))
        enddo
      end if
c
c ****** Check that the requested boundary conditions are valid.
c
c ****** Boundary conditions on temperature.
c
      if (tbc0type.eq.'isolated') then
        ibc_t_0=IBC_ISOLATED
      else if (tbc0type.eq.'fixed') then
        ibc_t_0=IBC_FIXED
      else if (tbc0type.eq.'rad_balance') then
        ibc_t_0=IBC_FIXED
        set_rad_balance_t0=.true.
      else
        call undefined_bc_type ('TBC0TYPE',tbc0type)
      end if
c
      if (tbc1type.eq.'isolated') then
        ibc_t_1=IBC_ISOLATED
      else if (tbc1type.eq.'fixed') then
        ibc_t_1=IBC_FIXED
      else if (tbc1type.eq.'rad_balance') then
        ibc_t_1=IBC_FIXED
        set_rad_balance_t1=.true.
      else
        call undefined_bc_type ('TBC1TYPE',tbc1type)
      end if
c
c ****** Boundary conditions on density.
c
      if (nbc0type.eq.'isolated') then
        ibc_ne_0=IBC_ISOLATED
      else if (nbc0type.eq.'fixed') then
        ibc_ne_0=IBC_FIXED
      else if (nbc0type.eq.'radbc') then
        ibc_ne_0=IBC_RADBC
      else
        call undefined_bc_type ('NBC0TYPE',nbc0type)
      end if
c
      if (nbc1type.eq.'isolated') then
        ibc_ne_1=IBC_ISOLATED
      else if (nbc1type.eq.'fixed') then
        ibc_ne_1=IBC_FIXED
      else if (nbc1type.eq.'radbc') then
        ibc_ne_1=IBC_RADBC
      else
        call undefined_bc_type ('NBC1TYPE',nbc1type)
      end if
c
c ****** Boundary conditions on velocity.
c
      if (vbc0type.eq.'zero') then
        ibc_v_0=IBC_ZERO
      else if (vbc0type.eq.'charbc') then
        ibc_v_0=IBC_CHARBC
      else
        call undefined_bc_type ('VBC0TYPE',vbc0type)
      end if
c
      if (vbc1type.eq.'zero') then
        ibc_v_1=IBC_ZERO
      else if (vbc1type.eq.'charbc') then
        ibc_v_1=IBC_CHARBC
      else
        call undefined_bc_type ('VBC1TYPE',vbc1type)
      end if
c
      return
      end
c#######################################################################
      subroutine start
c
c-----------------------------------------------------------------------
c
c ****** Initialization of the run.
c
c-----------------------------------------------------------------------
c
      use globals
      use mesh
      use initial_state
      use params
      use diagnostic_output
      use file_names
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
c ****** Set the flux tube geometry.
c
      if (geometry_file.ne.' ') then
        call read_flux_tube_geometry (geometry_file)
        call set_flux_tube_length
      else
        call set_circular_flux_tube
      end if
c
c ****** If the mesh was not read in, generate the mesh using the
c ****** specified mesh parameters.
c
      if (.not.mesh_was_read_in) then
        call generate_s_mesh
      end if
c
c ****** Calculate mesh quantities.
c
      call calculate_mesh_quantities
c
c ****** Derive the flux tube geometry parameters.
c
      call set_flux_tube_geometry_params
c
c ****** Initialize the fields.
c
      call initialize_fields (initial_state_file)
c
c ****** Load the heating.
c
      call load_heating
c
c ****** Set the temperature at the boundaries that balances
c ****** heating with radiative loss at the loop ends.
c ****** Note that this is only done once for the initial heating
c ****** profile.
c
      call set_rad_balance_temp
c
c ****** Set the initial time step.
c
      call setdt
c
c ****** Initialize the diagnostics.
c
      call history_check
c
c ****** Collect histories at t=0.
c
      if (hist_int_step.gt.0.or.hist_int_time.gt.0.) then
        s_mesh_hist_index=max(1,s_mesh_hist_index)
        s_mesh_hist_index=min(nsm1,s_mesh_hist_index)
        write (*,*)
        write (*,*) '### Collecting time histories at s = ',
     &              s(s_mesh_hist_index)
      end if
c
c ****** Initialize the total mass in the loop.
c
      call total_mass
c
c ****** Collect and write diagnostics.
c
      call diags
c
      return
      end
c#######################################################################
      function match (keyword,n,table)
c
c-----------------------------------------------------------------------
c
c ****** Match KEYWORD to the list of N words in TABLE.
c
c ****** A successful match returns the index of the first matched
c ****** entry in TABLE; an unsuccessful match returns 0.
c
c-----------------------------------------------------------------------
c
      use lcase_interface
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      character(*), intent(in) :: keyword
      integer, intent(in) :: n
      character(*), dimension(n), intent(in) :: table
      integer :: match
c
c-----------------------------------------------------------------------
c
      integer :: i
c
c-----------------------------------------------------------------------
c
      match=0
c
      do i=1,n
        if (lcase(keyword).eq.lcase(table(i))) then
          match=i
          return
        end if
      end do
c
      return
      end
c#######################################################################
      subroutine undefined_bc_type (type,value)
c
c-----------------------------------------------------------------------
c
c ****** Stop with an error condition because the boundary condition
c ****** type TYPE with value VALUE is not defined.
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      character(*) :: type,value
c
c-----------------------------------------------------------------------
c
      write (*,*)
      write (*,*) '### ERROR in UNDEFINED_BC_TYPE:'
      write (*,*) '### The boundary condition type is undefined.'
      write (*,*) 'Boundary condition type:  ',type
      write (*,*) 'Boundary condition value: ',value
      call exit (1)
c
      end
c#######################################################################
      subroutine allocate_memory
c
c-----------------------------------------------------------------------
c
c ****** Allocate memory for the main arrays.
c
c-----------------------------------------------------------------------
c
      use globals
      use params
      use mesh
      use fields
      use si_term_velocities
      use energy_diagnostics
      use momentum_diagnostics
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      if (.not.mesh_was_read_in) then
        allocate (s(nsm1))
      end if
c
      allocate (ds(nsm1))
      allocate (sh(ns))
      allocate (dsh(ns))
c
      allocate (z(nsm1))
      allocate (zh(ns))
c
      allocate (area(nsm1))
      allocate (areah(ns))
c
      allocate (g(nsm1))
c
      allocate (p(ns))
      allocate (ne(ns))
      allocate (rho(ns))
      allocate (v(nsm1))
      allocate (temp(ns))
      allocate (b(ns))
c
      allocate (kappa(ns))
c
      allocate (heating(ns))
      allocate (heating_static(ns))
c
      allocate (visc(nsm1))
      allocate (sifac(nsm1))
c
c ****** Allocate memory for the semi-implicit term intermediate
c ****** velocities.
c
      allocate (v_si_old1(nsm1))
      allocate (v_si_old2(nsm1))
c
c ****** Allocate memory for the energy diagnostic terms.
c
      allocate (h_h(ns))
      allocate (h_qr(ns))
      allocate (h_qc(ns))
      allocate (h_pv(ns))
      allocate (h_q_tc(nsm1))
c
c ****** Allocate memory for the momentum diagnostic terms.
c
      allocate (vdgv(nsm1))
      allocate (delsq(nsm1))
      allocate (v_previous_step(nsm1))
c
      return
      end
c#######################################################################
      subroutine define_normalization
c
c-----------------------------------------------------------------------
c
c ****** Define the quantities that are used to normalize variables.
c
c-----------------------------------------------------------------------
c
      use number_types
      use normalization_parameters
      use heating_parameters
      use params
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: two=2._r_typ
      real(r_typ), parameter :: three=3._r_typ
      real(r_typ), parameter :: four=4._r_typ
      real(r_typ), parameter :: seven=7._r_typ
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
c ****** Set the parameters related to the Helium fraction, HE_FRAC.
c ****** HE_FRAC is defined by n_alpha=HE_FRAC*n_p,
c ****** where n_alpha is the Helium number density and
c ****** n_p is the proton number density.
c
c ****** HE_RHO is defined by rho(norm)=HE_RHO*n_e(norm),
c ****** where n_e is the electron number density.
c
      he_rho=(one+four*he_frac)/(one+two*he_frac)
c
c ****** HE_P is defined by p(norm)=HE_P*n_e(norm)*T(norm),
c ****** where p is the total pressure.
c
      he_p=(two+three*he_frac)/(one+two*he_frac)
c
c ****** HE_NP is defined by n_p(norm)=HE_NP*n_e(norm).
c
      he_np=one/(one+two*he_frac)
c
c ****** Define the normalization variables.
c
      fnorml=rsun
      fnormt=sqrt(g0*fnorml/g0phys)
      fnormm=fn0phys*m_proton*fnorml**3
c
c ****** Define the factors to normalize various physical
c ****** quantities. The physical quantity is expressed in terms
c ****** of the normalized quantity by:
c ****** Q(physical) = FN_Q * Q(normalized).
c
c ****** Number density normalization [/cm^3].
c
      fn_n=fn0phys
c
c ****** Mass density normalization [g/cm^3].
c
      fn_rho=m_proton*fn0phys
c
c ****** Pressure normalization [erg/cm^3].
c
      fn_p=m_proton*fn0phys*(fnorml/fnormt)**2
c
c ****** Temperature normalization [K].
c
      fn_t=m_proton*fnorml**2/(boltz*fnormt**2)
c
c ****** Velocity normalization [cm/s].
c
      fn_v=fnorml/fnormt
c
c ****** Magnetic field normalization [Gauss].
c
      fn_b=sqrt(four*pi*fn_rho)*fnorml/fnormt
c
c ****** Radiative loss function Q(T) normalization [erg-cm^3/s].
c
      fn_qrad=m_proton*fnorml**2/(fn0phys*fnormt**3)
c
c ****** Thermal conductivity normalization [erg/cm/s/K].
c
      fn_kappa=boltz*fn0phys*fnorml**2/fnormt
c
c ****** Surface heat flux normalization [ergs/cm^2/s].
c
      fn_q0=fnormm/fnormt**3
c
c ****** Volumetric heating normalization [ergs/cm^3/s].
c
      fn_heat=fn_q0/fnorml
c
c ****** Magnitude of the Spitzer parallel electron thermal
c ****** conductivity, kappa = KAPPA0_SPITZER*T^2.5 [erg/s/cm/K],
c ****** where T is in degrees Kelvin.
c
      kappa0_spitzer=9.e-7_r_typ
c
c ****** Radiative balance boundary condition constants.
c
      fn_radbc1=half*tcond*kappa0_spitzer*fn_t**2
     &          /(he_np*fnorml**2*fn0phys**2)
      fn_radbc2=(two/seven)*(fnormm/fnormt**3/fnorml)
     &          /(he_np*fn0phys**2)
c
      return
      end
c#######################################################################
      subroutine final_diags
c
c-----------------------------------------------------------------------
c
c ****** Write out final diagnostics and finish up the run.
c
c-----------------------------------------------------------------------
c
      use params
      use diagnostic_output
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      logical, save :: final_diags_has_been_called=.false.
c
c-----------------------------------------------------------------------
c
      if (final_diags_has_been_called) go to 100
c
      final_diags_has_been_called=.true.
c
c ****** Collect spatial diagnostics (unless just collected or if
c ****** the run has been aborted).
c
      if (plot_int_step.gt.0.or.plot_int_time.gt.0.) then
        if ((.not.ifabort).and.(.not.plotting_step)) then
          call save_dump_fields
        end if
      end if
c
c ****** Write the time histories.
c
      call write_histories
c
  100 continue
c
      call finish
c
      end
c#######################################################################
      subroutine finish
c
c-----------------------------------------------------------------------
c
c ****** Write the final state, write the saved dump fields,
c ****** and finish up.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use params
      use mesh
      use fields
      use file_names
      use normalization_parameters
      use diagnostic_output
      use energy_diagnostics
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i,ierr
      character(32) :: out_file
      real(r_typ) :: n_e_av,t_av,p_av,b_av,h_av
      real(r_typ) :: s_phys,v_phys,q_tc
c
c-----------------------------------------------------------------------
c
c ****** Write the final state to files MHD1.OUT and MHD2.OUT.
c ****** These can be used in a restart.
c
      write (*,*)
c
      out_file='mhd1.out'
c
      call ffopen (1,out_file,'rw',ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in FINISH:'
        write (*,*) '### Could not create an output file:'
        write (*,*) 'File name: ',trim(out_file)
        call exit (1)
      end if
c
      write (1,900) 'sh' ,char(9),
     &              'n_e',char(9),
     &              'T'  ,char(9),
     &              'p'  ,char(9),
     &              'B'  ,char(9),
     &              'A'  ,char(9),
     &              'H'
      do i=1,ns
        write (1,910) sh(i)     ,char(9),
     &                ne(i)     ,char(9),
     &                temp(i)   ,char(9),
     &                p(i)      ,char(9),
     &                b(i)      ,char(9),
     &                areah(i)  ,char(9),
     &                heating(i)
      enddo
c
      close (1)
c
      write (*,*) '### Wrote file: ',trim(out_file)
c
      out_file='mhd2.out'
c
      call ffopen (1,out_file,'rw',ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in FINISH:'
        write (*,*) '### Could not create an output file:'
        write (*,*) 'File name: ',trim(out_file)
        call exit (1)
      end if
c
      write (1,900) 's',char(9),
     &              'v',char(9),
     &              'A'
      do i=1,nsm1
        write (1,910) s(i)   ,char(9),
     &                v(i)   ,char(9),
     &                area(i)
      enddo
c
      close (1)
c
      write (*,*) '### Wrote file: ',trim(out_file)
c
c ****** Write the final state (in normalized units) to file
c ****** FINAL_STATE_FILE if requested.
c
      if (final_state_file.ne.' ') then
c
        call ffopen (1,final_state_file,'rw',ierr)
c
        if (ierr.ne.0) then
          write (*,*)
          write (*,*) '### ERROR in FINISH:'
          write (*,*) '### Could not create an output file:'
          write (*,*) 'File name: ',trim(final_state_file)
          call exit (1)
        end if
c
        write (1,900) 's'  ,char(9),
     &                'v'  ,char(9),
     &                'n_e',char(9),
     &                'T'  ,char(9),
     &                'p'  ,char(9),
     &                'B'  ,char(9),
     &                'A'  ,char(9),
     &                'H'
        do i=1,nsm1
          n_e_av=half*(ne(i)+ne(i+1))
          t_av=half*(temp(i)+temp(i+1))
          p_av=half*(p(i)+p(i+1))
          b_av=half*(b(i)+b(i+1))
          h_av=half*(heating(i)+heating(i+1))
          write (1,910) s(i)   ,char(9),
     &                  v(i)   ,char(9),
     &                  n_e_av ,char(9),
     &                  t_av   ,char(9),
     &                  p_av   ,char(9),
     &                  b_av   ,char(9),
     &                  area(i),char(9),
     &                  h_av
        enddo
c
        close (1)
c
        write (*,*) '### Wrote the final state (normalized'//
     &              ' units) to file: ',
     &              trim(final_state_file)
c
      end if
c
c ****** Write the final state (in physical units) to file
c ****** FINAL_STATE_PHYS_UNITS_FILE if requested.
c
      if (final_state_phys_units_file.ne.' ') then
c
        call ffopen (1,final_state_phys_units_file,'rw',ierr)
c
        if (ierr.ne.0) then
          write (*,*)
          write (*,*) '### ERROR in FINISH:'
          write (*,*) '### Could not create an output file:'
          write (*,*) 'File name: ',
     &                trim(final_state_phys_units_file)
          call exit (1)
        end if
c
        write (1,900) 's_[Mm]'        ,char(9),
     &                'v_[km/s]'      ,char(9),
     &                'n_e_[/cm^3]'   ,char(9),
     &                'T_[MK]'        ,char(9),
     &                'p_[dyn/cm^2]'  ,char(9),
     &                'B_[G]'         ,char(9),
     &                'A'             ,char(9),
     &                'H_[erg/cm^3/s]'
        do i=1,nsm1
          s_phys=s(i)*fnorml*1.e-8_r_typ
          v_phys=v(i)*fn_v*1.e-5_r_typ
          n_e_av=half*(ne(i)+ne(i+1))*fn_n
          t_av=half*(temp(i)+temp(i+1))*fn_t*1.e-6_r_typ
          p_av=half*(p(i)+p(i+1))*fn_p
          b_av=half*(b(i)+b(i+1))*fn_b
          h_av=half*(heating(i)+heating(i+1))*fn_heat
          write (1,910) s_phys ,char(9),
     &                  v_phys ,char(9),
     &                  n_e_av ,char(9),
     &                  t_av   ,char(9),
     &                  p_av   ,char(9),
     &                  b_av   ,char(9),
     &                  area(i),char(9),
     &                  h_av
        enddo
c
        close (1)
c
        write (*,*) '### Wrote the final state (physical'//
     &              ' units) to file: ',
     &              trim(final_state_phys_units_file)
c
      end if
c
c ****** Get the terms in the energy diagnostic unless the run
c ****** has been aborted.
c
      if (.not.ifabort) call get_energy_terms
c
c ****** Write the energy diagnostic terms to ENERGY_DIAGNOSTIC_FILE
c ****** if requested.
c
      if (.not.ifabort.and.energy_diagnostic_file.ne.' ') then
c
        call ffopen (1,energy_diagnostic_file,'rw',ierr)
c
        if (ierr.ne.0) then
          write (*,*)
          write (*,*) '### ERROR in FINISH:'
          write (*,*) '### Could not create an output file:'
          write (*,*) 'File name: ',
     &                trim(energy_diagnostic_file)
          call exit (1)
        end if
c
        write (1,900) 'sh_[Mm]'          ,char(9),
     &                'A'                ,char(9),
     &                'H_[erg/cm^3/s]'   ,char(9),
     &                'Qc_[erg/cm^3/s]'  ,char(9),
     &                'Qr_[erg/cm^3/s]'  ,char(9),
     &                'pv_[erg/cm^3/s]'  ,char(9),
     &                'q_tc_[erg/cm^2/s]'
        do i=1,ns
          if (i.eq.1) then
            q_tc=h_q_tc(   1)
          else if (i.eq.ns) then
            q_tc=h_q_tc(nsm1)
          else
            q_tc=half*(h_q_tc(i)+h_q_tc(i-1))
          end if
          s_phys=sh(i)*fnorml*1.e-8_r_typ
          write (1,910) s_phys  ,char(9),
     &                  areah(i),char(9),
     &                  h_h(i)  ,char(9),
     &                  h_qc(i) ,char(9),
     &                  h_qr(i) ,char(9),
     &                  h_pv(i) ,char(9),
     &                  q_tc
        enddo
c
        close (1)
c
        write (*,*) '### Wrote the energy diagnostic to file: ',
     &              trim(energy_diagnostic_file)
c
      end if
c
c ****** Write energy flow diagnostics unless the run has been
c ****** aborted.
c
      if (.not.ifabort) call energy_flow_diagnostic
c
c ****** Write momentum diagnostics unless the run has been
c ****** aborted.
c
      if (.not.ifabort) call momentum_diagnostic
c
c ****** Write the fields from the dump buffer.
c
      if (nplot.gt.0) call write_fields_txt
c
      if (.not.ifabort) then
        write (*,*)
        write (*,*) '### The run completed successfully.'
        call exit (0)
      else
        call exit (1)
      end if
c
  900 format (10(a,a))
  910 format (10(1pe21.14,a))
c
      end
c#######################################################################
      subroutine check_if_stoprun
c
c-----------------------------------------------------------------------
c
c ****** Stop the present run if a file named "STOPRUN" exists in
c ****** the run directory.
c
c ****** If it does, set the flag IFEND=.true. and return.
c
c-----------------------------------------------------------------------
c
      use params
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      logical :: exists
      integer :: ierr
c
c-----------------------------------------------------------------------
c
c ****** Check if the user has requested to stop the run
c ****** prematurely.
c
      inquire (file='STOPRUN',exist=exists)
      if (exists) then
        write (*,*)
        write (*,*) '### WARNING from MHD1_LOOP:'
        write (*,*) '### The run was stopped prematurely'
     &              //' via STOPRUN ...'
        write (*,*) 'ITIME = ',itime
        write (*,*) 'TIME = ',time
        ifend=.true.
      end if
c
      return
      end
c#######################################################################
      subroutine check_if_dumphdf
c
c-----------------------------------------------------------------------
c
c ****** Dump the fields that are being collected in the HDF
c ****** buffers if a file "DUMPHDF" exists in the run directory.
c
c ****** If it does, dump the fields to HDF files, and delete the
c ****** file "DUMPHDF".
c
c ****** This does not interrupt the collection of the fields in
c ****** the HDF buffers.  The files will be overwritten at the
c ****** end of the run, as usual.
c
c-----------------------------------------------------------------------
c
      use params
      use diagnostic_output
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      logical :: exists
      integer :: ierr
c
c-----------------------------------------------------------------------
c
c ****** Check if the user has requested an intermediate dump of
c ****** the fields that are being stored for final output to
c ****** HDF files.
c
      inquire (file='DUMPHDF',exist=exists)
      if (exists) then
        if (nplot.gt.0) then
          write (*,*)
          write (*,*) '### WARNING from MHD1_LOOP:'
          write (*,*) '### A dump of the HDF fields'
     &                //' was requested via DUMPHDF ...'
          write (*,*) 'ITIME = ',itime
          write (*,*) 'TIME = ',time
          call write_fields_txt
          write (*,*)
          open (1,file='DUMPHDF',status='old')
          close (1,status='delete')
        end if
      end if
c
      return
      end
c#######################################################################
      subroutine history_check
c
c-----------------------------------------------------------------------
c
c ****** Set the switches for collection of time histories and the
c ****** plotting of spatial diagnostics.
c
c-----------------------------------------------------------------------
c
      use number_types
      use diagnostic_output
      use params
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      logical, save :: first_call=.true.
c
c-----------------------------------------------------------------------
c
      logical, external :: diagnostic_step
c
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
c ****** Set the switch for time history diagnostics.
c-----------------------------------------------------------------------
c
c ****** If HIST_INT_STEP > 0, collect time histories every
c ****** HIST_INT_STEP time steps.  Otherwise, if HIST_INT_TIME > 0,
c ****** collect time histories every HIST_INT_TIME intervals of
c ****** time.
c
      history_step=.false.
c
      if (hist_int_step.le.0) then
        if (first_call.and.hist_int_time.gt.0.) then
          history_step=.true.
        else
          history_step=diagnostic_step(t_last_check,
     &                                 hist_int_time,
     &                                 time)
        end if
      else
        if (mod(itime,hist_int_step).eq.0) history_step=.true.
      end if
c
c-----------------------------------------------------------------------
c ****** Set the switch for spatial diagnostics.
c-----------------------------------------------------------------------
c
c ****** If PLOT_INT_STEP > 0, plot spatial diagnostics every
c ****** PLOT_INT_STEP time steps.  Otherwise, if PLOT_INT_TIME > 0,
c ****** plot spatial diagnostics every PLOT_INT_TIME intervals of
c ****** time.
c
      plotting_step=.false.
c
      if (plot_int_step.le.0) then
        if (first_call.and.plot_int_time.gt.0.) then
          plotting_step=.true.
        else
          plotting_step=diagnostic_step(t_last_check,
     &                                 plot_int_time,
     &                                 time)
        end if
      else
        if (mod(itime,plot_int_step).eq.0) plotting_step=.true.
      end if
c
c ****** Save the present TIME in T_LAST_CHECK.
c
      t_last_check=time
c
      if (first_call) first_call=.false.
c
      return
      end
c#######################################################################
      function diagnostic_step (t_previous,t_interval,t)
c
c-----------------------------------------------------------------------
c
c ****** Check if T corresponds to a diagnostic step, based on the
c ****** values of T_PREVIOUS and T_INTERVAL.
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: t_previous,t_interval,t
      logical :: diagnostic_step
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: tdump
c
c-----------------------------------------------------------------------
c
      diagnostic_step=.false.
c
      if (t_interval.le.0.) return
c
      tdump=t-mod(t,t_interval)
c
      if (t.ge.tdump.and.tdump.gt.t_previous) diagnostic_step=.true.
c
      return
      end
c#######################################################################
      subroutine diags
c
c-----------------------------------------------------------------------
c
c ****** If requested, collect time diagnostics and plot spatial
c ****** diagnostics.
c
c-----------------------------------------------------------------------
c
      use diagnostic_output
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
c ****** Collect time history diagnostics.
c
      if (history_step) call collect_histories
c
c ****** Plot spatial diagnostics.
c
      if (plotting_step) call save_dump_fields
c
      return
      end
c#######################################################################
      subroutine read_heating_params (fname)
c
c-----------------------------------------------------------------------
c
c ****** Read the extended heating parameters from the file
c ****** named FNAME.
c
c-----------------------------------------------------------------------
c
      use heating_parameters
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      character(*) :: fname
c
c-----------------------------------------------------------------------
c
      integer :: ierr
c
c-----------------------------------------------------------------------
c
      write (*,*)
      write (*,*) '### Reading the extended heating parameters'//
     &            ' from file: ',trim(fname)
c
      call ffopen (1,fname,'r',ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in READ_HEATING_PARAMS:'
        write (*,*) '### Could not open the extended heating'//
     &              ' model parameters file:'
        write (*,*) 'File name: ',trim(fname)
        call exit (1)
      end if
c
      read (1,heating_params)
c
      close (1)
c
      return
      end
c#######################################################################
      subroutine read_mesh (fname)
c
c-----------------------------------------------------------------------
c
c ****** Read in the s mesh from file FNAME.
c
c-----------------------------------------------------------------------
c
      use globals
      use mesh
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      character(*) :: fname
c
c-----------------------------------------------------------------------
c
      integer :: i,ierr
c
c-----------------------------------------------------------------------
c
c ****** Scan the file to determine the number of points in the table.
c
      call scan (fname,1,nsm1,ierr)
c
      if (ierr.ne.0) then
        i=0
        go to 900
      end if
c
c ****** Allocate memory for the main s mesh.
c
      allocate (s(nsm1))
c
c ****** Read the mesh.
c
      call ffopen (1,fname,'r',ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in READ_MESH:'
        write (*,*) '### Could not open the mesh file:'
        write (*,*) 'File name: ',trim(fname)
        call exit (1)
      end if
c
c ****** Skip the header.
c
      read (1,*)
c
      do i=1,nsm1
        read (1,*,err=900,end=900) s(i)
      enddo
c
      close (1)
c
c ****** Set the mesh limits.
c
      s0=s(1)
      s1=s(nsm1)
c
c ****** Check the validity of the mesh.
c
      call check_mesh_validity
c
      return
c
  900 continue
c
      write (*,*)
      write (*,*) '### ERROR in READ_MESH:'
      write (*,*) '### An error occurred while reading the mesh.'
      write (*,*) 'File name: ',trim(fname)
      write (*,*) 'Line number: ',i
      call exit (1)
c
      end
c#######################################################################
      subroutine check_mesh_validity
c
c-----------------------------------------------------------------------
c
c ****** Check the validity of the mesh intervals S0 and S1.
c
c-----------------------------------------------------------------------
c
      use globals
      use mesh
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
c ****** Check the validity of the mesh specification.
c
      if (s0.lt.0..or.s1.lt.0.) then
        write (*,*)
        write (*,*) '### ERROR in CHECK_MESH_VALIDITY:'
        write (*,*) '### The S mesh limits S0 and S1 are invalid.'
        write (*,*) '### S0 and S1 must be positive.'
        write (*,*) 'S0 = ',s0
        write (*,*) 'S1 = ',s1
        call exit (1)
      end if
c
      if (s1.le.s0) then
        write (*,*)
        write (*,*) '### ERROR in CHECK_MESH_VALIDITY:'
        write (*,*) '### The S mesh limits S0 and S1 are invalid.'
        write (*,*) '### S1 must exceed S0.'
        write (*,*) 'S0 = ',s0
        write (*,*) 'S1 = ',s1
        call exit (1)
      end if
c
c ****** Set the mesh interval.
c
      sl=s1-s0
c
c ****** Write the flux tube length.
c
      write (*,*)
      write (*,*) 'Loop length [Rs] = ',sl
c
      return
      end
c#######################################################################
      subroutine generate_s_mesh
c
c-----------------------------------------------------------------------
c
c ****** Generate the mesh from the specification given by
c ****** NSM1, S0, S1, DSRATIO, SFRAC, and NFILT_SMESH.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use meshdef
      use file_names
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: zero=0._r_typ
c
c-----------------------------------------------------------------------
c
c ****** Check the validity of the mesh.
c
      if (s1.le.s0) then
        write (*,*)
        write (*,*) '### ERROR in GENERATE_S_MESH:'
        write (*,*) '### The specified mesh limits are invalid:'
        write (*,*) 'S1 must exceed S0.'
        write (*,*) 'S0 = ',s0
        write (*,*) 'S1 = ',s1
        call exit (1)
      end if
c
c ****** Set the mesh interval.
c
      sl=s1-s0
c
c ****** Generate the s mesh.
c
      call genmesh (0,'s',nsm1,s0,s1,n_seg_max,sfrac,dsratio,
     &              nfilt_smesh,.false.,zero,s)
c
c ****** Write the s mesh to a text file if requested.
c
      if (mesh_output_file.ne.' ') then
        write (*,*)
        write (*,*) '### Writing the s mesh to file: ',
     &            trim(mesh_output_file)
        call write_mesh (mesh_output_file,'s',nsm1,.false.,s)
      end if
c
      return
      end
c#######################################################################
      subroutine calculate_mesh_quantities
c
c-----------------------------------------------------------------------
c
c ****** Calculate the mesh quantities that are derived from the
c ****** main s mesh.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
c
c-----------------------------------------------------------------------
c
      do i=2,nsm1
        sh(i)=half*(s(i)+s(i-1))
        dsh(i)=s(i)-s(i-1)
      enddo
c
      sh(1)=sh(2)-dsh(2)
      sh(ns)=sh(nsm1)+dsh(nsm1)
c
      dsh(1)=dsh(2)
      dsh(ns)=dsh(nsm1)
c
      do i=1,nsm1
        ds(i)=sh(i+1)-sh(i)
      enddo
c
      return
      end
c#######################################################################
      subroutine scan (fname,nd,n,ierr)
c
c-----------------------------------------------------------------------
c
c ****** Scan the input file of ND-dimensional coordinates to
c ****** determine how many points have been specified.
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      character(*) :: fname
      integer :: nd,n,ierr
c
c-----------------------------------------------------------------------
c
      integer :: i,ierr2
      real(r_typ) :: x
c
c-----------------------------------------------------------------------
c
      ierr=0
c
      if (nd.le.0) then
        write (*,*)
        write (*,*) '### ERROR in SCAN:'
        write (*,*) '### Invalid number of coordinates specified.'
        write (*,*) 'Number of coordinates = ',nd
        write (*,*) 'File name: ',trim(fname)
        call exit (1)
      end if
c
      call ffopen (1,fname,'r',ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in SCAN:'
        write (*,*) '### Could not open the requested file:'
        write (*,*) 'File name: ',trim(fname)
        go to 900
      end if
c
c ****** Skip the header.
c
      read (1,*,err=900,end=900)
c
c ****** Find the number of points specified.
c
      n=0
      do
        read (1,*,iostat=ierr2) (x,i=1,nd)
        if (ierr2.lt.0) exit
        n=n+1
      enddo
c
      close (1)
c
      return
c
c ****** Error exit: invalid format.
c
  900 continue
c
      ierr=1
c
      close (1)
c
      return
      end
c#######################################################################
      subroutine genmesh (io,label,nc,c0,c1,nseg,frac,dratio,
     &                    nfilt,periodic,c_shift,c)
c
c-----------------------------------------------------------------------
c
c ****** Generate a one-dimensional mesh.
c
c-----------------------------------------------------------------------
c
c ****** Input arguments:
c
c          IO      : [integer]
c                    Fortran file unit number to which to write
c                    mesh diagnostics.  Set IO=0 if diagnostics
c                    are not of interest.  It is assumed that
c                    unit IO has been connected to a file prior
c                    to calling this routine.
c
c          LABEL   : [character(*)]
c                    Name for the mesh coordinate (example: 'x').
c
c          NC      : [integer]
c                    Number of mesh points to load.
c
c          C0      : [real]
c                    The starting location for the coordinate.
c
c          C1      : [real]
c                    The ending location for the coordinate.
c                    It is required that C1.gt.C0.
c
c          NSEG    : [integer]
c                    Maximum number of mesh segments.
c                    The mesh spacing in each segment varies
c                    exponentially with a uniform amplification
c                    factor.  The actual number of mesh segments
c                    used is NSEG or less.  It is obtained from the
c                    information in array FRAC.
c
c          FRAC    : [real array, dimension NSEG]
c                    The normalized positions of the mesh segment
c                    boundaries (as a fraction of the size of the
c                    domain).  For a non-periodic mesh, the first
c                    value of FRAC specified must equal 0. and the
c                    last value must equal 1.  For a periodic mesh,
c                    FRAC must not contain both 0. and 1., since
c                    these represent the same point.
c
c          DRATIO  : [real array, dimension NSEG]
c                    The ratio of the mesh spacing at the end of a
c                    segment to that at the beginning.
c
c          NFILT   : [integer]
c                    The number of times to filter the mesh-point
c                    distribution array.  Set NFILT=0 if filtering
c                    is not desired.  Filtering can reduce
c                    discontinuities in the derivative of the mesh
c                    spacing.
c
c          PERIODIC: [logical]
c                    A flag to indicate whether the mesh to be
c                    generated represents a periodic coordinate.
c                    If the coordinate is specified as periodic,
c                    the range [C0,C1] should be the whole periodic
c                    interval; the first mesh point is set at C0
c                    and the last mesh point, C(NC), is set at C1.
c
c          C_SHIFT : [real]
c                    Amount by which to shift the periodic coordinate.
c                    C_SHIFT is only used when PERIODIC=.true.,
c                    and is ignored otherwise.  A positive C_SHIFT
c                    moves the mesh points to the right.
c
c ****** Output arguments:
c
c          C       : [real array, dimension NC]
c                    The locations of the mesh points.
c
c-----------------------------------------------------------------------
c
c ****** The arrays DRATIO and FRAC define the mesh as follows.
c
c ****** For example, suppose that a (non-periodic) mesh with three
c ****** segments is desired.  Suppose the domain size is c=[0:2].
c ****** In the first segment (with c between 0 and .5) the mesh
c ****** spacing is decreasing with c, such that DC at c=.5 is half
c ****** DC at c=0.  From c=.5 to c=1, the mesh is uniform.  From c=1
c ****** to c=2, the mesh spacing is increasing with c such that DC at
c ****** c=2 is 10 times DC at c=1.  This mesh would be specified by:
c ******
c ******     FRAC=0.,.25,.5,1.
c ******     DRATIO=.5,1.,10.
c ******
c ****** The variable C_SHIFT can be used to shift the mesh point
c ****** distribution for a periodic coordinate.  For example,
c ****** suppose C represents mesh points in the interval [0,2*pi].
c ****** C_SHIFT=.5*pi would move the distribution of mesh points
c ****** so that the original mesh point with C(1)=0. would be
c ****** close to .5*pi in the new mesh.
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      integer, intent(in) :: io
      character(*), intent(in) :: label
      integer, intent(in) :: nc
      real(r_typ), intent(in) :: c0,c1
      integer, intent(in) :: nseg
      real(r_typ), dimension(nseg), intent(in) :: frac,dratio
      integer, intent(in) :: nfilt
      logical, intent(in) :: periodic
      real(r_typ), intent(in) :: c_shift
      real(r_typ), dimension(nc), intent(out) :: c
c
c-----------------------------------------------------------------------
c
c ****** Storage for the coordinate transformation.
c
      integer :: ns
      real(r_typ), dimension(:), allocatable :: xi,cs,a,r,f
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: zero=0._r_typ
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: half=.5_r_typ
      real(r_typ), parameter :: eps=1.e-5_r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i,j,nf,nr,ll,j0
      real(r_typ) :: alpha,dr,fac,d,dxi,xiv,cshft,xi_shift
      real(r_typ), dimension(:), allocatable :: dc,rdc
c
c-----------------------------------------------------------------------
c
c ****** Check that the number of mesh points is valid.
c
      if (nc.lt.2) then
        write (*,*)
        write (*,*) '### ERROR in GENMESH:'
        write (*,*) '### Invalid number of mesh points specified.'
        write (*,*) '### There must be at least two mesh points.'
        write (*,*) 'Mesh coordinate: ',label
        write (*,*) 'Number of mesh points specified = ',nc
        stop
      end if
c
c ****** Check that a positive mesh interval has been specified.
c
      if (c0.ge.c1) then
        write (*,*)
        write (*,*) '### ERROR in GENMESH:'
        write (*,*) '### Invalid mesh interval specified.'
        write (*,*) '### C1 must be greater than C0.'
        write (*,*) 'Mesh coordinate: ',label
        write (*,*) 'C0 = ',c0
        write (*,*) 'C1 = ',c1
        stop
      end if
c
c ****** Find the number of values of FRAC specified.
c
      do nf=1,nseg-1
        if (frac(nf+1).eq.zero) exit
      enddo
c
c ****** When no values have been specified (NF=1, the default),
c ****** a uniform mesh is produced.
c
      if (nf.eq.1.and.frac(1).eq.zero) then
        ns=1
        allocate (cs(ns+1))
        allocate (r(ns))
        cs(1)=c0
        cs(2)=c1
        r(1)=one
        go to 100
      end if
c
c ****** Check that the specified values of FRAC are monotonically
c ****** increasing.
c
      do i=2,nf
        if (frac(i).lt.frac(i-1)) then
          write (*,*)
          write (*,*) '### ERROR in GENMESH:'
          write (*,*) '### Invalid mesh specification.'
          write (*,*) 'Mesh coordinate: ',label
          write (*,*) 'The values in FRAC must increase monotonically.'
          write (*,*) 'FRAC = ',frac(1:nf)
          stop
        end if
      enddo
c
c ****** Check the specified values of FRAC.
c
      if (periodic) then
c
c ****** A periodic mesh requires the specified values of FRAC
c ****** to be in the range 0. to 1.
c
        if (frac(1).lt.zero.or.frac(nf).gt.one) then
          write (*,*)
          write (*,*) '### ERROR in GENMESH:'
          write (*,*) '### Invalid mesh specification.'
          write (*,*) 'Mesh coordinate: ',label
          write (*,*) 'For a periodic coordinate, the values in'//
     &                ' FRAC must be between 0. and 1.'
          write (*,*) 'FRAC = ',frac(1:nf)
          stop
        end if
c
c ****** A periodic mesh cannot contain both 0. and 1. in FRAC,
c ****** since these represent the same point.
c
        if (frac(1).eq.zero.and.frac(nf).eq.one) then
          write (*,*)
          write (*,*) '### ERROR in GENMESH:'
          write (*,*) '### Invalid mesh specification.'
          write (*,*) 'Mesh coordinate: ',label
          write (*,*) 'For a periodic coordinate, FRAC must not'//
     &                ' contain both 0. and 1.'
          write (*,*) 'FRAC = ',frac(1:nf)
          stop
        end if
c
      else
c
c ****** A non-periodic mesh requires the first specified value
c ****** of FRAC to be 0., and the last value to equal 1.
c
        if (frac(1).ne.zero) then
          write (*,*)
          write (*,*) '### ERROR in GENMESH:'
          write (*,*) '### Invalid mesh specification.'
          write (*,*) 'Mesh coordinate: ',label
          write (*,*) 'For a non-periodic coordinate, the first'//
     &                ' value of FRAC must equal 0.'
          write (*,*) 'FRAC = ',frac(1:nf)
          stop
        end if
c
        if (frac(nf).ne.one) then
          write (*,*)
          write (*,*) '### ERROR in GENMESH:'
          write (*,*) '### Invalid mesh specification.'
          write (*,*) 'Mesh coordinate: ',label
          write (*,*) 'For a non-periodic coordinate, the last'//
     &                ' value of FRAC must equal 1.'
          write (*,*) 'FRAC = ',frac(1:nf)
          stop
        end if
c
      end if
c
c ****** Check that the required values of DRATIO have been set,
c ****** and are positive.
c
      if (periodic) then
        nr=nf
      else
        nr=nf-1
      end if
c
      do i=1,nr
        if (dratio(i).le.zero) then
          write (*,*)
          write (*,*) '### ERROR in GENMESH:'
          write (*,*) '### Invalid mesh specification.'
          write (*,*) 'Mesh coordinate: ',label
          write (*,*) 'A required value in DRATIO has not been set'//
     &                ' or is not positive.'
          write (*,*) 'DRATIO = ',dratio(1:nr)
          stop
        end if
      enddo
c
c ****** Check that an inherently discontinuous mesh has not been
c ****** specified inadvertently.
c
      if (periodic.and.nr.eq.1.and.dratio(1).ne.one) then
        write (*,*)
        write (*,*) '### WARNING from GENMESH:'
        write (*,*) '### Discontinuous mesh specification.'
        write (*,*) 'Mesh coordinate: ',label
        write (*,*) 'An inherently discontinuous mesh has been'//
     &              ' specified.'
        write (*,*) 'FRAC = ',frac(1:nf)
        write (*,*) 'DRATIO = ',dratio(1:nr)
      end if
c
c ****** Set the number of segments.
c
      ns=nf-1
c
c ****** For a periodic coordinate, add points at XI=0. and XI=1.
c ****** if they are not already present.
c
      if (periodic) then
        if (frac(1).ne.zero) ns=ns+1
        if (frac(nf).ne.one) ns=ns+1
      end if
c
      allocate (cs(ns+1))
      allocate (r(ns))
c
c ****** Set up the coordinate limits of the segments.
c
      if (periodic) then
        if (frac(1).ne.zero) then
          cs(1)=c0
          cs(2:nf+1)=c0+(c1-c0)*frac(1:nf)
          if (frac(nf).ne.one) then
            alpha=(one-frac(nf))/(frac(1)+one-frac(nf))
            r(1)=dratio(nr)/(one+alpha*(dratio(nr)-one))
            r(2:nr+1)=dratio(1:nr)
            cs(ns+1)=c1
            r(ns)=one+alpha*(dratio(nr)-one)
          else
            r(1)=dratio(nr)
            r(2:nr)=dratio(1:nr-1)
          end if
        else
          cs(1:nf)=c0+(c1-c0)*frac(1:nf)
          r(1:nr)=dratio(1:nr)
          cs(ns+1)=c1
        end if
      else
        cs(1:nf)=c0+(c1-c0)*frac(1:nf)
        r(1:nr)=dratio(1:nr)
      end if
c
  100 continue
c
      allocate (xi(ns+1))
      allocate (a(ns))
      allocate (f(ns))
c
c ****** Compute the XI values at the segment limits.
c
      do i=1,ns
        dr=r(i)-one
        if (abs(dr).lt.eps) then
          f(i)=(cs(i+1)-cs(i))*(one+half*dr)
        else
          f(i)=(cs(i+1)-cs(i))*log(r(i))/dr
        end if
      enddo
c
      fac=zero
      do i=ns,1,-1
        fac=fac/r(i)+f(i)
      enddo
c
      d=f(1)/fac
      xi(1)=zero
      do i=2,ns
        xi(i)=xi(i-1)+d
        if (i.lt.ns) d=d*f(i)/(f(i-1)*r(i-1))
      enddo
      xi(ns+1)=one
c
c ****** Set the amplification factor for each segment.
c
      do i=1,ns
        a(i)=log(r(i))/(xi(i+1)-xi(i))
      enddo
c
c ****** For a periodic coordinate, find the XI shift corresponding
c ****** to a shift of C_SHIFT in the coordinate.
c ****** Note that a positive value of C_SHIFT moves the mesh
c ****** points to the right.
c
      if (periodic) then
        cshft=-c_shift
        call map_c_to_xi (periodic,ns,xi,cs,a,r,cshft,xi_shift)
      else
        xi_shift=0.
      end if
c
c ****** Compute the location of the mesh points in array C
c ****** by mapping from the XI values.
c
      dxi=one/(nc-one)
c
      c(1)=c0
      do j=2,nc-1
        xiv=(j-1)*dxi
        call map_xi_to_c (periodic,ns,xi,cs,a,r,
     &                    cshft,xi_shift,xiv,c(j))
      enddo
      c(nc)=c1
c
c ****** Filter the mesh if requested.
c
      if (nfilt.gt.0) then
        do i=1,nfilt
          if (periodic) then
            call filter_coord_periodic (c1-c0,nc,c)
          else
            call filter_coord (nc,c)
          end if
        enddo
      end if
c
c ****** Write out the mesh information.
c
      if (io.gt.0) then
c
        write (io,*)
        write (io,*) '### COMMENT from GENMESH:'
        write (io,*) '### Mesh information for coordinate ',label,':'
        write (io,*)
        write (io,*) 'Flag to indicate a periodic mesh: ',periodic
        write (io,*) 'Number of mesh points = ',nc
        write (io,*) 'Lower mesh limit = ',c0
        write (io,*) 'Upper mesh limit = ',c1
        write (io,*) 'Number of times to filter the mesh = ',nfilt
        if (periodic) then
          write (io,*) 'Amount to shift the mesh = ',c_shift
        end if
        write (io,*)
        write (io,*) 'Number of mesh segments = ',ns
c
        ll=len_trim(label)
c
        write (io,900) 'Segment      xi0       xi1'//
     &                 repeat (' ',16-ll)//label//'0'//
     &                 repeat (' ',16-ll)//label//'1'//
     &                 '            ratio'
        do i=1,ns
          write (io,910) i,xi(i),xi(i+1),cs(i),cs(i+1),r(i)
        enddo
c
        allocate (dc(nc))
        allocate (rdc(nc))
c
        dc=c-cshift(c,-1)
        if (periodic) dc(1)=dc(nc)
        rdc=dc/cshift(dc,-1)
        if (periodic) rdc(1)=rdc(nc)
c
        write (io,*)
        write (io,920) 'Mesh-point locations:'
        write (io,920) '     i'//
     &                 repeat (' ',18-ll)//label//
     &                 repeat (' ',17-ll)//'d'//label//
     &                 '             ratio'
c
        if (periodic) then
          j0=1
        else
          j0=3
          write (io,930) 1,c(1)
          write (io,930) 2,c(2),dc(2)
        end if
        do j=j0,nc
          write (io,930) j,c(j),dc(j),rdc(j)
        enddo
c
        deallocate (dc)
        deallocate (rdc)
c
      end if
c
  900 format (/,tr1,a)
  910 format (tr1,i4,2x,2f10.6,4f17.8)
  920 format (tr1,a)
  930 format (tr1,i6,3f18.8)
c
      deallocate (cs)
      deallocate (r)
      deallocate (xi)
      deallocate (a)
      deallocate (f)
c
      return
      end
c#######################################################################
      subroutine map_xi_to_c (periodic,ns,xi,cs,a,r,
     &                        cshft,xi_shift,xiv,cv)
c
c-----------------------------------------------------------------------
c
c ****** Get the mesh coordinate value CV for the specified
c ****** xi value XIV.
c
c ****** Set PERIODIC=.true. for a periodic coordinate.
c ****** NS is the number of segments in the mesh definition.
c ****** The arrays XI, CS, A, and R define the mesh mapping.
c
c ****** CSHFT represents the amount to shift a periodic coordinate.
c ****** XI_SHIFT represents the corresponding amount to shift xi.
c
c ****** This is a utility routine for GENMESH.
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      logical, intent(in) :: periodic
      integer, intent(in) :: ns
      real(r_typ), dimension(ns+1), intent(in) :: xi,cs
      real(r_typ), dimension(ns), intent(in) :: a,r
      real(r_typ), intent(in) :: cshft,xi_shift
      real(r_typ), intent(in) :: xiv
      real(r_typ), intent(out) :: cv
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: zero=0._r_typ
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: half=.5_r_typ
      real(r_typ), parameter :: eps=1.e-5
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: xiv_p,d,d1,da,da1,fac
c
c-----------------------------------------------------------------------
c
      real(r_typ), external :: fold
c
c-----------------------------------------------------------------------
c
c ****** Find the index of the segment to which XIV belongs.
c
      if (periodic) then
c
c ****** Shift XIV by XI_SHIFT.
c
        xiv_p=xiv+xi_shift
c
c ****** Fold XIV_P into the main interval.
c
        xiv_p=fold(zero,one,xiv_p)
c
      else
c
        xiv_p=xiv
c
      end if
c
      do i=1,ns
        if (xiv_p.ge.xi(i).and.xiv_p.le.xi(i+1)) exit
      enddo
c
      if (i.gt.ns) then
        write (*,*)
        write (*,*) '### ERROR in MAP_XI_TO_C:'
        write (*,*) '### Error in finding the XI segment.'
        write (*,*) '### Could not find XIV in the XI table.'
        write (*,*) '[Utility routine for GENMESH.]'
        write (*,*) '[This is an internal error.]'
        write (*,*) 'XI = ',xi
        write (*,*) 'XIV = ',xiv
        write (*,*) 'XIV_P = ',xiv_p
        stop
      end if
c
      d =xiv_p  -xi(i)
      d1=xi(i+1)-xi(i)
c
      da =a(i)*d
      da1=a(i)*d1
c
c ****** Interpolate the mapping function at XIV_P.
c
      if (abs(da1).lt.eps) then
        fac=(d*(one+half*da))/(d1*(one+half*da1))
      else
        fac=(exp(da)-one)/(r(i)-one)
      end if
c
      cv=cs(i)+(cs(i+1)-cs(i))*fac
c
      if (periodic) then
c
c ****** Shift CV by the amount CSHFT.
c
        cv=cv-cshft
c
c ****** Fold CV into the main interval.
c
        cv=fold(cs(1),cs(ns+1),cv)
c
      end if
c
      return
      end
c#######################################################################
      subroutine map_c_to_xi (periodic,ns,xi,cs,a,r,cv,xiv)
c
c-----------------------------------------------------------------------
c
c ****** Get the xi value XIV for the specified coordinate value CV.
c
c ****** Set PERIODIC=.true. for a periodic coordinate.
c ****** NS is the number of segments in the mesh definition.
c ****** The arrays XI, CS, A, and R define the mesh mapping.
c
c ****** This is a utility routine for GENMESH.
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      logical, intent(in) :: periodic
      integer, intent(in) :: ns
      real(r_typ), dimension(ns+1), intent(in) :: xi,cs
      real(r_typ), dimension(ns), intent(in) :: a,r
      real(r_typ), intent(in) :: cv
      real(r_typ), intent(out) :: xiv
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: zero=0._r_typ
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: eps=1.e-5
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: cv_p,d,da,fac
c
c-----------------------------------------------------------------------
c
      real(r_typ), external :: fold
c
c-----------------------------------------------------------------------
c
c ****** Find the index of the segment to which CV belongs.
c
      if (periodic) then
c
c ****** Fold CV_P into the main interval.
c
        cv_p=fold(cs(1),cs(ns+1),cv)
c
      else
c
        cv_p=cv
c
      end if
c
      do i=1,ns
        if (cv_p.ge.cs(i).and.cv_p.le.cs(i+1)) exit
      enddo
c
      if (i.gt.ns) then
        write (*,*)
        write (*,*) '### ERROR in MAP_C_TO_XI:'
        write (*,*) '### Error in finding the CS segment.'
        write (*,*) '### Could not find CV in the CS table.'
        write (*,*) '[Utility routine for GENMESH.]'
        write (*,*) '[This is an internal error.]'
        write (*,*) 'CS = ',cs
        write (*,*) 'CV = ',cv
        write (*,*) 'CV_P = ',cv_p
        stop
      end if
c
      d=(cv_p-cs(i))/(cs(i+1)-cs(i))
      da=(r(i)-one)*d
c
c ****** Interpolate the mapping function at XIV_P.
c
      if (abs(da).lt.eps) then
        fac=d*(xi(i+1)-xi(i))
      else
        fac=log(da+one)/a(i)
      end if
c
      xiv=xi(i)+fac
c
      return
      end
c#######################################################################
      subroutine filter_coord (n,f)
c
c-----------------------------------------------------------------------
c
c ****** Apply a "(1,2,1)/4" low-pass digital filter to a
c ****** 1D coordinate.
c
c ****** The end-points F(1) and F(N) are not changed.
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      integer :: n
      real(r_typ), dimension(n) :: f
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: two=2._r_typ
      real(r_typ), parameter :: quarter=.25_r_typ
c
c-----------------------------------------------------------------------
c
      real(r_typ), dimension(n) :: ff
c
c-----------------------------------------------------------------------
c
      integer :: i
c
c-----------------------------------------------------------------------
c
c ****** Make a copy of the function.
c
      ff=f
c
c ****** Apply the filter.
c
      do i=2,n-1
        f(i)=quarter*(ff(i-1)+two*ff(i)+ff(i+1))
      enddo
c
      return
      end
c#######################################################################
      subroutine filter_coord_periodic (xl,n,f)
c
c-----------------------------------------------------------------------
c
c ****** Apply a "(1,2,1)/4" low-pass digital filter to a
c ****** periodic 1D coordinate.
c
c-----------------------------------------------------------------------
c
c ****** XL is the periodic interval for the coordinate.
c
c ****** The filtered coordinate is translated so that F(1)
c ****** is preserved.
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: xl
      integer :: n
      real(r_typ), dimension(n) :: f
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: two=2._r_typ
      real(r_typ), parameter :: quarter=.25_r_typ
c
c-----------------------------------------------------------------------
c
      real(r_typ), dimension(0:n+1) :: ff
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: f1old,f1new
c
c-----------------------------------------------------------------------
c
c ****** Save the value of F(1).
c
      f1old=f(1)
c
c ****** Make a periodic copy of the function.
c
      ff(1:n)=f(:)
c
      ff(0)=f(n-1)-xl
      ff(n+1)=f(2)+xl
c
c ****** Apply the filter.
c
      do i=1,n
        f(i)=quarter*(ff(i-1)+two*ff(i)+ff(i+1))
      enddo
c
c ****** Translate F so that F(1) is preserved.
c
      f1new=f(1)
      do i=1,n
        f(i)=f(i)-f1new+f1old
      enddo
c
      return
      end
c#######################################################################
      function fold (x0,x1,x)
c
c-----------------------------------------------------------------------
c
c ****** "Fold" X into the periodic interval [X0,X1].
c
c ****** On return, X is such that X0.le.X.lt.X1.
c
c-----------------------------------------------------------------------
c
c ****** It is assumed that X0 does not equal X1, as is physically
c ****** necessary.  If X0 and X1 are equal, the routine just
c ****** returns with FOLD=X.
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: fold
      real(r_typ) :: x0,x1,x
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: xl
c
c-----------------------------------------------------------------------
c
      fold=x
c
      if (x0.eq.x1) return
c
      xl=x1-x0
c
      fold=mod(x-x0,xl)+x0
c
      if (fold.lt.x0) fold=fold+xl
      if (fold.ge.x1) fold=fold-xl
c
      return
      end
c#######################################################################
      subroutine write_mesh (fname,label,nc,periodic,c)
c
c-----------------------------------------------------------------------
c
c ****** Write a one-dimensional mesh to file FNAME.
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      character(*), intent(in) :: fname
      character(*), intent(in) :: label
      integer, intent(in) :: nc
      logical, intent(in) :: periodic
      real(r_typ), dimension(nc), intent(in) :: c
c
c-----------------------------------------------------------------------
c
      character, parameter :: TAB=achar(9)
c
c-----------------------------------------------------------------------
c
      integer :: ierr,j,j0
      real(r_typ), dimension(:), allocatable :: dc,rdc
c
c-----------------------------------------------------------------------
c
      call ffopen (1,fname,'rw',ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in WRITE_MESH:'
        write (*,*) '### Could not open the output mesh file:'
        write (*,*) 'File name: ',trim(fname)
        call exit (1)
      end if
c
      allocate (dc(nc))
      allocate (rdc(nc))
c
      dc=c-cshift(c,-1)
      if (periodic) dc(1)=dc(nc)
      rdc=dc/cshift(dc,-1)
      if (periodic) rdc(1)=rdc(nc)
c
      write (1,910) 'i',TAB,trim(label),TAB,
     &              'd'//trim(label),TAB,'ratio'
c
      if (periodic) then
        j0=1
      else
        j0=3
        write (1,920) 1,TAB,c(1),TAB,dc(2),TAB,rdc(3)
        write (1,920) 2,TAB,c(2),TAB,dc(2),TAB,rdc(3)
      end if
      do j=j0,nc
        write (1,920) j,TAB,c(j),TAB,dc(j),TAB,rdc(j)
      enddo
c
  910 format (a,3(a,a))
  920 format (i8,3(a,1pe21.14))
c
      close (1)
c
      deallocate (dc)
      deallocate (rdc)
c
      return
      end
c#######################################################################
      subroutine read_flux_tube_geometry (fname)
c
c-----------------------------------------------------------------------
c
c ****** Read the flux tube geometry from file FNAME.
c ****** The file should have 5 columns with the quantities
c ****** (s,z,A,B,gnorm).
c
c ****** The normalized gravity should be positive at the left
c ****** footpoint of the loop, gnorm = sign(Bz0)*Bz/B, where
c ****** Bz0 is the value of Bz at the left footpoint.
c
c-----------------------------------------------------------------------
c
      use flux_tube_geometry
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      character(*) :: fname
c
c-----------------------------------------------------------------------
c
      integer :: i,ierr
c
c-----------------------------------------------------------------------
c
c ****** Read the file containing the flux tube geometry.
c
      write (*,*)
      write (*,*) '### Reading flux tube geometry file: ',
     &            trim(fname)
c
c ****** Scan the file to determine the number of points in the table.
c
      call scan (fname,5,nft,ierr)
c
      if (ierr.ne.0) go to 900
c
c ****** Allocate memory.
c
      allocate (s_ft(nft))
      allocate (z_ft(nft))
      allocate (a_ft(nft))
      allocate (b_ft(nft))
      allocate (g_ft(nft))
c
c ****** Read the flux tube geometry.
c
      call ffopen (1,fname,'r',ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in READ_FLUX_TUBE_GEOMETRY:'
        write (*,*) '### Could not open the flux tube geometry file:'
        write (*,*) 'File name: ',trim(fname)
        call exit (1)
      end if
c
c ****** Skip the header.
c
      read (1,*)
c
      do i=1,nft
        read (1,*,err=900,end=900) s_ft(i),
     &                             z_ft(i),
     &                             a_ft(i),
     &                             b_ft(i),
     &                             g_ft(i)
      enddo
c
      close (1)
c
      return
c
  900 continue
c
      write (*,*)
      write (*,*) '### ERROR in READ_FLUX_TUBE_GEOMETRY:'
      write (*,*) '### Error while reading the geometry.'
      write (*,*) 'File name: ',trim(fname)
      call exit (1)
c
      end
c#######################################################################
      subroutine set_circular_flux_tube
c
c-----------------------------------------------------------------------
c
c ****** Initialize a flux tube that is a semi-circle, with a
c ****** specified B profile.
c
c-----------------------------------------------------------------------
c
      use number_types
      use mesh
      use flux_tube_geometry
      use normalization_parameters
      use initial_state
      use params
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: ds_unif,radius,theta,s_l,s_r
c
c-----------------------------------------------------------------------
c
c ****** Check the validity of the mesh.
c
      call check_mesh_validity
c
c ****** Set the number of points to define the semi-circle.
c
      nft=1001
c
c ****** Allocate memory.
c
      allocate (s_ft(nft))
      allocate (z_ft(nft))
      allocate (a_ft(nft))
      allocate (b_ft(nft))
      allocate (g_ft(nft))
c
c ****** Define the flux tube shape to be a semi-circle.
c
      ds_unif=sl/(nft-1)
      radius=sl/pi
c
      do i=1,nft
        s_ft(i)=s0+(i-1)*ds_unif
        theta=s_ft(i)*pi/sl
        z_ft(i)=radius*sin(theta)
        g_ft(i)=cos(theta)
      enddo
c
c ****** Set the magnetic field profile and the flux tube area.
c
      b_ft=b_uniform
c
      do i=1,nft
        s_l=s_ft(i)-s0
        s_r=s1-s_ft(i)
        b_ft(i)=b_ft(i)+b0_l*exp(-s_l/blen_l)
     &                 +b0_r*exp(-s_r/blen_r)
      enddo
c
      a_ft=b_ft(1)/b_ft
c
      return
      end
c#######################################################################
      subroutine set_flux_tube_length
c
c-----------------------------------------------------------------------
c
c ****** Set the flux tube length for the case when the flux tube
c ****** geometry was read in, and the user requests automatic
c ****** setting of the length.
c
c-----------------------------------------------------------------------
c
      use number_types
      use mesh
      use flux_tube_geometry
      use params
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: s0_read_in,s1_read_in
c
c-----------------------------------------------------------------------
c
      s0_read_in=s_ft(1)
      s1_read_in=s_ft(nft)
c
c ****** Set the location of the left leg of the loop.
c
c ****** If the user set S0 to a negative value, set the starting
c ****** location to equal that of the flux tube read in.  Otherwise,
c ****** use the value of S0 specified, checking that it is valid.
c
      if (s0.lt.0.) then
        s0=s0_read_in
      else if (s0.lt.s0_read_in) then
        write (*,*)
        write (*,*) '### ERROR in SET_FLUX_TUBE_LENGTH:'
        write (*,*) '### The requested S0 is invalid:'
        write (*,*)
        write (*,*) 'Requested S0 = ',s0
        write (*,*) 'Flux tube S0 = ',s0_read_in
        write (*,*) 'Flux tube S1 = ',s1_read_in
        write (*,*)
        write (*,*) '[To use S0 of the flux tube read'//
     &              ' in, set S0=-1.]'
        call exit (1)
      end if
c
c ****** Set the location of the right leg of the loop.
c
c ****** If the user set S1 to a non-positive value, then set S1
c ****** so that the flux tube length equals that read in.
c ****** Otherwise, use the value of S1 requested, checking that
c ****** it is valid.
c
      if (s1.le.0.) then
        s1=s1_read_in
      else if (s1.lt.s0.or.s1.gt.s1_read_in) then
        write (*,*)
        write (*,*) '### ERROR in SET_FLUX_TUBE_LENGTH:'
        write (*,*) '### The requested S1 is invalid:'
        write (*,*)
        write (*,*) 'Requested S0 = ',s0
        write (*,*) 'Requested S1 = ',s1
        write (*,*) 'Flux tube S0 = ',s0_read_in
        write (*,*) 'Flux tube S1 = ',s1_read_in
        write (*,*)
        write (*,*) '[To use S1 of the flux tube read'//
     &              ' in, set S1=-1.]'
        call exit (1)
      end if
c
c ****** Check the validity of the mesh.
c
      call check_mesh_validity
c
      return
      end
c#######################################################################
      subroutine set_flux_tube_geometry_params
c
c-----------------------------------------------------------------------
c
c ****** Set parameters based on the flux tube geometry.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use flux_tube_geometry
      use heating_parameters
      use params
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: a0
c
c-----------------------------------------------------------------------
c
c ****** Interpolate the flux tube height, area, magnetic field,
c ****** and normalized gravity onto the code mesh.
c
      call interp_array (nft,s_ft,z_ft,nsm1,s,z)
      call interp_array (nft,s_ft,a_ft,nsm1,s,area)
      call interp_array (nft,s_ft,g_ft,nsm1,s,g)
c
      call interp_array (nft,s_ft,a_ft,ns,sh,areah)
      call interp_array (nft,s_ft,z_ft,ns,sh,zh)
      call interp_array (nft,s_ft,b_ft,ns,sh,b)
c
      call extrapolate_boundary_values (nft,s_ft,z_ft,zh)
c
c ****** Normalize the area to equal 1 at the left end of the loop.
c
      a0=area(1)
      area=area/a0
      areah=areah/a0
c
c ****** Multiply the normalized gravity by G0 to get gravity.
c
      g=g0*g
c
c ****** Set gravity to zero if requested.
c
      if (.not.use_gravity) g=0.
c
c ****** Deallocate the memory that is being used to hold the
c ****** flux tube geometry that was read in, since it is no
c ****** longer needed.
c
      deallocate (s_ft)
      deallocate (z_ft)
      deallocate (a_ft)
      deallocate (b_ft)
      deallocate (g_ft)
c
c ****** Integrate the gravity to get the gravitational potential
c ****** at the flux tube ends.  This is used in the energy
c ****** diagnostic.
c
c ****** Set the gravitational potential at the left leg to
c ****** be zero (arbitrary).
c
      phi0=0.
c
      phi1=phi0
      do i=2,nsm1
        phi1=phi1+half*(g(i-1)+g(i))*dsh(i)
      enddo
c
      return
      end
c#######################################################################
      subroutine interp_array (n1,x1,f1,n2,x2,f2)
c
c-----------------------------------------------------------------------
c
c ****** Interpolate the array F1(N1), defined on mesh X1(N1),
c ****** to the new array F2(N2), defined on mesh X2(N2),
c ****** using linear interpolation.
c
c-----------------------------------------------------------------------
c
c ****** X1 is assumed to be monotonically increasing.
c
c ****** Values outside the range of the table X1 are clamped to
c ****** the boundary values.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      integer :: n1
      real(r_typ), dimension(n1) :: x1,f1
      integer :: n2
      real(r_typ), dimension(n2) :: x2,f2
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i,ix,ixp1
      real(r_typ) :: alpha
c
c-----------------------------------------------------------------------
c
      do i=1,n2
        if (x2(i).le.x1(1)) then
          f2(i)=f1(1)
        else if (x2(i).ge.x1(n1)) then
          f2(i)=f1(n1)
        else
          call get_interp_factors (n1,x1,x2(i),ix,alpha)
          ixp1=min0(ix+1,n1)
          f2(i)=(one-alpha)*f1(ix)+alpha*f1(ixp1)
        end if
      enddo
c
      return
      end
c#######################################################################
      subroutine extrapolate_boundary_values (ni,si,fi,f)
c
c-----------------------------------------------------------------------
c
c ****** Set the boundary values of an array on the SH mesh,
c ****** F(1) and F(NS), using extrapolation from the data in
c ****** table FI(NI) and SI(NI).
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      integer :: ni
      real(r_typ), dimension(ni) :: si,fi
      real(r_typ), dimension(ns) :: f
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: two=2._r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i,ip1
      real(r_typ) :: alpha,f0,f1
c
c-----------------------------------------------------------------------
c
c ****** Boundary at s=S0.
c
      call get_interp_factors (ni,si,s(1),i,alpha)
      ip1=min(i+1,ni)
      f0=(one-alpha)*fi(i)+alpha*fi(ip1)
      f(1)=two*f0-f(2)
c
c ****** Boundary at s=S1.
c
      call get_interp_factors (ni,si,s(nsm1),i,alpha)
      ip1=min(i+1,ni)
      f1=(one-alpha)*fi(i)+alpha*fi(ip1)
      f(ns)=two*f1-f(nsm1)
c
      return
      end
c#######################################################################
      subroutine get_interp_factors (n,x,xv,i,alpha)
c
c-----------------------------------------------------------------------
c
c ****** Get interpolation factor ALPHA and table index I.
c
c ****** This routine does not do the actual interpolation.  Use the
c ****** returned values of I and ALPHA to get the interpolated value.
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      integer :: n
      real(r_typ), dimension(n) :: x
      real(r_typ) :: xv
      integer :: i
      real(r_typ) :: alpha
c
      intent(in) :: n,x,xv
      intent(out) :: i,alpha
c
c-----------------------------------------------------------------------
c
      do i=1,n-1
        if (xv.ge.x(i).and.xv.le.x(i+1)) then
          alpha=(xv-x(i))/(x(i+1)-x(i))
          return
        end if
      enddo
c
c ****** Value not found --- signal error and stop.
c
      write (*,*)
      write (*,*) '### ERROR in GET_INTERP_FACTORS:'
      write (*,*) '### Value not found in table.'
      write (*,*) 'Value requested = ',xv
      write (*,*) 'Minimum table value = ',x(1)
      write (*,*) 'Maximum table value = ',x(n)
      call exit (1)
c
      end
c#######################################################################
      subroutine initialize_fields (fname)
c
c-----------------------------------------------------------------------
c
c ****** Set the initial state.
c
c ****** If FNAME='RESTART', read n_e, T, and v from the end of
c ****** a previous run (i.e., do a "restart").
c
c ****** If FNAME='HSEQUIL', initialize an analytic hydrostatic
c ****** equilibrium.
c
c ****** Otherwise, read the initial state from file FNAME.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use normalization_parameters
      use params
      use si_term_velocities
      use boundary_conditions
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      character(*) :: fname
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
      if (fname.eq.' ') then
        write (*,*)
        write (*,*) '### ERROR in INITIALIZE_FIELDS:'
        write (*,*) '### The initial state file name cannot be blank.'
        write (*,*)
        write (*,*) 'Specify a valid file name or use one of the'//
     &              ' following options:'
        write (*,*) '''HSEQUIL'''
        write (*,*) '''RESTART'''
        call exit (1)
      else if (fname.eq.'RESTART') then
        call read_restart
      else if (fname.eq.'HSEQUIL') then
        call initialize_hs_equil
      else
        write (*,*)
        write (*,*) '### COMMENT from INITIALIZE_FIELDS:'
        write (*,*) '### Reading the initial state from file: ',
     &              trim(fname)
        call read_initial_state (fname)
      end if
c
c ****** Set the mass density from NE.
c
      call ne2rho
c
c ****** Set the pressure.
c
      call set_p
c
c ****** Set boundary values of NE.
c
      ne0=half*(ne( 1)+ne(   2))
      ne1=half*(ne(ns)+ne(nsm1))
c
c ****** Initialize the semi-implicit term intermediate velocities.
c
      v_si_old1=v
      v_si_old2=v
c
c ****** Set the viscosity.
c
      call load_viscosity
c
      return
      end
c#######################################################################
      subroutine load_viscosity
c
c-----------------------------------------------------------------------
c
c ****** Set the viscosity profile.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use viscosity_profile
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i,ierr
      real(r_typ) :: dummy
      real(r_typ), dimension(:), allocatable :: visc_from_file
c
c-----------------------------------------------------------------------
c
      real(r_typ), external :: node_profile
c
c-----------------------------------------------------------------------
c
c ****** Initialize the viscosity to zero.
c
      visc=0.
c
c ****** If USE_VISCOSITY=.false., the viscosity remains zero.
c
c ****** Write a warning if a viscosity file was specified, but
c ****** USE_VISCOSITY=.false. was also set.  This may be a user
c ****** error.
c
      if (.not.use_viscosity.and.viscosity_input_file.ne.' ') then
        write (*,*)
        write (*,*) '### WARNING from INITIALIZE_VISCOSITY:'
        write (*,*) '### You set USE_VISCOSITY=.false., but you'//
     &              ' also specified a viscosity'
        write (*,*) '### table input file.'
        write (*,*) 'Please check that this was your intention.'
        write (*,*) 'Viscosity file name: ',trim(viscosity_input_file)
      end if
c
      if (.not.use_viscosity) then
        write (*,*)
        write (*,*) '### COMMENT from INITIALIZE_VISCOSITY:'
        write (*,*) 'The viscosity has been set to zero.'
        return
      end if
c
c ****** Add the viscosity defined by the profile VISC_PROF.
c
c ****** Check that the specified profile is valid.
c
      if (visc_prof%n.gt.nodes_max) then
        write (*,*)
        write (*,*) '### ERROR in INITIALIZE_VISCOSITY:'
        write (*,*) '### The number of nodes in the viscosity'//
     &              ' profile exceeds the maximum allowed.'
        write (*,*) 'Number of nodes specified = ',visc_prof%n
        write (*,*) 'Maximum number allowed    = ',nodes_max
        call exit(1)
      end if
c
      if (visc_prof%n.gt.0) then
c
        dummy=node_profile(.true.,visc_prof,s(1))
        do i=1,nsm1
          visc(i)=visc(i)+node_profile(.false.,visc_prof,s(i))
        enddo
c
        write (*,*)
        write (*,*) '### COMMENT from INITIALIZE_VISCOSITY:'
        write (*,*) '### Added the viscosity defined by the'//
     &              ' specified profile.'
        write (*,*) 'Number of nodes = ',visc_prof%n
        write (*,*) 'Node locations:'
        write (*,*) visc_prof%x(1:visc_prof%n)
        write (*,*) 'Viscosity at nodes:'
        write (*,*) visc_prof%f(1:visc_prof%n)
c
      end if
c
c ****** If VISCOSITY_INPUT_FILE is not blank, add the viscosity
c ****** read in from that file.
c
      if (viscosity_input_file.ne.' ') then
c
        write (*,*)
        write (*,*) '### Reading viscosity file: ',
     &              trim(viscosity_input_file)
c
        allocate (visc_from_file(nsm1))
        call read_and_interp_field_m (viscosity_input_file,
     &                                visc_from_file)
        visc=visc+visc_from_file
        deallocate (visc_from_file)
c
        write (*,*)
        write (*,*) '### COMMENT from INITIALIZE_VISCOSITY:'
        write (*,*) '### Added the viscosity from file: ',
     &              trim(viscosity_input_file)
c
      end if
c
c ****** Write the viscosity to an output file if requested.
c
      if (viscosity_output_file.ne.' ') then
 
        write (*,*)
        write (*,*) '### COMMENT from INITIALIZE_VISCOSITY:'
        write (*,*) '### Writing the viscosity to file: ',
     &              trim(viscosity_output_file)
c
        call ffopen (1,viscosity_output_file,'rw',ierr)
c
        if (ierr.ne.0) then
          write (*,*)
          write (*,*) '### ERROR in INITIALIZE_VISCOSITY:'
          write (*,*) '### Could not open the viscosity output file:'
          write (*,*) 'File name: ',trim(viscosity_output_file)
          call exit (1)
        end if
c
        write (1,900) 's'   ,char(9),
     &                'visc'
        do i=1,nsm1
          write (1,910) s(i)   ,char(9),
     &                  visc(i)
        enddo
c
        close (1)
c
      end if
c
  900 format (10(a,a))
  910 format (10(1pe21.14,a))
c
      return
      end
c#######################################################################
      subroutine read_and_interp_field_m (fname,f)
c
c-----------------------------------------------------------------------
c
c ****** Read a field from file FNAME and interpolate it to the
c ****** code mesh.
c
c ****** This version is for a field defined on the main mesh.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      character(*) :: fname
      real(r_typ), dimension(nsm1) :: f
c
c-----------------------------------------------------------------------
c
      integer :: i,npts,ierr
      real(r_typ), dimension(:), allocatable :: s_t,f_t
c
c-----------------------------------------------------------------------
c
c ****** Scan the file to determine the number of points in the table.
c
      call scan (fname,2,npts,ierr)
c
      if (ierr.ne.0) then
        i=0
        go to 900
      end if
c
c ****** Allocate memory.
c
      allocate (s_t(npts))
      allocate (f_t(npts))
c
c ****** Read the table.
c
      call ffopen (1,fname,'r',ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in READ_AND_INTERP_FIELD_M:'
        write (*,*) '### Could not open the input file:'
        write (*,*) 'File name: ',trim(fname)
        call exit (1)
      end if
c
c ****** Skip the header.
c
      read (1,*)
c
      do i=1,npts
        read (1,*,err=900,end=900) s_t(i),
     &                             f_t(i)
      enddo
c
      close (1)
c
c ****** Interpolate the field to the main code mesh.
c
      call interp_array (npts,s_t,f_t,nsm1,s,f)
c
c ****** Deallocate memory.
c
      deallocate (s_t)
      deallocate (f_t)
c
      return
c
  900 continue
c
      write (*,*)
      write (*,*) '### ERROR in READ_AND_INTERP_FIELD_M:'
      write (*,*) '### Error while reading the table from the file.'
      write (*,*) 'File name: ',trim(fname)
      write (*,*) 'Line number: ',i
      call exit (1)
c
      end
c#######################################################################
      subroutine read_and_interp_field_h (fname,f)
c
c-----------------------------------------------------------------------
c
c ****** Read a field from file FNAME and interpolate it to the
c ****** code mesh.
c
c ****** This version is for a field defined on the half mesh.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      character(*) :: fname
      real(r_typ), dimension(ns) :: f
c
c-----------------------------------------------------------------------
c
      integer :: i,npts,ierr
      real(r_typ), dimension(:), allocatable :: s_t,f_t
c
c-----------------------------------------------------------------------
c
c ****** Scan the file to determine the number of points in the table.
c
      call scan (fname,2,npts,ierr)
c
      if (ierr.ne.0) then
        i=0
        go to 900
      end if
c
c ****** Allocate memory.
c
      allocate (s_t(npts))
      allocate (f_t(npts))
c
c ****** Read the table.
c
      call ffopen (1,fname,'r',ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in READ_AND_INTERP_FIELD_H:'
        write (*,*) '### Could not open the input file:'
        write (*,*) 'File name: ',trim(fname)
        call exit (1)
      end if
c
c ****** Skip the header.
c
      read (1,*)
c
      do i=1,npts
        read (1,*,err=900,end=900) s_t(i),
     &                             f_t(i)
      enddo
c
      close (1)
c
c ****** Interpolate the field to the main code mesh.
c
      call interp_array (npts,s_t,f_t,ns,sh,f)
c
c ****** Extrapolate the boundary points.
c
      call extrapolate_boundary_values (npts,s_t,f_t,f)
c
c ****** Deallocate memory.
c
      deallocate (s_t)
      deallocate (f_t)
c
      return
c
  900 continue
c
      write (*,*)
      write (*,*) '### ERROR in READ_AND_INTERP_FIELD_H:'
      write (*,*) '### Error while reading the table from the file.'
      write (*,*) 'File name: ',trim(fname)
      write (*,*) 'Line number: ',i
      call exit (1)
c
      end
c#######################################################################
      subroutine initialize_hs_equil
c
c-----------------------------------------------------------------------
c
c ****** Set up a hydrostatic equilibrium with a parabolic loop
c ****** temperature, with a cool chromospheric region at the two
c ****** ends of the loop.
c
c ****** The density is set from hydrostatic equilibrium.
c ****** The velocity is set to zero.
c
c ****** This initial state will generally not be in energy
c ****** balance, so it will evolve.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use initial_state
      use normalization_parameters
      use params
      use boundary_conditions
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: two=2._r_typ
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: s_apex,coronal_half_length,
     &               fac0,fac1,sv,svp,tv,dtds,v1
c
c-----------------------------------------------------------------------
c
c ****** Set the temperature to go smoothly from TBC0 at the left
c ****** leg of the loop to HS_T_APEX at the midpoint of the loop,
c ****** and back to TBC1 at the right leg of the loop, with a
c ****** chromosphere region of thickness HS_CHROMO_THICKNESS
c ****** at each end of the loop.
c
      s_apex=half*sl
c
      coronal_half_length=s_apex-hs_chromo_thickness
      fac0=(hs_t_apex-tbc0)/coronal_half_length**2
      fac1=(hs_t_apex-tbc1)/coronal_half_length**2
c
      do i=2,nsm1
        sv=sh(i)-s0
        if (sv.le.hs_chromo_thickness) then
          temp(i)=tbc0
        else if (sv.ge.(sl-hs_chromo_thickness)) then
          temp(i)=tbc1
        else if (sv.lt.s_apex) then
          svp=sv-hs_chromo_thickness
          temp(i)=tbc0-fac0*svp*(svp-two*coronal_half_length)
        else
          svp=(sl-sv)-hs_chromo_thickness
          temp(i)=tbc1-fac1*svp*(svp-two*coronal_half_length)
        end if
      enddo
      temp(1)=two*tbc0-temp(2)
      temp(ns)=two*tbc1-temp(nsm1)
c
c ****** Set the electron density from hydrostatic equilibrium.
c
      tv=half*(temp(1)+temp(2))
      dtds=(temp(2)-temp(1))/ds(1)
      v1=ds(1)*(g(1)*he_rho/he_p+dtds)/tv
      rho(1)=log(two*he_rho*hs_n_e_base/(one+exp(-v1)))
c
      do i=1,nsm1
        tv=half*(temp(i)+temp(i+1))
        dtds=(temp(i+1)-temp(i))/ds(i)
        rho(i+1)=rho(i)-ds(i)*(g(i)*he_rho/he_p+dtds)/tv
      enddo
c
      rho=exp(rho)
c
c ****** Set NE from the mass density.
c
      call rho2ne
c
c ****** Initialize the base densities NE0 and NE1.
c
      ne0=half*(ne( 1)+ne(   2))
      ne1=half*(ne(ns)+ne(nsm1))
c
c ****** Set the velocity to zero.
c
      v=0.
c
      write (*,*)
      write (*,*) '### COMMENT from INITIALIZE_HS_EQUIL:'
      write (*,*) '### Initializing using a hydrostatic'//
     &              ' equilibrium:'
      write (*,*)
      write (*,900) 'Loop apex temperature [MK]         = ',
     &              hs_t_apex*fn_t*1.e-6_r_typ
      write (*,901) 'Loop base electron density [/cm^3] = ',
     &              hs_n_e_base*fn0phys
      write (*,900) 'Chromosphere thickness [Rs]        = ',
     &              hs_chromo_thickness
c
  900 format (1x,a,f12.6)
  901 format (1x,a,1pe13.6)
c
      return
      end
c#######################################################################
      subroutine read_restart
c
c-----------------------------------------------------------------------
c
c ****** Restart the code from the end state of a previous run.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use params
      use boundary_conditions
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
      character(16) :: fname
      integer :: i,ierr,npts
c
c-----------------------------------------------------------------------
c
c ****** Read the initial state from a previous run.
c
      write (*,*)
      write (*,*) '### COMMENT from READ_RESTART:'
      write (*,*) '### Restarting from a previous run.'
c
c ****** Check that the number of points in the file equals
c ****** that expected.
c
      fname='restart1.dat'
      call scan (fname,3,npts,ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in READ_RESTART:'
        write (*,*) '### Error while reading the restart'//
     &              ' file for n_e and T.'
        write (*,*) 'File name: ',trim(fname)
        call exit (1)
      end if
c
      if (npts.ne.ns) then
        write (*,*)
        write (*,*) '### ERROR in READ_RESTART:'
        write (*,*) '### The restart file for n_e and T'//
     &              ' has the wrong length:'
        write (*,*) 'Number of lines expected = ',ns+1
        write (*,*) 'Number of lines found    = ',npts+1
        write (*,*) 'File name: ',trim(fname)
        call exit (1)
      end if
c
      call ffopen (1,fname,'r',ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in READ_RESTART:'
        write (*,*) '### Could not open the requested file:'
        write (*,*) 'File name: ',trim(fname)
        call exit (1)
      end if
c
      read (1,*)
      do i=1,ns
        read (1,*) sh(i),ne(i),temp(i)
      enddo
c
      close (1)
c
      fname='restart2.dat'
      call scan (fname,2,npts,ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in READ_RESTART:'
        write (*,*) '### Error while reading the restart'//
     &              ' file for v.'
        write (*,*) 'File name: ',trim(fname)
        call exit (1)
      end if
c
      if (npts.ne.nsm1) then
        write (*,*)
        write (*,*) '### ERROR in READ_RESTART:'
        write (*,*) '### The restart file for n_e and T'//
     &              ' has the wrong length:'
        write (*,*) 'Number of lines expected = ',nsm1+1
        write (*,*) 'Number of lines found    = ',npts+1
        write (*,*) 'File name: ',trim(fname)
        call exit (1)
      end if
c
      call ffopen (1,fname,'r',ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in READ_RESTART:'
        write (*,*) '### Could not open the requested file:'
        write (*,*) 'File name: ',trim(fname)
        call exit (1)
      end if
c
      read (1,*)
      do i=1,nsm1
        read (1,*) s(i),v(i)
      enddo
c
      close (1)
c
c ****** Set boundary values.
c
      ne0=half*(ne(1)+ne(2))
      ne1=half*(ne(ns)+ne(nsm1))
c
c ****** Set the mass density from NE.
c
      call ne2rho
c
      return
      end
c#######################################################################
      subroutine read_initial_state (fname)
c
c-----------------------------------------------------------------------
c
c ****** Read the initial state from file FNAME.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use params
      use boundary_conditions
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      character(*) :: fname
c
c-----------------------------------------------------------------------
c
      integer :: i,npts,ierr
c
      real(r_typ), dimension(:), allocatable :: s_t
      real(r_typ), dimension(:), allocatable :: v_t
      real(r_typ), dimension(:), allocatable :: n_t
      real(r_typ), dimension(:), allocatable :: t_t
c
c-----------------------------------------------------------------------
c
c ****** Scan the file to determine the number of points in the table.
c
      call scan (fname,4,npts,ierr)
c
      if (ierr.ne.0) then
        i=0
        go to 900
      end if
c
c ****** Allocate memory.
c
      allocate (s_t(npts))
      allocate (v_t(npts))
      allocate (n_t(npts))
      allocate (t_t(npts))
c
c ****** Read the initial state.
c
      call ffopen (1,fname,'r',ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in READ_INITIAL_STATE:'
        write (*,*) '### Could not open the initial state file:'
        write (*,*) 'File name: ',trim(fname)
        call exit (1)
      end if
c
c ****** Skip the header.
c
      read (1,*)
c
c ****** Read the arc length, velocity, electron number density,
c ****** and temperature (in normalized units).
c
      do i=1,npts
        read (1,*,err=900,end=900) s_t(i),
     &                             v_t(i),
     &                             n_t(i),
     &                             t_t(i)
      enddo
c
      close (1)
c
c ****** Interpolate the fields to the code mesh.
c
      call interp_array (npts,s_t,v_t,nsm1,s,v)
c
      call interp_array (npts,s_t,n_t,ns,sh,ne)
      call interp_array (npts,s_t,t_t,ns,sh,temp)
c
      call extrapolate_boundary_values (npts,s_t,n_t,ne)
      call extrapolate_boundary_values (npts,s_t,t_t,temp)
c
c ****** Deallocate memory.
c
      deallocate (s_t)
      deallocate (v_t)
      deallocate (n_t)
      deallocate (t_t)
c
      return
c
  900 continue
c
      write (*,*)
      write (*,*) '### ERROR in READ_INITIAL_STATE:'
      write (*,*) '### Error while reading initial data.'
      write (*,*) 'File name: ',fname
      write (*,*) 'Line number: ',i
      call exit (1)
c
      end
c#######################################################################
      subroutine set_rad_balance_temp
c
c-----------------------------------------------------------------------
c
c ****** If requested, set the boundary temperature to the value
c ****** that balances the heating via radiative loss.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use fields
      use normalization_parameters
      use boundary_conditions
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
c ****** Convergence criterion for the boundary temperature
c ****** estimation (maximum fractional error in Q).
c
      real(r_typ), parameter :: eps=1.e-9_r_typ
c
c ****** Maximum number of iterations for the boundary temperature
c ****** estimation.
c
      integer, parameter :: max_iter=500
c
c ****** Maximum value of the temperature [degrees K] allowed
c ****** when searching for a solution.
c
      real(r_typ), parameter :: tmax_k=1.e6_r_typ
c
c ****** Temperature increment [degrees K] for the bisection method
c ****** when searching for a solution.
c
      real(r_typ), parameter :: dtemp_k=1._r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: h,qv
      real(r_typ) :: t0,tmax,dtemp
      real(r_typ) :: tg,q,f,tg0,tg1,f0,f1,err
      logical :: found_crossing
c
c-----------------------------------------------------------------------
c
      real(r_typ), external :: qrad
c
c-----------------------------------------------------------------------
c
c ****** If requested, find the temperature needed at the boundaries
c ****** to have a balance between heating and radiative loss.
c ****** Use the method of bisection to find the nonlinear solution.
c
c-----------------------------------------------------------------------
c ****** Boundary at s=S0.
c-----------------------------------------------------------------------
c
      if (set_rad_balance_t0) then
c
        t0=tbc0
c
c ****** Set the target value of Q desired.
c
        h=half*(heating(1)+heating(2))
        qv=h/(he_np*ne0**2)
c
c ****** Starting at T0, increment T until a crossing is found.
c
        tmax=tmax_k/fn_t
        dtemp=dtemp_k/fn_t
c
        tg=t0
        q=qrad(tg*fn_t)/fn_qrad
        f=q-qv
        if (f.eq.0.) go to 100
c
        found_crossing=.false.
        do while (tg.le.tmax)
          tg0=tg
          f0=f
          tg=tg+dtemp
          q=qrad(tg*fn_t)/fn_qrad
          f=q-qv
          if (f*f0.le.0.) then
            found_crossing=.true.
            exit
          end if
        enddo
c
        if (.not.found_crossing) then
          write (*,*)
          write (*,*) '### ERROR in SET_RAD_BALANCE_TEMP:'
          write (*,*) '### Could not find a temperature that'//
     &                ' balances heating and radiation at S0:'
          write (*,*) 'Target radiation loss [erg-cm^3/s] = ',
     &                qv*fn_qrad
          write (*,*) 'Starting search temperature [K] = ',t0*fn_t
          write (*,*) 'Ending search temperature [K]   = ',tmax_k
          write (*,*) 'Temperature increment [K]       = ',dtemp_k
          call exit (1)
        end if
c
c ****** Use the method of bisection to find the temperature.
c
        if (f.eq.0.) go to 100
c
        tg1=tg
        f1=f
c
        i=0
        do
          i=i+1
c
          tg=half*(tg0+tg1)
          q=qrad(tg*fn_t)/fn_qrad
          f=q-qv
c
          err=abs(q-qv)
          if (qv.ne.0.) err=err/abs(qv)
c
          if (err.le.eps) go to 100
c
          if (f*f0.le.0.) then
            f1=f
            tg1=tg
          else
            f0=f
            tg0=tg
          end if
c
          if (i.ge.max_iter) then
            write (*,*)
            write (*,*) '### ERROR in SET_RAD_BALANCE_TEMP:'
            write (*,*) '### The iteration to estimate the'//
     &                  ' radiation balance temperature'
            write (*,*) '### at S0 did not converge:'
            write (*,*) 'Target radiation loss [erg-cm^3/s] = ',
     &                  qv*fn_qrad
            write (*,*) 'Fractional error in Q = ',err
            write (*,*) 'Number of iterations = ',i
            call exit (1)
          end if
c
        enddo
c
  100   continue
c
        tbc0=tg
c
        write (*,*)
        write (*,*) '### COMMENT from SET_RAD_BALANCE_TEMP:'
        write (*,*) '### The radiation balance temperature was'//
     &              ' set at S0:'
        write (*,*) '### Boundary temperature [K] at S0 = ',tbc0*fn_t
c
      end if
c
c-----------------------------------------------------------------------
c ****** Boundary at s=S1.
c-----------------------------------------------------------------------
c
      if (set_rad_balance_t1) then
c
        t0=tbc1
c
c ****** Set the target value of Q desired.
c
        h=half*(heating(ns)+heating(nsm1))
        qv=h/(he_np*ne1**2)
c
c ****** Starting at T0, increment T until a crossing is found.
c
        tmax=tmax_k/fn_t
        dtemp=dtemp_k/fn_t
c
        tg=t0
        q=qrad(tg*fn_t)/fn_qrad
        f=q-qv
        if (f.eq.0.) go to 200
c
        found_crossing=.false.
        do while (tg.le.tmax)
          tg0=tg
          f0=f
          tg=tg+dtemp
          q=qrad(tg*fn_t)/fn_qrad
          f=q-qv
          if (f*f0.le.0.) then
            found_crossing=.true.
            exit
          end if
        enddo
c
        if (.not.found_crossing) then
          write (*,*)
          write (*,*) '### ERROR in SET_RAD_BALANCE_TEMP:'
          write (*,*) '### Could not find a temperature that'//
     &                ' balances heating and radiation at S1:'
          write (*,*) 'Target radiation loss [erg-cm^3/s] = ',
     &                qv*fn_qrad
          write (*,*) 'Starting search temperature [K] = ',t0*fn_t
          write (*,*) 'Ending search temperature [K]   = ',tmax_k
          write (*,*) 'Temperature increment [K]       = ',dtemp_k
          call exit (1)
        end if
c
c ****** Use the method of bisection to find the temperature.
c
        if (f.eq.0.) go to 200
c
        tg1=tg
        f1=f
c
        i=0
        do
          i=i+1
c
          tg=half*(tg0+tg1)
          q=qrad(tg*fn_t)/fn_qrad
          f=q-qv
c
          err=abs(q-qv)
          if (qv.ne.0.) err=err/abs(qv)
c
          if (err.le.eps) go to 200
c
          if (f*f0.le.0.) then
            f1=f
            tg1=tg
          else
            f0=f
            tg0=tg
          end if
c
          if (i.ge.max_iter) then
            write (*,*)
            write (*,*) '### ERROR in SET_RAD_BALANCE_TEMP:'
            write (*,*) '### The iteration to estimate the'//
     &                  ' radiation balance temperature'
            write (*,*) '### at S1 did not converge:'
            write (*,*) 'Target radiation loss [erg-cm^3/s] = ',
     &                  qv*fn_qrad
            write (*,*) 'Fractional error in Q = ',err
            write (*,*) 'Number of iterations = ',i
            call exit (1)
          end if
c
        enddo
c
  200   continue
c
        tbc1=tg
c
        write (*,*)
        write (*,*) '### COMMENT from SET_RAD_BALANCE_TEMP:'
        write (*,*) '### The radiation balance temperature was'//
     &              ' set at S1:'
        write (*,*) '### Boundary temperature [K] at S0 = ',tbc1*fn_t
c
      end if
c
      return
      end
c#######################################################################
      subroutine collect_histories
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use params
      use diagnostic_output
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: vv,fke,fie
c
c-----------------------------------------------------------------------
c
      if (ihist.ge.n_hist_max) then
        call write_histories
      end if
c
      ihist=ihist+1
      hist_t(ihist)=time
c
      hist_ne(ihist)=ne(s_mesh_hist_index)
      hist_temp(ihist)=temp(s_mesh_hist_index)
      hist_v(ihist)=v(s_mesh_hist_index)
c
c ****** Calculate the kinetic and internal energy.
c
      fke=0.
      do i=2,nsm1
        vv=half*(v(i)+v(i-1))
        fke=fke+half*rho(i)*vv**2*areah(i)*dsh(i)
      enddo
c
      fie=0.
      do i=2,nsm1
        fie=fie+p(i)*areah(i)*dsh(i)/(gamma-one)
      enddo
c
      hist_ke(ihist)=fke
      hist_ie(ihist)=fie
c
      return
      end
c#######################################################################
      subroutine write_histories
c
c-----------------------------------------------------------------------
c
      use globals
      use mesh
      use fields
      use params
      use diagnostic_output
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      integer :: i,ierr
      character(16) :: fname
      character(2) :: ch2
c
c-----------------------------------------------------------------------
c
      integer, save :: iseq=0
c
c-----------------------------------------------------------------------
c
      if (ihist.le.0) return
c
      iseq=iseq+1
      write (ch2,'(i2.2)') iseq
      fname='hist'//ch2//'.dat'
c
      call ffopen (1,fname,'rw',ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in WRITE_HISTORIES:'
        write (*,*) '### Could not open the history output file:'
        write (*,*) 'File name: ',trim(fname)
        call exit (1)
      end if
c
      write (1,900) 't'  ,char(9),
     &              'v'  ,char(9),
     &              'n_e',char(9),
     &              'T'  ,char(9),
     &              'KE' ,char(9),
     &              'IE'
      do i=1,ihist
        write (1,910) hist_t(i)   ,char(9),
     &                hist_v(i)   ,char(9),
     &                hist_ne(i)  ,char(9),
     &                hist_temp(i),char(9),
     &                hist_ke(i)  ,char(9),
     &                hist_ie(i)
      enddo
c
      close (1)
c
  900 format (10(a,a))
  910 format (10(1pe21.14,a))
c
      write (*,*)
      write (*,*) '### COMMENT from WRITE_HISTORIES:'
      write (*,*) '### Wrote time histories at t = ',time
      write (*,*) 'History file name: ',trim(fname)
c
c ****** Reset the history counter.
c
      ihist=0
c
      return
      end
c#######################################################################
      subroutine save_dump_fields
c
c-----------------------------------------------------------------------
c
c ****** Save the fields requested to the dump buffers.
c
c ****** These are written to HDF files at the end of the run.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use fields
      use params
      use normalization_parameters
      use diagnostic_output
      use energy_diagnostics
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i,n
      real(r_typ) :: scale_fac
      logical :: energy_diag_needed
c
c-----------------------------------------------------------------------
c
      logical, save :: first_call=.true.
c
c-----------------------------------------------------------------------
c
      if (nplot.le.0) return
c
c ****** On the first time in, allocate the dump buffers.
c
      if (first_call) then
c
        allocate (dump_fld_buffer(nplot))
c
c ****** Set the pointers to the fields.  This also sets the
c ****** units of the output fields.
c
        do i=1,nplot
          select case (plot_index(i))
          case (DIAG_FLD_T)
            dump_fld_buffer(i)%scaling_factor=fn_t*1.e-6_r_typ
            dump_fld_buffer(i)%source=>temp
          case (DIAG_FLD_NE)
            dump_fld_buffer(i)%scaling_factor=fn_n
            dump_fld_buffer(i)%source=>ne
          case (DIAG_FLD_V)
            dump_fld_buffer(i)%scaling_factor=fn_v*1.e-5_r_typ
            dump_fld_buffer(i)%source=>v
          case (DIAG_FLD_P)
            dump_fld_buffer(i)%scaling_factor=fn_p
            dump_fld_buffer(i)%source=>p
          case (DIAG_FLD_H)
            dump_fld_buffer(i)%scaling_factor=one
            dump_fld_buffer(i)%source=>h_h
          case (DIAG_FLD_QC)
            dump_fld_buffer(i)%scaling_factor=one
            dump_fld_buffer(i)%source=>h_qc
          case (DIAG_FLD_QR)
            dump_fld_buffer(i)%scaling_factor=one
            dump_fld_buffer(i)%source=>h_qr
          case (DIAG_FLD_PV)
            dump_fld_buffer(i)%scaling_factor=one
            dump_fld_buffer(i)%source=>h_pv
          case (DIAG_FLD_Q_TC)
            dump_fld_buffer(i)%scaling_factor=one
            dump_fld_buffer(i)%source=>h_q_tc
          end select
        enddo
c
c ****** Allocate memory for the field buffers.
c
        do i=1,nplot
          n=size(dump_fld_buffer(i)%source)
          allocate (dump_fld_buffer(i)%f(n,n_dump_buffer))
        enddo
c
c ****** Allocate the buffer to hold the time values at the
c ****** dump steps.
c
        allocate (dump_t(n_dump_buffer))
c
        first_call=.false.
c
      end if
c
c ****** Increment the dump counter.
c
      n_dump=n_dump+1
c
c ****** Expand the buffers if needed.
c
      if (n_dump.gt.n_dump_buffer) then
        call expand_dump_buffers
      end if
c
c ****** Store the time of the dump (in seconds).
c
      dump_t(n_dump)=time*fnormt
c
c ****** Calculate the terms in the energy diagnostic if they are
c ****** being plotted.
c
      energy_diag_needed=.false.
      do i=1,nplot
        select case (plot_index(i))
        case (DIAG_FLD_H,DIAG_FLD_QR,DIAG_FLD_QC,DIAG_FLD_PV,
     &        DIAG_FLD_Q_TC)
          energy_diag_needed=.true.
        end select
      enddo
      if (energy_diag_needed) call get_energy_terms
c
c ****** Store the fields.
c
      do i=1,nplot
        scale_fac=dump_fld_buffer(i)%scaling_factor
        dump_fld_buffer(i)%f(:,n_dump)=dump_fld_buffer(i)%source
     &                                 *scale_fac
      enddo
c
      return
      end
c#######################################################################
      subroutine expand_dump_buffers
c
c-----------------------------------------------------------------------
c
c ****** Expand the buffers that hold the dump fields by doubling
c ****** them.
c
c-----------------------------------------------------------------------
c
      use number_types
      use dump_buf_def
      use diagnostic_output
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      integer :: i,n,n_prev
      real(r_typ), dimension(:), pointer :: t1
      real(r_typ), dimension(:,:), pointer :: t2
c
c-----------------------------------------------------------------------
c
      n_prev=n_dump_buffer
c
      n_dump_buffer=2*n_dump_buffer
c
c ****** Expand the 1D buffer DUMP_T.
c
      allocate (t1(n_prev))
      t1(1:n_prev)=dump_t(1:n_prev)
      deallocate (dump_t)
      allocate (dump_t(n_dump_buffer))
      dump_t(1:n_prev)=t1(1:n_prev)
      deallocate (t1)
cccc ****** The following code is more efficient, but ifort v12.0
cccc ****** has a bug that causes a core dump!
ccc      allocate (t1(n_dump_buffer))
ccc      t1(1:n_prev)=dump_t(1:n_prev)
ccc      deallocate (dump_t)
ccc      dump_t=>t1
c
c ****** Expand the 2D buffers DUMP_FLD_BUFFER(:)%F.
c
      do i=1,nplot
        n=size(dump_fld_buffer(i)%source)
        allocate (t2(n,n_prev))
        t2(:,1:n_prev)=dump_fld_buffer(i)%f(:,1:n_prev)
        deallocate (dump_fld_buffer(i)%f)
        allocate (dump_fld_buffer(i)%f(n,n_dump_buffer))
        dump_fld_buffer(i)%f(:,1:n_prev)=t2(:,1:n_prev)
        deallocate (t2)
cccc ****** The following code is more efficient, but ifort v12.0
cccc ****** has a bug that causes a core dump!
ccc        allocate (t2(n,n_dump_buffer))
ccc        t2(:,1:n_prev)=dump_fld_buffer(i)%f(:,1:n_prev)
ccc        deallocate (dump_fld_buffer(i)%f)
ccc        dump_fld_buffer(i)%f=>t2
      enddo
c
      return
      end
c#######################################################################
      subroutine write_fields_txt
c
c-----------------------------------------------------------------------
c
c ****** Write the field dumps to plain text files in SDS format.
c
c-----------------------------------------------------------------------
c
      use globals
      use mesh
      use fields
      use params
      use normalization_parameters
      use diagnostic_output
      use lcase_interface
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      integer :: i,n,ierr
      character(16) :: fld
      character(32) :: fname
      real(r_typ), dimension(:), allocatable :: s_mm
c
c-----------------------------------------------------------------------
c
      write (*,*)
c
      do i=1,nplot
c
        fld=diag_fld_name(plot_index(i))
        fname=lcase(trim(fld))//'.dat'
c
        n=size(dump_fld_buffer(i)%source)
c
        if (n.eq.ns) then
          allocate (s_mm(ns))
          s_mm=sh*fnorml*1.e-8_r_typ
          call wrtxt_2d (fname,.true.,ns,n_dump,
     &                   dump_fld_buffer(i)%f,s_mm,dump_t,ierr)
          deallocate (s_mm)
        else
          allocate (s_mm(nsm1))
          s_mm=s*fnorml*1.e-8_r_typ
          call wrtxt_2d (fname,.true.,nsm1,n_dump,
     &                   dump_fld_buffer(i)%f,s_mm,dump_t,ierr)
          deallocate (s_mm)
        end if
c
        if (ierr.ne.0) then
          write (*,*)
          write (*,*) '### WARNING from WRITE_FIELDS_TXT:'
          write (*,*) '### Could not write a field to a text file.'
          write (*,*) 'Field name: ',trim(fld)
          write (*,*) 'File name: ',trim(fname)
          write (*,*) 'IERR (from WRTXT_2D) = ',ierr
        else
          write (*,*) '### Wrote field: ',trim(fld),
     &                ' to file: ',trim(fname)
        end if
c
      enddo
c
      return
      end
c#######################################################################
      subroutine setdt
c
c-----------------------------------------------------------------------
c
c ****** Set the time step.
c
c-----------------------------------------------------------------------
c
c ****** If the semi-implicit term is being used (SEMI_IMPLICIT=.t.),
c ****** the time step is set to the smaller of DTMAX and
c ****** the advective time step limit.
c
c ****** Otherwise (i.e., fully explicit), the time step is
c ****** set to the smaller of DTMAX and the fraction
c ****** CFL of the "CFL" limit.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use params
      use normalization_parameters
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: two=2._r_typ
      real(r_typ), parameter :: half=.5_r_typ
      real(r_typ), parameter :: quarter=.25_r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: tav,cs,sicof,fkmax2,beta,cflv
c
c-----------------------------------------------------------------------
c
      real(r_typ), save :: dt_old=-1.
c
c-----------------------------------------------------------------------
c
      if (time.lt.t_small_dt) then
        dt=dtmax_initial
      else
        dt=dtmax
      end if
c
      if (dt_old.gt.0.) then
        dt=min(1.05*dt_old,dt)
      end if
c
c ****** Apply the radiative loss law limit to the time step.
c
      call apply_dt_rad_limit
c
      if (.not.semi_implicit) then
c
c ****** Fully explicit algorithm.
c
        do i=1,nsm1
          tav=half*(temp(i)+temp(i+1))
          cs=sqrt(gamma*he_p*tav/he_rho)
          dt=min(dt,cfl*ds(i)/(abs(v(i))+cs))
          sifac(i)=0.
        enddo
c
      else
c
c ****** Semi-implicit algorithm.
c
c ****** First, set the advective limit.
c
        do i=1,nsm1
          dt=min(dt,cfl*ds(i)/max(abs(v(i)),tiny(one)))
        enddo
c
c ****** Next, compute the semi-implicit coefficient.
c
        sicof=0.
        do i=1,nsm1
          tav=half*(temp(i)+temp(i+1))
          cs=sqrt(abs(gamma*he_p*tav/he_rho))
          fkmax2=two/ds(i)
          beta=dt*cs*fkmax2
          cflv=v(i)*dt/ds(i)
          sifac(i)=simult*max(0.,( quarter*beta**2
     &                             /(one-fac_cflv*cflv)**2
     &                            -one)/(dt*fkmax2**2))
        enddo
c
      end if
c
      dt_old=dt
c
      return
      end
c#######################################################################
      subroutine apply_dt_rad_limit
c
c-----------------------------------------------------------------------
c
c ****** Set the time step to satisfy the limit placed by the
c ****** radiative loss law.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use normalization_parameters
      use heating_parameters
      use params
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
c
c-----------------------------------------------------------------------
c
c ****** Factor by which to multiply the estimated time step limit.
c
      real(r_typ), parameter :: safety_factor=.4_r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: dt_rad,q,qp,alpha,fac,dt_lim
c
c-----------------------------------------------------------------------
c
      real(r_typ), external :: qrad
      real(r_typ), external :: d_qrad
c
c-----------------------------------------------------------------------
c
c ****** Find the time step limit set by the radiative loss law.
c
      dt_rad=huge(one)
      do i=2,nsm1
        q=qrad(fn_t*temp(i))/fn_qrad
        qp=d_qrad(fn_t*temp(i))*fn_t/fn_qrad
        alpha=radloss*(gamma-one)*(ne(i)*he_np/he_p)
        fac=q/temp(i)-qp
        if (fac.gt.0.) then
          dt_lim=one/(alpha*fac)
          dt_rad=min(dt_rad,dt_lim)
        end if
      enddo
c
c ****** Set the time step to be smaller than that set by the
c ****** radiative loss law limit, with the appropriate safety
c ****** factor.
c
      dt=min(dt,safety_factor*dt_rad)
c
      return
      end
c#######################################################################
      subroutine rho2ne
c
c-----------------------------------------------------------------------
c
c ****** Set the electron number density NE from the mass density
c ****** RHO (in normalized units).
c
c-----------------------------------------------------------------------
c
      use fields
      use normalization_parameters
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      ne=rho/he_rho
c
      return
      end
c#######################################################################
      subroutine ne2rho
c
c-----------------------------------------------------------------------
c
c ****** Set the mass density RHO from the electron number
c ****** density NE (in normalized units).
c
c-----------------------------------------------------------------------
c
      use fields
      use normalization_parameters
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      rho=he_rho*ne
c
      return
      end
c#######################################################################
      subroutine advance_t
c
c-----------------------------------------------------------------------
c
c ****** Advance the temperature.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use normalization_parameters
      use heating_parameters
      use params
      use boundary_conditions
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: two=2._r_typ
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: wavefac
      real(r_typ) :: np,q,qp
      real(r_typ) :: fac
      real(r_typ) :: kappa_m,kappa_p
c
      real(r_typ) :: ne0v,ne1v
      real(r_typ) :: nerad0,nerad1
      real(r_typ) :: rint,dtds
      real(r_typ) :: t0,tmin,h
c
      real(r_typ), dimension(ns) :: temp_p
      real(r_typ), dimension(ns) :: divv,div_tv
      real(r_typ), dimension(ns) :: source_term
      real(r_typ), dimension(ns) :: q_impl
      real(r_typ), dimension(ns) :: aa,bb,cc
c
c-----------------------------------------------------------------------
c
      real(r_typ), external :: qrad
      real(r_typ), external :: d_qrad
      real(r_typ), external :: radint
c
c-----------------------------------------------------------------------
c
c ****** Get the thermal conductivity.
c
      call get_kappa (temp)
c
c ****** Get div(v).
c
      do i=2,nsm1
        divv(i)=( area(i  )*v(i  )
     &           -area(i-1)*v(i-1)
     &          )/(areah(i)*dsh(i))
      enddo
c
c ****** Predict the temperature if requested.
c
      temp_p=temp
c
      if (use_predictor) then
        if (use_flow_in_predictor) then
          call divfv (temp,v,div_tv)
          do i=2,nsm1
            temp_p(i)=temp_p(i)-div_tv(i)*dt
          enddo
          wavefac=one
        else
          wavefac=0.
        endif
c
c ****** Add the wave terms in the predictor.
c
        do i=2,nsm1
          temp_p(i)= temp_p(i)
     &              -( betapc_t*(gamma-one)
     &                -wavefac
     &               )*temp(i)*divv(i)*dt
        enddo
c
c ****** Set boundary conditions.
c
        call set_bc_temp (temp_p)
c
      endif
c
c ****** Corrector step.
c
      call divfv (temp_p,v,div_tv)
c
c ****** Collect the source terms in the temperature equation.
c
c ****** Add the advective term and the -(gamma-2)*T*div(v) part.
c
      do i=2,nsm1
        source_term(i)=-div_tv(i)
     &                 -(gamma-two)*temp(i)*divv(i)
      enddo
c
c ****** Add the heating term.
c
      do i=2,nsm1
        source_term(i)= source_term(i)
     &                 +(gamma-one)*heating(i)/(ne(i)*he_p)
      enddo
c
c ****** Add the radiative loss term.  Note that the implicit
c ****** radiation contribution is saved in the factor Q_IMPL.
c
      do i=2,nsm1
        np=radloss*ne(i)*he_np
        q=qrad(fn_t*temp(i))/fn_qrad
        qp=d_qrad(fn_t*temp(i))*fn_t/fn_qrad
        source_term(i)= source_term(i)
     &                 -(gamma-one)*np*q/he_p
        q_impl(i)=one/(one+dt*(gamma-one)*np*qp/he_p)
      enddo
c
c ****** Advance the temperature by the source terms.
c
      do i=2,nsm1
        temp(i)=temp(i)+source_term(i)*dt*q_impl(i)
      enddo
c
c ****** If TCOND=0, assume we are doing a polytropic energy
c ****** equation case.
c
      if (tcond.eq.0.) then
        call set_bc_temp (temp)
        return
      end if
c
c ****** Construct the matrix coefficients for the implicit
c ****** advance of the thermal conduction term.
c
      do i=2,nsm1
        fac=(gamma-one)*dt*q_impl(i)/(ne(i)*he_p*fn_kappa)
        kappa_m=half*(kappa(i)+kappa(i-1))
        kappa_p=half*(kappa(i)+kappa(i+1))
        aa(i)=one+fac*( kappa_p*area(i  )/ds(i  )
     &                 +kappa_m*area(i-1)/ds(i-1)
     &                )/(areah(i)*dsh(i))
        bb(i)=-fac*(kappa_p*area(i  )/ds(i  ))
     &            /(areah(i)*dsh(i))
        cc(i)=-fac*(kappa_m*area(i-1)/ds(i-1))
     &            /(areah(i)*dsh(i))
      enddo
c
c ****** Set the boundary conditions.
c
      select case (ibc_t_0)
      case (IBC_FIXED)
        aa(1)=half
        bb(1)=half
        temp(1)=tbc0
      case (IBC_ISOLATED)
        aa(1)=half
        bb(1)=-half
        temp(1)=0.
      end select
c
      select case (ibc_t_1)
      case (IBC_FIXED)
        aa(ns)=half
        cc(ns)=half
        temp(ns)=tbc1
      case (IBC_ISOLATED)
        aa(ns)=half
        cc(ns)=-half
        temp(ns)=0.
      end select
c
c ****** Solve the tridiagonal system.
c
      call trid (ns,cc,aa,bb,temp)
c
c ****** If using "radiation balance BCs", get the radiation integral
c ****** in the chromosphere-transition region boundary and use it
c ****** to estimate the boundary density for the next step.
c
      if (ibc_ne_0.eq.IBC_RADBC) then
        rint=radint(fn_t*tbc0)
        dtds=(temp(2)-temp(1))/ds(1)
        dtds=max(dtds,0.)
        h=half*(heating(1)+heating(2))
        nerad0=sqrt(( fn_radbc1*dtds**2*(fn_t*tbc0)**3
     &               +fn_radbc2*h*(fn_t*tbc0)**1.5)/rint)
        ne0v=nerad0
      else
        ne0v=ne_target0
      endif
c
      if (ibc_ne_1.eq.IBC_RADBC) then
        rint=radint(fn_t*tbc1)
        dtds=(temp(nsm1)-temp(ns))/ds(nsm1)
        dtds=max(dtds,0.)
        h=half*(heating(ns)+heating(nsm1))
        nerad1=sqrt(( fn_radbc1*dtds**2*(fn_t*tbc1)**3
     &               +fn_radbc2*h*(fn_t*tbc1)**1.5)/rint)
        ne1v=nerad1
      else
        ne1v=ne_target1
      endif
c
c ****** Advance the base densities NE0 and NE1 towards the
c ****** required values with a time constant NE_ADVANCE_TIME_CONST.
c
      tmin=abs((one-ne0v/ne0)*dt/ne_advance_frac_lim)
      t0=max(ne_advance_time_const,tmin)
      ne0=(ne0+ne0v*dt/t0)/(one+dt/t0)
c
      tmin=abs((one-ne1v/ne1)*dt/ne_advance_frac_lim)
      t0=max(ne_advance_time_const,tmin)
      ne1=(ne1+ne1v*dt/t0)/(one+dt/t0)
c
      return
      end
c#######################################################################
      subroutine set_bc_temp (t)
c
c-----------------------------------------------------------------------
c
c ****** Set boundary conditions on the temperature in array T.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use boundary_conditions
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: two=2._r_typ
c
c-----------------------------------------------------------------------
c
      real(r_typ), dimension(ns) :: t
c
c-----------------------------------------------------------------------
c
      select case (ibc_t_0)
      case (IBC_FIXED)
        t(1)=two*tbc0-t(2)
      case (IBC_ISOLATED)
        t(1)=t(2)
      end select
c
      select case (ibc_t_1)
      case (IBC_FIXED)
        t(ns)=two*tbc1-t(nsm1)
      case (IBC_ISOLATED)
        t(ns)=t(nsm1)
      end select
c
      return
      end
c#######################################################################
      subroutine set_bc_ne (ne)
c
c-----------------------------------------------------------------------
c
c ****** Set boundary conditions on the density in array NE.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use boundary_conditions
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: two=2._r_typ
c
c-----------------------------------------------------------------------
c
      real(r_typ), dimension(ns) :: ne
c
c-----------------------------------------------------------------------
c
      select case (ibc_ne_0)
      case (IBC_FIXED,IBC_RADBC)
        ne(1)=two*ne0-ne(2)
      case (IBC_ISOLATED)
        ne(1)=ne(2)
      end select
c
      select case (ibc_ne_1)
      case (IBC_FIXED,IBC_RADBC)
        ne(ns)=two*ne1-ne(nsm1)
      case (IBC_ISOLATED)
        ne(ns)=ne(nsm1)
      end select
c
      return
      end
c#######################################################################
      subroutine set_p
c
c-----------------------------------------------------------------------
c
      use globals
      use mesh
      use fields
      use normalization_parameters
      use params
c
c-----------------------------------------------------------------------
c
c ****** Set the pressure.
c
      p=he_p*ne*temp
c
c ****** Check that the pressure is non-negative.
c
      do i=1,ns
        if (p(i).lt.0.) then
          write (*,*)
          write (*,*) '### ERROR in SET_P:'
          write (*,*) '### The pressure is negative.'
          write (*,*) 'ITIME = ',itime
          write (*,*) 'TIME = ',time
          write (*,*) 'I = ',i
          write (*,*) 'SH(I) = ',sh(i)
          write (*,*) 'P(I) = ',p(i)
          ifabort=.true.
          call final_diags
        end if
      enddo
c
      return
      end
c#######################################################################
      subroutine divfv (f,v,div)
c
c-----------------------------------------------------------------------
c
c ****** Return div(f*v) in array DIV for a quantity F defined on
c ****** the half-mesh.
c
c ****** DIV is computed on the internal points.
c
c-----------------------------------------------------------------------
c
c ****** The field F is assumed to be defined on the half-mesh and
c ****** the velocity V is assumed to be defined on the main mesh.
c
c ****** Depending on the the value of switch UPWIND, either use
c ****** upwind or centered differencing for the fluxes.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use params
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), dimension(ns) :: f
      real(r_typ), dimension(nsm1) :: v
      real(r_typ), dimension(ns) :: div
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: cadvm,cadvp
      real(r_typ) :: fpp,fmp,fpm,fmm
c
c-----------------------------------------------------------------------
c
c ****** Compute div(f*v) at the internal points.
c ****** The boundary points, which are not used, are set to 0.
c
      do i=2,nsm1
        cadvp=sign(upwind,v(i  ))
        cadvm=sign(upwind,v(i-1))
        fpp=half*(one-cadvp)
        fmp=half*(one+cadvp)
        fpm=half*(one-cadvm)
        fmm=half*(one+cadvm)
        div(i)=( area(i  )*v(i  )*(fpp*f(i+1)+fmp*f(i  ))
     &          -area(i-1)*v(i-1)*(fpm*f(i  )+fmm*f(i-1))
     &         )/(areah(i)*dsh(i))
      enddo
c
c ****** Set boundary points, which are not used, to zero.
c
      div( 1)=0.
      div(ns)=0.
c
      return
      end
c#######################################################################
      function boost (tempk)
c
c-----------------------------------------------------------------------
c
c ****** Boost function for thermal conductivity and radiative loss.
c
c ****** TEMPK is the temperature in degrees Kelvin.
c
c-----------------------------------------------------------------------
c
      use number_types
      use heating_parameters
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: tempk
      real(r_typ) :: boost
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
c
c-----------------------------------------------------------------------
c
c ****** Power of temperature for the thermal conductivity (Spitzer).
c
      real(r_typ), parameter :: power=2.5_r_typ
c
c-----------------------------------------------------------------------
c
      if (tempk.lt.t_cutoff) then
        boost=(tempk/t_cutoff)**(p_cutoff-power)
      else
        boost=one
      end if
c
      return
      end
c#######################################################################
      subroutine initialize_qrad (rad_law_type,zero_q_chromo_base,
     &                            t_chromo_base,t_chromo_plus)
c
c-----------------------------------------------------------------------
c
c ****** Initialize the radiative loss law.
c
c-----------------------------------------------------------------------
c
c ****** When ZERO_Q_CHROMO_BASE=.true., use a modification that
c ****** sets Q to zero at the base of the chromosphere (assumed
c ****** to be at temperature T_CHROMO_BASE in degrees [K]).  Q is
c ****** set to zero for temperatures below T_CHROMO_BASE.  The
c ****** modification is carried out for temperatures below
c ****** T_CHROMO_PLUS (in degrees [K]).
c
c ****** When ZERO_Q_CHROMO_BASE=.false., the radiative loss law is
c ****** not modified.
c
c-----------------------------------------------------------------------
c
c ****** The flag LEGACY_Q_CHROMO_REDUCTION (passed via module
c ****** RADIATIVE_LOSS_LAW) takes precedence.  If it is set, then the
c ****** legacy way of reducing Q near the base of the chromosphere
c ****** (originally developed by Yung Mok) is used.  This is not
c ****** recommended; it is provided for backward compatibility.
c
c-----------------------------------------------------------------------
c
      use number_types
      use radiative_loss_law
      use lcase_interface
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      character(*) :: rad_law_type
      logical :: zero_q_chromo_base
      real(r_typ) :: t_chromo_base
      real(r_typ) :: t_chromo_plus
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: two=2._r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: q1,qp1
c
c-----------------------------------------------------------------------
c
      real(r_typ), external :: qrad
      real(r_typ), external :: d_qrad
c
c-----------------------------------------------------------------------
c
c ****** Check that the radiative loss law requested is valid.
c
      rad_law_index=0
      do i=1,n_rad_law
        if (lcase(rad_law_type).eq.lcase(rad_law_name(i))) then
          rad_law_index=i
          exit
        end if
      enddo
c
      if (rad_law_index.eq.0) then
        write (*,*)
        write (*,*) '### ERROR in INITIALIZE_QRAD:'
        write (*,*) '### The requested radiative loss law'//
     &              ' is not valid: ',
     &              trim(rad_law_type)
        write (*,*)
        write (*,*) '### The radiative loss law must be one'//
     &              ' of the following:'
        do i=1,n_rad_law
          write (*,*) ''''//trim(rad_law_name(i))//''''
        enddo
        call exit (1)
      end if
c
      write (*,*)
      write (*,*) '### Radiative loss law selected: ',
     &            trim(rad_law_type)
c
c ****** Set the flag to indicate that the radiative loss law has
c ****** been initialized.
c
      rad_law_initialized=.true.
c
c ****** If the option to make Q go to zero at the base of the
c ****** chromosphere was selected, compute the parameters
c ****** required.
c
      if (zero_q_chromo_base) then
c
        write (*,*)
        write (*,*) '### The radiative loss function is being set'//
     &              ' to zero at the base of'
        write (*,*) '### the chromosphere.'
c
c ****** Check that T_CHROMO_BASE is valid.
c
        if (t_chromo_base.le.0.) then
          write (*,*)
          write (*,*) '### ERROR in INITIALIZE_QRAD:'
          write (*,*) '### The temperature at the base of the'//
     &                ' chromosphere must be positive:'
          write (*,*) 'T_CHROMO_BASE = ',t_chromo_base
          call exit (1)
        end if
c
c ****** Check if the legacy (Yung Mok) reduction scheme is being
c ****** requested.
c
        if (legacy_q_chromo_reduction) then
c
          t_zqc_0=t_chromo_base
          zero_q_at_chromo_base=.true.
          apply_inverse_boost_to_q=.true.
c
          write (*,*)
          write (*,*) '### The legacy scheme (developed by Yung'//
     &                ' Mok) is being used:'
          write (*,*) 'Base temperature [K] = ',t_chromo_base
c
        else
c
c ****** Check that T_CHROMO_PLUS is valid.
c
          if (t_chromo_plus.le.t_chromo_base) then
            write (*,*)
            write (*,*) '### ERROR in INITIALIZE_QRAD:'
            write (*,*) '### The temperatures that define the'//
     &                  ' region in which Q goes to zero at the'
            write (*,*) '### base of the chromosphere are invalid:'
            write (*,*) '### It is required that T_CHROMO_PLUS >'//
     &                  ' T_CHROMO_BASE.'
            write (*,*) 'T_CHROMO_BASE = ',t_chromo_base
            write (*,*) 'T_CHROMO_PLUS = ',t_chromo_plus
            call exit (1)
          end if
c
c ****** Store the temperatures in the RADIATIVE_LOSS_LAW module.
c
          t_zqc_0=t_chromo_base
          t_zqc_1=t_chromo_plus
c
c ****** Set the flags that control the radiative loss law
c ****** (temporarily) so that Q is evaluated properly when
c ****** determining the coefficients A_ZQC and B_ZQC that
c ****** define the modification of Q.
c
          zero_q_at_chromo_base=.false.
          apply_inverse_boost_to_q=.false.
c
c ****** Get the values of Q and Q' at T_CHROMO_PLUS.
c
          q1=qrad(t_zqc_1)
          qp1=d_qrad(t_zqc_1)
c
c ****** Set the coefficients.
c
          a_zqc=two*q1-qp1*(t_zqc_1-t_zqc_0)
          b_zqc=q1-qp1*(t_zqc_1-t_zqc_0)
c
c ****** Reset the flags to their normal values.
c
          zero_q_at_chromo_base=.true.
          apply_inverse_boost_to_q=.true.
c
          write (*,*)
          write (*,*) '### Region over which Q is being modified:'
          write (*,*) 'Base temperature [K]  = ',t_chromo_base
          write (*,*) 'Upper temperature [K] = ',t_chromo_plus
c
        end if
c
      else
c
        zero_q_at_chromo_base=.false.
        apply_inverse_boost_to_q=.true.
c
        write (*,*)
        write (*,*) '### The radiative loss function is NOT'//
     &              ' being set to zero at the base of'
        write (*,*) '### the chromosphere.'
c
      end if
c
      return
      end
c#######################################################################
      function qrad (tempk)
c
c-----------------------------------------------------------------------
c
c ****** Select the radiative loss law, based on the value of the
c ****** index RAD_LAW_INDEX.
c
c-----------------------------------------------------------------------
c
      use number_types
      use radiative_loss_law
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: tempk
      real(r_typ) :: qrad
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: q
c
c-----------------------------------------------------------------------
c
      real(r_typ), external :: qrad_athay
      real(r_typ), external :: qrad_rosner
      real(r_typ), external :: qrad_rtv
      real(r_typ), external :: qrad_chianti_corona
      real(r_typ), external :: qrad_chianti_photo
      real(r_typ), external :: boost
c
c-----------------------------------------------------------------------
c
c ****** Check that the radiative loss law has been initialized.
c
      if (.not.rad_law_initialized) then
        write (*,*)
        write (*,*) '### ERROR in QRAD:'
        write (*,*) '### The radiative loss law has not'//
     &              ' been initialized.'
        write (*,*) '### The routine INITIALIZE_QRAD must be'//
     &              ' called prior to calling QRAD.'
        call exit (1)
      end if
c
c ****** Call the appropriate radiative loss law.
c
      select case (rad_law_index)
      case (RAD_LAW_ATHAY)
        q=qrad_athay(tempk)
      case (RAD_LAW_ROSNER)
        q=qrad_rosner(tempk)
      case (RAD_LAW_RTV)
        q=qrad_rtv(tempk)
      case (RAD_LAW_CHIANTI_CORONA)
        q=qrad_chianti_corona(tempk)
      case (RAD_LAW_CHIANTI_PHOTO)
        q=qrad_chianti_photo(tempk)
      case default
        write (*,*)
        write (*,*) '### ERROR in QRAD:'
        write (*,*) '### The radiative loss law has not'//
     &              ' been initialized.'
        write (*,*) '### The routine INITIALIZE_QRAD must be'//
     &              ' called prior to calling QRAD.'
        call exit (1)
      end select
c
c ****** Apply the reduction of Q at the base of the chromosphere,
c ****** if requested.
c
      if (zero_q_at_chromo_base) then
        call q_chromo_mod (tempk,q)
      end if
c
c ****** Attenuate Q by the boost factor for thermal conductivity.
c
      if (apply_inverse_boost_to_q) then
        q=q/boost(tempk)
      end if
c
      qrad=q
c
      return
      end
c#######################################################################
      subroutine q_chromo_mod (tempk,q)
c
c-----------------------------------------------------------------------
c
c ****** Apply the modification to Q at the base of the chromosphere.
c
c ****** The value of Q is overwritten by the modified value.
c
c-----------------------------------------------------------------------
c
      use number_types
      use radiative_loss_law
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: tempk
      real(r_typ) :: q
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: small_value=tiny(1._r_typ)
c
c-----------------------------------------------------------------------
c
c ****** Parameter to make Q'(T) continuous at T_ZQC_0.  This adds
c ****** a narrow layer near T_ZQC_0 in which Q is quadratic in T,
c ****** avoiding a discontinuity at T_ZQC_0.
c
      real(r_typ), parameter :: alpha0=.01_r_typ
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: fac
      real(r_typ) :: alpha,g
c
c-----------------------------------------------------------------------
c
c ****** Reduce the radiative loss to zero smoothly at the base
c ****** of the chromosphere.
c
      if (legacy_q_chromo_reduction) then
c
c ****** Use the legacy scheme (originally developed by Yung Mok)
c ****** if requested.  This option is not recommended; it is
c ****** provided for backward compatibility.
c
        if (tempk.le.t_zqc_0) then
          q=0.
        else
          fac=(tempk/t_zqc_0)**4-one
          fac=one/max(fac,small_value)
          q=q*exp(-fac)
        end if
c
      else
c
c ****** Make Q go to zero at the base of the chromosphere
c ****** smoothly.  Q and Q' match the radiative loss law values
c ****** at T_ZQC_1.
c
       if (tempk.le.t_zqc_0) then
          q=0.
        else if (tempk.le.t_zqc_1) then
          alpha=(tempk-t_zqc_0)/(t_zqc_1-t_zqc_0)
          g=one-exp(-alpha/alpha0)
          q=(a_zqc-b_zqc*alpha)*alpha*g
        end if

      end if
c
      return
      end
c#######################################################################
      function d_qrad (tempk)
c
c-----------------------------------------------------------------------
c
c ****** Derivative of radiative loss function in [erg-cm**3/s/K].
c
c ****** TEMPK is the temperature in degrees Kelvin.
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: tempk
      real(r_typ) :: d_qrad
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
c ****** Fractional increment to use in evaluating the derivative
c ****** numerically.
c
      real(r_typ), parameter :: eps=1.e-4_r_typ
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: tm,tp,dt
c
c-----------------------------------------------------------------------
c
      real(r_typ), external :: qrad
c
c-----------------------------------------------------------------------
c
c ****** Set temperature values that will be used to evaluate
c ****** the derivative of Q numerically.
c
      dt=eps*tempk
      tm=tempk-half*dt
      tp=tempk+half*dt
c
      d_qrad=(qrad(tp)-qrad(tm))/dt
c
      return
      end
c#######################################################################
      function qrad_athay (tempk)
c
c-----------------------------------------------------------------------
c
c ****** Athay radiative loss function in [erg-cm**3/s].
c
c ****** TEMPK is the temperature in degrees Kelvin.
c
c-----------------------------------------------------------------------
c
c ****** This routine was obtained from Yung Mok.
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: tempk
      real(r_typ) :: qrad_athay
c
c-----------------------------------------------------------------------
c
c ****** Parameters for Athay's radiative loss law.
c
      real(r_typ), parameter :: q0=1.e-22_r_typ
      real(r_typ), dimension(4) :: c=(/0.4,4.,4.5,2./)
      real(r_typ), dimension(4) :: d=(/-30.,-20.,-16.,-4./)
      real(r_typ), dimension(4) :: e=(/4.6,4.9,5.35,6.1/)
      save :: c,d,e
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: q
      real(r_typ) :: t10
c
c-----------------------------------------------------------------------
c
      t10=log10(tempk)
c
      q=q0*( c(1)*exp(d(1)*(t10-e(1))**2)
     &      +c(2)*exp(d(2)*(t10-e(2))**2)
     &      +c(3)*exp(d(3)*(t10-e(3))**2)
     &      +c(4)*exp(d(4)*(t10-e(4))**2))
c
      qrad_athay=q
c
      return
      end
c#######################################################################
      function qrad_rosner (tempk)
c
c-----------------------------------------------------------------------
c
c ****** Rosner radiative loss function in [erg-cm**3/s].
c
c ****** TEMPK is the temperature in degrees Kelvin.
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: tempk
      real(r_typ) :: qrad_rosner
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: q
      real(r_typ) :: t,qalpha,qchi
c
c-----------------------------------------------------------------------
c
      t=tempk
c
      if (tempk.lt.2.00e4) then
        qalpha=6.15
        qchi=1.408e-30
        t=t*1.e-3
      else if (tempk.ge.2.00e4.and.tempk.lt.3.76e4) then
        qalpha=0.
        qchi=1.412e-22
      else if (tempk.ge.3.76e4.and.tempk.lt.7.94e4) then
        qalpha=2.
        qchi=9.988e-32
      else if (tempk.ge.7.94e4.and.tempk.lt.2.51e5) then
        qalpha=0.
        qchi=6.297e-22
      else if (tempk.ge.2.51e5.and.tempk.lt.5.91e5) then
        qalpha=-2.
        qchi=3.967e-11
      else if (tempk.ge.5.91e5.and.tempk.lt.2.08e6) then
        qalpha=0.
        qchi=1.136e-22
      else if (tempk.ge.2.08e6) then
        qalpha=-0.67
        qchi=1.943e-18
      end if
c
      q=qchi*t**qalpha
c
      qrad_rosner=q
c
      return
      end
c#######################################################################
      function qrad_rtv (tempk)
c
c-----------------------------------------------------------------------
c
c ****** RTV radiative loss function in [erg-cm**3/s].
c
c ****** TEMPK is the temperature in degrees Kelvin.
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: tempk
      real(r_typ) :: qrad_rtv
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: t1=1.2882496e5_r_typ
      real(r_typ), parameter :: c1=2.7360324e-29_r_typ
      real(r_typ), parameter :: c2=0.1548817e-18_r_typ
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: q
      real(r_typ) :: sqrttemp
c
c-----------------------------------------------------------------------
c
      sqrttemp=sqrt(tempk)
c
      if (tempk.lt.t1) then
        q=c1*tempk*tempk/sqrttemp
      else
        q=c2/sqrttemp
      end if
c
      qrad_rtv=q
c
      return
      end
c#######################################################################
      function qrad_chianti_corona (tempk)
c
c-----------------------------------------------------------------------
c
c ****** Radiative loss function from CHIANTI with coronal
c ****** abundances.
c
c ****** TEMPK is the temperature in [K].
c ****** Q is returned in [erg-cm**3/s].
c
c-----------------------------------------------------------------------
c
      use number_types
      use chianti_rad_loss_corona
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: tempk
      real(r_typ) :: qrad_chianti_corona
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: ten=10._r_typ
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: q
      integer :: i
      real(r_typ) :: log10_t,log10_q,i_cont,alpha
c
c-----------------------------------------------------------------------
c
c ****** Interpolate log10(T) linearly from the uniform-increment
c ****** table LOG10_Q_TABLE.
c
      log10_t=log10(tempk)
c
      i_cont=one+(log10_t-log10_t_min)*log10_dt_inv
      i=i_cont
c
      if (i.lt.1) then
        log10_q=log10_q_table(1)
      else if (i.ge.n_elem) then
        log10_q=log10_q_table(n_elem)
      else
        alpha=i_cont-i
        log10_q=(one-alpha)*log10_q_table(i)+alpha*log10_q_table(i+1)
      end if
c
      q=ten**log10_q
c
      qrad_chianti_corona=q
c
      return
      end
c#######################################################################
      function qrad_chianti_photo (tempk)
c
c-----------------------------------------------------------------------
c
c ****** Radiative loss function from CHIANTI with photospheric
c ****** abundances.
c
c ****** TEMPK is the temperature in [K].
c ****** Q is returned in [erg-cm**3/s].
c
c-----------------------------------------------------------------------
c
      use number_types
      use chianti_rad_loss_photo
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: tempk
      real(r_typ) :: qrad_chianti_photo
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: ten=10._r_typ
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: q
      integer :: i
      real(r_typ) :: log10_t,log10_q,i_cont,alpha
c
c-----------------------------------------------------------------------
c
c ****** Interpolate log10(T) linearly from the uniform-increment
c ****** table LOG10_Q_TABLE.
c
      log10_t=log10(tempk)
c
      i_cont=one+(log10_t-log10_t_min)*log10_dt_inv
      i=i_cont
c
      if (i.lt.1) then
        log10_q=log10_q_table(1)
      else if (i.ge.n_elem) then
        log10_q=log10_q_table(n_elem)
      else
        alpha=i_cont-i
        log10_q=(one-alpha)*log10_q_table(i)+alpha*log10_q_table(i+1)
      end if
c
      q=ten**log10_q
c
      qrad_chianti_photo=q
c
      return
      end
c#######################################################################
      function radint (tempk)
c
c-----------------------------------------------------------------------
c
c ****** Select the radiation integral, based on the value of the
c ****** index RAD_LAW_INDEX.
c
c ****** Note that not all the radiative loss laws have a
c ****** corresponding radiation integral.  For those that are
c ****** missing, we use the Rosner radiation integral.
c
c-----------------------------------------------------------------------
c
      use number_types
      use radiative_loss_law
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: tempk
      real(r_typ) :: radint
c
c-----------------------------------------------------------------------
c
      real(r_typ), external :: radint_rosner
      real(r_typ), external :: radint_athay
c
c-----------------------------------------------------------------------
c
      select case (rad_law_index)
      case (RAD_LAW_ATHAY)
        radint=radint_athay(tempk)
      case default
        radint=radint_rosner(tempk)
      end select
c
      return
      end
c#######################################################################
      function radint_rosner (tempk)
c
c-----------------------------------------------------------------------
c
c ****** Get the radiation integral int(Q(T)*T**0.5)dT from the
c ****** bottom of the chromosphere (assumed to be at T = 6000K)
c ****** through the transition region up to a temperature
c ****** TEMPK (in degrees Kelvin) by piecewise integration of
c ****** the Rosner et al. (1978) radiative loss curve.
c
c ****** The integral is returned in units of [erg-cm**3-K**1.5/s].
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: tempk
      real(r_typ) :: radint_rosner
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: t
c
c-----------------------------------------------------------------------
c
      t=tempk
c
      if (tempk.le.6.e3) then
        write (*,*)
        write (*,*) '### ERROR in RADINT_ROSNER:'
        write (*,*) '### Illegal temperature specified.'
        write (*,*) 'T = ',tempk
        call exit (1)
      else if (tempk.gt.6.00e3.and.tempk.lt.2.00e4) then
        t=t*1.e-3
        radint_rosner=          5.821e-27*(t**(+7.65)-8.971e+05)
      else if (tempk.ge.2.00e4.and.tempk.lt.3.76e4) then
        radint_rosner=5.222e-17+9.413e-23*(t**(+1.50)-2.828e+06)
      else if (tempk.ge.3.76e4.and.tempk.lt.7.94e4) then
        radint_rosner=4.723e-16+2.854e-32*(t**(+3.50)-1.031e+16)
      else if (tempk.ge.7.94e4.and.tempk.lt.2.51e5) then
        radint_rosner=4.203e-15+4.198e-22*(t**(+1.50)-2.237e+07)
      else if (tempk.ge.2.51e5.and.tempk.lt.5.91e5) then
        radint_rosner=4.760e-14-7.934e-11*(t**(-0.50)-1.996e-03)
      else if (tempk.ge.5.91e5.and.tempk.lt.2.08e6) then
        radint_rosner=1.028e-13+7.573e-23*(t**(+1.50)-4.543e+08)
      else if (tempk.ge.2.08e6) then
        radint_rosner=2.956e-13+2.341e-18*(t**(+0.83)-1.754e+05)
      end if
c
      return
      end
c#######################################################################
      function radint_athay (tempk)
c
c-----------------------------------------------------------------------
c
c ****** Get the radiation integral int(Q(T)*T**0.5)dT
c ****** for Athay's radiative loss law.
c
c ****** The integral is returned in units of [erg-cm**3-K**1.5/s].
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: tempk
      real(r_typ) :: radint_athay
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: t,alpha
      integer :: i,ip1
c
c-----------------------------------------------------------------------
c
      integer, parameter :: n_temp=21
c
      real(r_typ), dimension(n_temp) :: temp_array
      real(r_typ), dimension(n_temp) :: radint_array
c
      data temp_array/20000.,100000.,200000.,300000.,400000.,500000.,
     & 600000.,700000.,800000.,900000.,1.e6,1.1e6,1.2e6,1.3e6,
     & 1.4e6,1.5e6,1.6e6,1.7e6,1.8e6,1.9e6,2e6/
c
      data radint_array/0.,.0554986e-13,
     & 0.2073482e-13,0.4322839e-13,0.6130756e-13,0.7444938e-13,
     & 0.8629608e-13,0.9910115e-13,1.1372597e-13,1.3032514e-13,
     & 1.4875648e-13,1.6876998e-13,1.9008915e-13,2.1244536e-13,
     & 2.3559178e-13,2.5930832e-13,2.8340229e-13,3.0770726e-13,
     & 3.3208089e-13,3.5640262e-13,3.8057114e-13/
c
c-----------------------------------------------------------------------
c
      t=tempk
c
      if (tempk.le.temp_array(     1).or.
     &    tempk.ge.temp_array(n_temp)) then
        write (*,*)
        write (*,*) '### ERROR in RADINT_ATHAY:'
        write (*,*) '### Illegal temperature specified.'
        write (*,*) 'T = ',tempk
        call exit (1)
      end if
c
      call get_interp_factors (n_temp,temp_array,tempk,i,alpha)
      ip1=min0(i+1,n_temp)
c
      radint_athay= (one-alpha)*radint_array(i  )
     &             +     alpha *radint_array(ip1)
c
      return
      end
c#######################################################################
      subroutine get_kappa (t)
c
c-----------------------------------------------------------------------
c
c ****** Calculate the thermal conductivity in [erg/s/cm/K]
c ****** evaluated at the temperature T (in normalized units).
c
c ****** The conductivity is stored in the array KAPPA in
c ****** the FIELDS module.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use fields
      use heating_parameters
      use normalization_parameters
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), dimension(ns) :: t
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: power=2.5_r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: tk,bf
c
c-----------------------------------------------------------------------
c
      real(r_typ), external :: boost
c
c-----------------------------------------------------------------------
c
c ****** Set kappa to the Spitzer value, multiplied by TCOND,
c ****** and boosted by the "boost factor".
c
      do i=1,ns
        tk=fn_t*abs(t(i))
        bf=boost(tk)
        kappa(i)=bf*tcond*kappa0_spitzer*tk**power
      enddo
c
      return
      end
c#######################################################################
      subroutine advance_ne
c
c-----------------------------------------------------------------------
c
c ****** Advance the electron density using the continuity equation.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use params
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: two=2._r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: wavefac
c
      real(r_typ), dimension(ns) :: ne_p
      real(r_typ), dimension(ns) :: div
c
c-----------------------------------------------------------------------
c
c ****** Predictor step.
c
      ne_p=ne
c
      if (use_predictor) then
        if (use_flow_in_predictor) then
          call divfv (ne_p,v,div)
          do i=2,nsm1
            ne_p(i)=ne_p(i)-dt*div(i)
          enddo
            wavefac=one
        else
            wavefac=0.
        endif
        do i=2,nsm1
          div(i)=(betapc_t-wavefac)*( area(i  )*v(i  )
     &                               -area(i-1)*v(i-1))*ne(i)
     &                              /(areah(i)*dsh(i))
        enddo
        do i=2,nsm1
         ne_p(i)=ne_p(i)-div(i)*dt
        enddo
      endif
c
c ****** Set boundary conditions.
c
      call set_bc_ne (ne_p)
c
c ****** Corrector step.
c
      call divfv (ne_p,v,div)
c
      do i=2,nsm1
        ne(i)=ne(i)-div(i)*dt
      enddo
c
c ****** Set boundary conditions.
c
      call set_bc_ne (ne)
c
c ****** Check that the electron density is non-negative.
c
      do i=1,ns
        if (ne(i).le.0.) then
          write (*,*)
          write (*,*) '### ERROR in ADVANCE_NE:'
          write (*,*) '### The electron density is non-positive.'
          write (*,*) 'ITIME = ',itime
          write (*,*) 'TIME = ',time
          write (*,*) 'I = ',i
          write (*,*) 'SH(I) = ',sh(i)
          write (*,*) 'NE(I) = ',ne(i)
          ifabort=.true.
          call final_diags
        end if
      enddo
c
c ****** Set the mass density from the electron number density.
c
      call ne2rho
c
c ****** Calculate the total mass in the loop.
c
      call total_mass
c
      return
      end
c#######################################################################
      subroutine total_mass
c
c-----------------------------------------------------------------------
c
c ****** Calculate the total mass in the loop.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use params
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      integer :: i
c
c-----------------------------------------------------------------------
c
      tmass=0.
      do i=2,nsm1
        tmass=tmass+areah(i)*dsh(i)*rho(i)
      enddo
c
      return
      end
c#######################################################################
      subroutine advance_v
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use viscosity_profile
      use params
      use boundary_conditions
      use si_term_velocities
      use momentum_diagnostics
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: rhoav,sip,sim,sp0,sm0,dv
c
      real(r_typ), dimension(nsm1) :: vp
      real(r_typ), dimension(nsm1) :: aa,bb,cc
c
c-----------------------------------------------------------------------
c
c ****** Advance the velocity using the momentum equation.
c
      v_previous_step=v
c
c ****** Set the boundary velocity at the left leg.
c
      select case (ibc_v_0)
      case (IBC_ZERO)
        vbc0=0.
      case (IBC_CHARBC)
        call getbc0 (vbc0)
      end select
c
c ****** Set the boundary velocity at the right leg.
c
      select case (ibc_v_1)
      case (IBC_ZERO)
        vbc1=0.
      case (IBC_CHARBC)
        call getbc1 (vbc1)
      end select
c
c ****** Predictor step.
c
      if (use_predictor) then
        if (use_flow_in_predictor) then
           call get_v_dot_grad_v (v,v,vdgv)
        else
          vdgv=0.
        endif
        call advance_si (betapc_v,vbc0,vbc1,vdgv,vp,v_si_old1)
        v_si_old1=vp
      else
        vp=v
      endif
c
c ****** Corrector step.
c
      call get_v_dot_grad_v (v,vp,vdgv)
c
      call advance_si (one,vbc0,vbc1,vdgv,v,v_si_old2)
      v_si_old2=v
c
c ****** Add the implicit viscous advance (if the viscosity
c ****** advance has been split off).
c
      if (use_viscosity.and.split_visc_advance) then
c
        do i=2,nsm2
          rhoav=half*(rho(i)+rho(i+1))
          sip=half*(visc(i)+visc(i+1))*dt
          sim=half*(visc(i)+visc(i-1))*dt
          sp0=rho(i+1)*sip
          sm0=rho(i  )*sim
          dv=ds(i)
          aa(i)=rhoav*dv+( sp0/dsh(i+1)
     &                    +sm0/dsh(i  ))
          bb(i)=-sp0/dsh(i+1)
          cc(i)=-sm0/dsh(i  )
          v(i)=v(i)*rhoav*dv
        enddo
c
c ****** Set boundary conditions.
c
        v(1)=vbc0
        aa(1)=one
        bb(1)=0.
        v(nsm1)=vbc1
        aa(nsm1)=one
        cc(nsm1)=0.
c
c ****** Solve the tridiagonal system.
c
        call trid (nsm1,cc,aa,bb,v)
c
      end if
c
      return
      end
c#######################################################################
      subroutine getbc0 (ub)
c
c-----------------------------------------------------------------------
c
c ****** Get the left boundary velocity using the characteristics.
c
c ****** Return the left boundary velocity in UB.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use fields
      use mesh
      use normalization_parameters
      use params
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: ub,ps,rhos,tav,cs,f_area,duds,dpds,cfl1,
     &               f_si_mod,fac,dudt
c
c-----------------------------------------------------------------------
c
c ****** This formulation assumes that the pressure
c ****** at the boundary is specified (and fixed in time).
c
c ****** Initialize the variables.
c
      ub=v(1)
      ps=half*(p(1)+p(2))
      rhos=half*(rho(1)+rho(2))
      tav=half*(temp(1)+temp(2))
      cs=sqrt((gamma*he_p*tav/he_rho))
      f_area=(areah(2)-areah(1))/(area(1)*ds(1))
c
c ****** Get derivatives for projecting along the characteristic.
c
      duds=(v(2)-v(1))/dsh(2)
      dpds=(p(2)-p(1))/ds(1)
c
c ****** Calculate the modification in the characteristic
c ****** BCs due to the semi-implicit term.
c
      cfl1=cs*dt/ds(1)
      f_si_mod=sqrt(one+cfl1**2)
c
      fac=(ub-cs/f_si_mod)
c
      dudt= fac*dpds/(rhos*cs*f_si_mod)
     &       -fac*duds
     &       -g(1)/(f_si_mod**2)
     &       +ub*cs*f_area/(f_si_mod)
c
      ub=ub+dudt*dt
c
      if (ub.gt.cs) then
        ub=cs
        write (*,*)
        write (*,*) '### ERROR in GETBC0:'
        write (*,*) '### Supersonic inflow at s=S0.'
        write (*,*) 'ITIME  = ',itime
        write (*,*) 'TIME  = ',time
        write (*,*) 'CS = ',cs
        write (*,*) 'UB = ',ub
        ifabort=.true.
        call final_diags
      end if
c
      if (idebug.gt.0) then
        write (*,*)
        write (*,*) '### COMMENT from GETBC0:'
        write (*,*) 'TIME = ',time
        write (*,*) 'PS = ',ps,' RHOS = ',rhos
        write (*,*) 'UB = ',ub,' CS = ',cs
      end if
c
      return
      end
c#######################################################################
      subroutine getbc1 (ub)
c
c-----------------------------------------------------------------------
c
c ****** Get the right boundary velocity using the characteristics.
c
c ****** Return the right boundary velocity in UB.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use fields
      use mesh
      use normalization_parameters
      use params
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: ub,ps,rhos,tav,cs,f_area,duds,dpds,cfl1,
     &               f_si_mod,fac,dudt
c
c-----------------------------------------------------------------------
c
c ****** This formulation assumes that the pressure
c ****** at the boundary is specified (and fixed in time).
c
c ****** Initialize the variables.
c
      ub=v(nsm1)
      ps=half*(p(ns)+p(nsm1))
      rhos=half*(rho(ns)+rho(nsm1))
      tav=half*(temp(ns)+temp(nsm1))
      cs=sqrt((gamma*he_p*tav/he_rho))
      f_area=(areah(ns)-areah(nsm1))/(area(nsm1)*ds(nsm1))
c
c ****** Get derivatives for projecting along the characteristic.
c
      duds=(v(nsm1)-v(nsm2))/dsh(nsm2)
      dpds=(p(ns)-p(nsm1))/ds(nsm1)
c
c ****** Calculate the modification in the characteristic
c ****** BCs due to the semi-implicit term.
c
      cfl1=cs*dt/ds(nsm1)
      f_si_mod=sqrt(one+cfl1**2)
c
      fac=(ub+cs/f_si_mod)
c
      dudt=-fac*dpds/(rhos*cs*f_si_mod)
     &       -fac*duds
     &       -g(nsm1)/(f_si_mod**2)
     &       -ub*cs*f_area/(f_si_mod)
c
      ub=ub+dudt*dt
c
      if (ub.lt.-cs) then
        ub=-cs
        write (*,*)
        write (*,*) '### ERROR in GETBC1:'
        write (*,*) '### Supersonic inflow at s=S1.'
        write (*,*) 'ITIME  = ',itime
        write (*,*) 'TIME  = ',time
        write (*,*) 'CS = ',cs
        write (*,*) 'UB = ',ub
        ifabort=.true.
        call final_diags
      end if
c
      if (idebug.gt.0) then
        write (*,*)
        write (*,*) '### COMMENT from GETBC1:'
        write (*,*) 'TIME = ',time
        write (*,*) 'PS = ',ps,' RHOS = ',rhos
        write (*,*) 'UB = ',ub,' CS = ',cs
      end if
c
      return
      end
c#######################################################################
      subroutine get_v_dot_grad_v (v1,v2,vdgv)
c
c-----------------------------------------------------------------------
c
c ****** Calculate v1-dot-grad-v2 on the internal points of
c ****** the main mesh.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use params
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), dimension(nsm1) :: v1
      real(r_typ), dimension(nsm1) :: v2
      real(r_typ), dimension(nsm1) :: vdgv
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: cadv,dvdsm,dvdsp
c
c-----------------------------------------------------------------------
c
c ****** Compute v*dv/ds.
c
      if (upwind.gt.0.) then
c
c ****** Upwind differences.
c
        do i=2,nsm2
          cadv=sign(upwind,v1(i))
          dvdsp=(v2(i+1)-v2(i  ))/dsh(i+1)
          dvdsm=(v2(i  )-v2(i-1))/dsh(i  )
          vdgv(i)=v1(i)*half*( (one-cadv)*dvdsp
     &                        +(one+cadv)*dvdsm)
        enddo
c
      else
c
c ****** Centered differences.
c
        do i=2,nsm2
          vdgv(i)=v1(i)*(v2(i+1)-v2(i-1))/(dsh(i+1)+dsh(i))
        enddo
c
      end if
c
c ****** Set boundary points, which are not used, to zero.
c
      vdgv(1)=0.
      vdgv(nsm1)=0.
c
      return
      end
c#######################################################################
      subroutine advance_si (alpha,vbc0,vbc1,adv,vp,v_old)
c
c-----------------------------------------------------------------------
c
c ****** Predictor-corrector for the velocity (including the
c ****** semi-implicit term).
c
c ****** ALPHA is the fraction of the force terms to use
c ****** in the time advance.
c
c ****** On output, VP has the predicted velocity.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use viscosity_profile
      use params
      use momentum_diagnostics
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: alpha
      real(r_typ) :: vbc0
      real(r_typ) :: vbc1
      real(r_typ), dimension(nsm1) :: adv
      real(r_typ), dimension(nsm1) :: vp
      real(r_typ), dimension(nsm1) :: v_old
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: rhoav,sip,sim,sp0,sm0,dv
c
      real(r_typ), dimension(nsm1) :: aa,bb,cc
c
c-----------------------------------------------------------------------
c
c ****** Construct the semi-implicit term.
c
      do i=2,nsm2
        rhoav=half*(rho(i)+rho(i+1))
        sip=half*(sifac(i)+sifac(i+1))*dt
        sim=half*(sifac(i)+sifac(i-1))*dt
        sp0=rho(i+1)*sip
        sm0=rho(i  )*sim
        dv=ds(i)
        aa(i)=rhoav*dv+( sp0/dsh(i+1)
     &                  +sm0/dsh(i  ))
        bb(i)=-sp0/dsh(i+1)
        cc(i)=-sm0/dsh(i  )
        delsq(i)=( sp0*(v_old(i+1)-v_old(i  ))/dsh(i+1)
     &            -sm0*(v_old(i  )-v_old(i-1))/dsh(i  ))
     &           /ds(i)
      enddo
c
c ****** Construct the right-hand-side.
c
      do i=2,nsm2
        rhoav=half*(rho(i)+rho(i+1))
        dv=ds(i)
        vp(i)=dv*( rhoav*(v(i)-adv(i)*dt)
     &            -alpha*dt*((p(i+1)-p(i))/ds(i))
     &            -alpha*dt*(g(i)*rhoav)
     &            -delsq(i))
      enddo
c
c ****** Add the implicit viscous advance (if the viscosity
c ****** advance has not been split off).
c
      if (use_viscosity) then
        if (.not.split_visc_advance) then
c
          do i=2,nsm2
            sip=half*(visc(i)+visc(i+1))*dt
            sim=half*(visc(i)+visc(i-1))*dt
            sp0=rho(i+1)*sip
            sm0=rho(i  )*sim
            aa(i)=aa(i)+( sp0/dsh(i+1)
     &                   +sm0/dsh(i  ))
            bb(i)=bb(i)-sp0/dsh(i+1)
            cc(i)=cc(i)-sm0/dsh(i  )
          enddo
c
        end if
      end if
c
c ****** Set boundary conditions.
c
      vp(1)=vbc0
      aa(1)=one
      bb(1)=0.
      vp(nsm1)=vbc1
      aa(nsm1)=one
      cc(nsm1)=0.
c
c ****** Solve the tridiagonal system.
c
      call trid (nsm1,cc,aa,bb,vp)
c
      return
      end
c#######################################################################
      subroutine get_energy_terms
c
c-----------------------------------------------------------------------
c
c ****** Calculate the terms in the energy equation.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use normalization_parameters
      use heating_parameters
      use params
      use energy_diagnostics
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: kappa_av,dtds
      real(r_typ) :: divpv,divv
      real(r_typ) :: p_m,p_p
c
c-----------------------------------------------------------------------
c
      real(r_typ), external :: qrad
c
c-----------------------------------------------------------------------
c
c ****** Update the thermal conductivity.
c
      call get_kappa (temp)
c
c ****** Calculate the conductive heat flux.
c
      do i=1,nsm1
        kappa_av=half*(kappa(i)+kappa(i+1))
        dtds=fn_t*(temp(i+1)-temp(i))/(ds(i)*fnorml)
        h_q_tc(i)=-kappa_av*dtds
      enddo
c
c ****** Calculate the divergence of the conductive heat flux.
c
      do i=2,nsm1
        h_qc(i)=-( area(i  )*h_q_tc(i  )
     &            -area(i-1)*h_q_tc(i-1)
     &           )/(areah(i)*dsh(i)*fnorml)
      enddo
      h_qc( 1)=h_qc(   2)
      h_qc(ns)=h_qc(nsm1)
c
c ****** Coronal heating.
c
      h_h=fn_heat*heating
c
c ****** Radiative loss.
c
      do i=1,ns
        h_qr(i)=-radloss*he_np*(ne(i)*fn_n)**2*qrad(fn_t*temp(i))
      enddo
c
c ****** Enthalpy flux.
c
      do i=2,nsm1
        p_m=half*(p(i)+p(i-1))
        p_p=half*(p(i)+p(i+1))
        divpv=( area(i  )*p_p*v(i  )
     &         -area(i-1)*p_m*v(i-1))
     &        /(areah(i)*dsh(i))
        divv=(area(i  )*v(i  )-
     &        area(i-1)*v(i-1))/
     &        (areah(i)*dsh(i))
        h_pv(i)=-fn_p*(p(i)*divv+divpv/(gamma-one))/fnormt
      enddo
      h_pv( 1)=h_pv(   2)
      h_pv(ns)=h_pv(nsm1)
c
      return
      end
c#######################################################################
      subroutine momentum_diagnostic
c
c-----------------------------------------------------------------------
c
c ****** Calculate the momentum diagnostic.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use viscosity_profile
      use params
      use file_names
      use momentum_diagnostics
      use si_term_velocities
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i,ierr
      real(r_typ) :: rhoav,sip,sim,sp0,sm0,dv,gradp_term,dvdt_term
      real(r_typ), dimension(nsm1) :: visc_term
c
c-----------------------------------------------------------------------
c
      if (momentum_diagnostic_file.eq.' ') return
c
c ****** Write the momentum terms to MOMENTUM_DIAGNOSTIC_FILE.
c
      do i=2,nsm2
        rhoav=half*(rho(i)+rho(i+1))
        sip=half*(sifac(i)+sifac(i+1))
        sim=half*(sifac(i)+sifac(i-1))
        sp0=rho(i+1)*sip
        sm0=rho(i  )*sim
        dv=ds(i)
        delsq(i)=(( sp0*(v_si_old2(i+1)-v_si_old2(i  ))/dsh(i+1)
     &             -sm0*(v_si_old2(i  )-v_si_old2(i-1))/dsh(i  ))
     &           /ds(i)-delsq(i)/dt)/rhoav
      enddo
      if (use_viscosity) then
        do i=2,nsm2
          rhoav=half*(rho(i)+rho(i+1))
          sip=half*(visc(i)+visc(i+1))
          sim=half*(visc(i)+visc(i-1))
          sp0=rho(i+1)*sip
          sm0=rho(i  )*sim
          visc_term(i)=( sp0*(v(i+1)-v(i  ))/dsh(i+1)
     &                  -sm0*(v(i  )-v(i-1))/dsh(i  )
     &                 )/(ds(i)*rhoav)
        enddo
      else
        visc_term=0.
      endif
c
      call ffopen (1,momentum_diagnostic_file,'rw',ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in MOMENTUM_DIAGNOSTIC:'
        write (*,*) '### Could not open the momentum diagnostic'//
     &              ' output file:'
        write (*,*) 'File name: ',trim(momentum_diagnostic_file)
        call exit (1)
      end if
c
      write (1,900) 's'         ,char(9),
     &              '-vdgv'     ,char(9),
     &              '-gradp/rho',char(9),
     &              '-gravity'  ,char(9),
     &              'viscosity' ,char(9),
     &              'SI'        ,char(9),
     &              '-dvdt'
      do i=2,nsm2
        rhoav=half*(rho(i)+rho(i+1))
        gradp_term=(p(i)-p(i+1))/(ds(i)*rhoav)
        dvdt_term=(v_previous_step(i)-v(i))/dt
        write (1,910) s(i)        ,char(9),
     &                -vdgv(i)    ,char(9),
     &                gradp_term  ,char(9),
     &                -g(i),char(9),
     &                visc_term(i),char(9),
     &                delsq(i)    ,char(9),
     &                dvdt_term
      enddo
c
      close (1)
c
  900 format (10(a,a))
  910 format (10(1pe21.14,a))
c
      return
      end
c#######################################################################
      subroutine energy_flow_diagnostic
c
c-----------------------------------------------------------------------
c
c ****** Calculate global energy flow diagnostics.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use normalization_parameters
      use heating_parameters
      use params
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: s_0,rho_0,p_0,v_0,t_0,area_0,fm_0,ke_0,te_0,qtc_0
      real(r_typ) :: s_1,rho_1,p_1,v_1,t_1,area_1,fm_1,ke_1,te_1,qtc_1
      real(r_typ) :: dv,hvol,kappa_av,dtds,rvol,wgrav,de
c
      real(r_typ), dimension(nsm1) :: qc
c
c-----------------------------------------------------------------------
c
      real(r_typ), external :: qrad
c
c-----------------------------------------------------------------------
c
c ****** Variables at S0.
c
      s_0=s(1)
      rho_0=half*(rho(1)+rho(2))
      p_0=half*(p(1)+p(2))
      v_0=v(1)
      t_0=half*(temp(1)+temp(2))
      area_0=area(1)
c
c ****** Variables at S1.
c
      s_1=s(nsm1)
      rho_1=half*(rho(ns)+rho(nsm1))
      p_1=half*(p(ns)+p(nsm1))
      v_1=v(nsm1)
      t_1=half*(temp(ns)+temp(nsm1))
      area_1=area(nsm1)
c
c ****** Thermal conduction heat flux.
c
      call get_kappa (temp)
c
      do i=1,nsm1
        kappa_av=half*(kappa(i)+kappa(i+1))/fn_kappa
        dtds=(temp(i+1)-temp(i))/ds(i)
        qc(i)=-kappa_av*dtds
      enddo
c
c ****** Total coronal heating.
c
      hvol=0.
      do i=2,nsm1
        dv=dsh(i)*areah(i)
        hvol=hvol+heating(i)*dv
      enddo
c
c ****** Total radiation lost.
c
      rvol=0.
      do i=2,nsm1
        dv=dsh(i)*areah(i)
        rvol=rvol+he_np*radloss*ne(i)**2
     &                 *(qrad(fn_t*temp(i))/fn_qrad)*dv
      enddo
c
c ****** Mass flux.
c
      fm_0=rho_0*v_0*area_0
      fm_1=rho_1*v_1*area_1
c
c ****** Total work done against gravity.
c
      wgrav=rho_0*v_0*area_0*phi0-rho_1*v_1*area_1*phi1
c
c ****** Kinetic energy flow.
c
      ke_0=half*rho_0*v_0**3*area_0
      ke_1=half*rho_1*v_1**3*area_1
c
c ****** Enthalpy flow.
c
      te_0=gamma*p_0*v_0*area_0/(gamma-one)
      te_1=gamma*p_1*v_1*area_1/(gamma-one)
c
c ****** Thermal conduction heat flux.
c
      qtc_0=-qc(   1)*area_0
      qtc_1= qc(nsm1)*area_1
c
      write (*,*)
      write (*,*) '### Final solution:'
      write (*,*)
      write (*,*) 'Mass density at S0                  = ',rho_0
      write (*,*) 'Pressure at S0                      = ',p_0
      write (*,*) 'Velocity at S0                      = ',v_0
      write (*,*) 'Temperature at S0                   = ',t_0
      write (*,*)
      write (*,*) 'Mass density at S1                  = ',rho_1
      write (*,*) 'Pressure at S1                      = ',p_1
      write (*,*) 'Velocity at S1                      = ',v_1
      write (*,*) 'Temperature at S1                   = ',t_1
c
      write (*,*)
      write (*,*) '### Mass flow diagnostic:'
      write (*,*)
      write (*,*) 'Area at S0                          = ',area_0
      write (*,*) 'Area at S1                          = ',area_1
      write (*,*)
      write (*,*) 'Mass flux at S0                     = ',fm_0
      write (*,*) 'Mass flux at S1                     = ',fm_1
c
      write (*,*)
      write (*,*) '### Energy flow diagnostic:'
      write (*,*)
      write (*,*) 'Total heating rate                  = ',hvol
      write (*,*) 'Total radiative loss rate           = ',rvol
      write (*,*)
      write (*,*) 'Rate of work done against gravity   = ',wgrav
      write (*,*)
      write (*,*) 'Thermal conduction loss rate at S0  = ',qtc_0
      write (*,*) 'Thermal conduction loss rate at S1  = ',qtc_1
      write (*,*)
      write (*,*) 'Rate of KE advected in  at S0       = ',ke_0
      write (*,*) 'Rate of KE advected out at S1       = ',ke_1
      write (*,*)
      write (*,*) 'Rate of enthalpy advected in  at S0 = ',te_0
      write (*,*) 'Rate of enthalpy advected out at S1 = ',te_1
c
c ****** Compute the energy error.
c
      de=hvol-rvol-wgrav-qtc_0-qtc_1+ke_0-ke_1+te_0-te_1
c
      write (*,*)
      write (*,*) '### Energy error:'
      write (*,*)
      write (*,*) ' energy error =   heating'
      write (*,*) '                - radiation'
      write (*,*) '                - work done against gravity'
      write (*,*) '                - thermal conduction loss at S0'
      write (*,*) '                - thermal conduction loss at S1'
      write (*,*) '                + KE advected in at S0'
      write (*,*) '                - KE advected out at S1'
      write (*,*) '                + enthalpy advected in at S0'
      write (*,*) '                - enthalpy advected out at S1'
      write (*,*)
      write (*,*) 'Error in rate of energy flow = ',de
c
      if (hvol.ne.0.) then
        write (*,*)
        write (*,*) '(Energy rate error)/(heating rate) = ',de/hvol
      end if
c
      return
      end
c#######################################################################
      subroutine trid (n,c,a,b,d)
c
c-----------------------------------------------------------------------
c
c ****** Solve the tridiagonal system of equations:
c
c         C(i)*X(i-1) + A(i)*X(i) + B(i)*X(i+1) = D(i)
c
c        for i=2,...,N-1, with
c
c           A(1)*X(1) + B(1)*X(2) = D(1)
c
c        and
c
c           C(N)*X(N-1) + A(N)*X(N) = D(N)
c
c ****** Note that C(1) and B(N) are not referenced.
c
c ****** The array A is overwritten during the call.
c
c ****** D is overwritten with the solution.
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      integer :: n
      real(r_typ), dimension(n) :: c,a,b,d
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: ace
c
c-----------------------------------------------------------------------
c
c ****** Forward elimination.
c
      d(1)=d(1)/a(1)
      a(1)=b(1)/a(1)
      do i=2,n
        ace=one/(a(i)-c(i)*a(i-1))
        if (i.ne.n) a(i)=ace*b(i)
        d(i)=ace*(d(i)-c(i)*d(i-1))
      enddo
c
c ****** Backward substitution.
c
      do i=n-1,1,-1
        d(i)=d(i)-a(i)*d(i+1)
      enddo
c
      return
      end
c#######################################################################
      function node_profile (check,p,x)
c
c-----------------------------------------------------------------------
c
c ****** Interpolate a function linearly.
c
c-----------------------------------------------------------------------
c
c ****** The function is defined by the profile P.  The function
c ****** value returned is the linear interpolant at X.
c
c ****** Note that if X.lt.P%X(1), the function value returned
c ****** is P%F(1), and if X.gt.P%X(P%N), the function value
c ****** returned is P%F(P%N).
c
c ****** Call once with CHECK=.true. to check that the profile
c ****** abscissa table is monotonically increasing.  In this mode
c ****** X is not accessed, and the function value is set to 0.
c
c-----------------------------------------------------------------------
c
      use number_types
      use profile_def
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      logical :: check
      type(prof):: p
      real(r_typ) :: x
      real(r_typ) :: node_profile
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: alpha
c
c-----------------------------------------------------------------------
c
c ****** If CHECK=.true., check the abscissa table.
c
      if (check) then
        do i=1,p%n-1
          if (p%x(i+1).le.p%x(i)) then
            write (*,*)
            write (*,*) '### ERROR in NODE_PROFILE:'
            write (*,*) '### The profile abscissa table is not'//
     &                  ' monotonically increasing:'
            write (*,*) 'Abscissa table:'
            write (*,*) p%x(1:p%n)
            call exit (1)
          end if
        enddo
        node_profile=0.
        return
      end if
c
      if (x.le.p%x(1)) then
        node_profile=p%f(1)
      else if (x.gt.p%x(p%n)) then
        node_profile=p%f(p%n)
      else
        do i=1,p%n-1
          if (x.ge.p%x(i).and.x.lt.p%x(i+1)) exit
        enddo
        alpha=(x-p%x(i))/(p%x(i+1)-p%x(i))
        node_profile=p%f(i)*(one-alpha)+p%f(i+1)*alpha
      end if
c
      return
      end
c#######################################################################
      subroutine load_heating
c
c-----------------------------------------------------------------------
c
c ****** Set the heating profile.
c
c ****** This routine is called at every time step.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use heating_parameters
      use params
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      integer :: ierr
      character(32) :: fname
      logical, save :: first_call=.true.
c
c-----------------------------------------------------------------------
c
c ****** On the first time in, set up the static heating component.
c
      if (first_call) then
c
        heating=0.
c
c ****** Add a uniform heating component.
c
        heating=heating+h0_uniform
c
c ****** Add a heating profile that decreases exponentially
c ****** away from each of the legs.
c
        call add_exponential_heating
c
c ****** Add the heating profile from a file.
c
        call add_heating_from_file
c
        heating( 1)=heating(   2)
        heating(ns)=heating(nsm1)
c
c ****** Save the static heating component.
c
        heating_static=heating
c
      end if
c
c ****** Add the time-dependent heating components.
c
      heating=heating_static
c
c ****** Add extended heating if it was requested.
c
      if (extended_heating_model) then
c
        call add_extended_heating
c
c ****** Write the extended heating model NAMELIST parameters
c ****** to an output file.
c
        if (first_call) then
c
          fname='heating_params.out'
c
          call ffopen (1,fname,'rw',ierr)
c
          if (ierr.ne.0) then
            write (*,*)
            write (*,*) '### ERROR in LOAD_HEATING:'
            write (*,*) '### Could not open the heating parameters'//
     &                  ' output file:'
            write (*,*) 'File name: ',trim(fname)
            call exit (1)
          end if
c
          write (1,heating_params)
c
          close (1)
c
        end if
c
      end if
c
      heating( 1)=heating(   2)
      heating(ns)=heating(nsm1)
c
c ****** Get the heating expressed as a heat flux as a diagnostic.
c
      call get_heating_as_a_flux (q0,q1)
c
      if (first_call) first_call=.false.
c
      return
      end
c#######################################################################
      subroutine get_heating_as_a_flux (q0,q1)
c
c-----------------------------------------------------------------------
c
c ****** Get the total volumetric coronal heating expressed as
c ****** a heat flux [erg/cm^2/s] at each end of the loop.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use normalization_parameters
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: q0,q1
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: q_total
c
c-----------------------------------------------------------------------
c
      q_total=0.
      do i=2,nsm1
        q_total=q_total+heating(i)*dsh(i)*areah(i)
      enddo
      q0=q_total*fn_q0/area(   1)
      q1=q_total*fn_q0/area(nsm1)
c
      return
      end
c#######################################################################
      subroutine add_exponential_heating
c
c-----------------------------------------------------------------------
c
c ****** Add a heating profile that decreases exponentially away
c ****** from each of the legs, with an optional offset.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use heating_parameters
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: sv
c
c-----------------------------------------------------------------------
c
c ****** Left leg.
c
      do i=2,nsm1
        sv=sh(i)-s0
        if (sv.le.h_s_offset_l) then
          heating(i)=heating(i)+h0_l_1+h0_l_2
        else
          heating(i)= heating(i)
     &               +h0_l_1*exp(-(sv-h_s_offset_l)/hlen_l_1)
     &               +h0_l_2*exp(-(sv-h_s_offset_l)/hlen_l_2)
        end if
      enddo
c
c ****** Right leg.
c
      do i=2,nsm1
        sv=s1-sh(i)
        if (sv.le.h_s_offset_r) then
          heating(i)=heating(i)+h0_r_1+h0_r_2
        else
          heating(i)= heating(i)
     &               +h0_r_1*exp(-(sv-h_s_offset_r)/hlen_r_1)
     &               +h0_r_2*exp(-(sv-h_s_offset_r)/hlen_r_2)
        end if
      enddo
c
      return
      end
c#######################################################################
      subroutine add_heating_from_file
c
c-----------------------------------------------------------------------
c
c ****** Add a heating profile from an input file.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use heating_parameters
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), dimension(:), allocatable :: h_from_file
c
c-----------------------------------------------------------------------
c
c ****** If HEATING_INPUT_FILE is not blank, add the heating
c ****** read in from that file.
c
      if (heating_input_file.ne.' ') then
c
        write (*,*)
        write (*,*) '### Reading heating file: ',
     &              trim(heating_input_file)
c
        allocate (h_from_file(ns))
        call read_and_interp_field_h (heating_input_file,
     &                                h_from_file)
        heating=heating+h_from_file
        deallocate (h_from_file)
c
        write (*,*)
        write (*,*) '### COMMENT from ADD_HEATING_FROM_FILE:'
        write (*,*) '### Added the heating from file: ',
     &              trim(heating_input_file)
c
      end if
c
      return
      end
c#######################################################################
      subroutine add_extended_heating
c
c-----------------------------------------------------------------------
c
c ****** Add the extended heating contribution.
c
c-----------------------------------------------------------------------
c
      use heating_parameters
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
c ****** Add the heating from the model requested.
c
      select case (heating_model_index)
      case (HEAT_MODEL_B_POWER)
        call add_heating_b_power
      case (HEAT_MODEL_IMPULSIVE)
        call add_heating_impulsive
      case (HEAT_MODEL_RAPPAZZO)
        call add_heating_rappazzo
      end select
c
      return
      end
c#######################################################################
      subroutine add_heating_b_power
c
c-----------------------------------------------------------------------
c
c ****** Add the heating from the model that defines heating that
c ****** is proportional to a power of B.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use fields
      use heating_parameters
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: half=.5_r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: bn,mask
c
c-----------------------------------------------------------------------
c
c ****** Add a heating rate proportional to B to a power,
c ****** with a B-dependent mask.
c
      do i=2,nsm1
        bn=(b(i)-h_bpower_mask_b0)/h_bpower_mask_db
        mask=half*(one+tanh(bn))
        heating(i)=heating(i)+mask*h_bpower_h0*b(i)**h_bpower_power
      enddo
c
      return
      end
c#######################################################################
      subroutine add_heating_impulsive
c
c-----------------------------------------------------------------------
c
c ****** Add an impulsive heating contribution.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use heating_parameters
      use params
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ) :: tv,sv
c
c-----------------------------------------------------------------------
c
      tv=(time-h_impulsive_t0)/h_impulsive_dt
c
      do i=2,nsm1
        sv=(sh(i)-h_impulsive_s0)/h_impulsive_ds
        heating(i)= heating(i)
     &             +h_impulsive_h0*exp(-tv**2)*exp(-sv**2)
      enddo
c
      return
      end
c#######################################################################
      subroutine add_heating_rappazzo
c
c-----------------------------------------------------------------------
c
c ****** Add the heating contribution from the Rappazzo-Velli
c ****** heating model.
c
c-----------------------------------------------------------------------
c
      use number_types
      use globals
      use mesh
      use fields
      use heating_parameters
      use params
      use normalization_parameters
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), parameter :: one=1._r_typ
      real(r_typ), parameter :: half=.5_r_typ
      real(r_typ), parameter :: four=4._r_typ
c
c-----------------------------------------------------------------------
c
      integer :: i
      real(r_typ), save :: b_power
      real(r_typ), save :: l_power
      real(r_typ), save :: rho_power
c
c-----------------------------------------------------------------------
c
      real(r_typ) :: arg,f,h_base
c
      logical, save :: first_call=.true.
      real(r_typ), dimension(:), allocatable, save :: l
c
c-----------------------------------------------------------------------
c
c ****** On the first time in, read the "loop length" profile,
c ****** if requested.
c
c ****** This is used primarily in the case when we are trying to
c ****** reproduce the results from a 3D run where, e.g., this
c ****** "length" is approximated by the local radius of curvature.
c
c ****** If H_RAPPAZZO_LOOP_LENGTH_FILE is not blank, read the
c ****** "loop length" profile from that file.  Otherwise, use the
c ****** actual loop length.
c
      if (first_call) then
c
        allocate (l(ns))
c
        if (h_rappazzo_loop_length_file.ne.' ') then
          write (*,*)
          write (*,*) '### Reading Rappazzo loop length file: ',
     &                trim(h_rappazzo_loop_length_file)
          call read_and_interp_field_h (h_rappazzo_loop_length_file,
     &                                  l)
        else
          l=sl
        end if
c
c ****** Set the exponents.  The alpha parameter for the
c ****** Rappazzo-Velli model is the one defined in the 2008 paper.
c ****** Note that the alpha used in the 2007 paper is different.
c ****** They are related by: alpha(2008) = alpha(2007) - 1.
c
        b_power=(2*h_rappazzo_alpha+3)/(h_rappazzo_alpha+2)
        rho_power=one/(2*h_rappazzo_alpha+4)
c
c ****** Either use the correct L power or use Yung's "wrong"
c ****** L power.  The interpretation of this still needs to be
c ****** finalized.
c
        if (h_rappazzo_use_wrong_l_power) then
          l_power=-(h_rappazzo_alpha+1)/(h_rappazzo_alpha+2)
        else
          l_power=-(2*h_rappazzo_alpha+3)/(h_rappazzo_alpha+2)
        end if
c
        first_call=.false.
c
      end if
c
      do i=2,nsm1
        heating(i)= heating(i)
     &             +h_rappazzo_h0*b(i)**b_power
     &                           *rho(i)**rho_power
     &                           *l(i)**l_power
      enddo
c
c ****** Add the effect of the z profile to the heating, if
c ****** requested.
c
c ****** This sets the heating to H_RAPPAZZO_Z_PROF_H0 in
c ****** [erg/cm^/s] at the loop footpoints.  The heating for
c ****** points in the loop with z smaller than
c ****** H_RAPPAZZO_Z_PROF_Z0 [solar radii] approaches
c ****** H_RAPPAZZO_Z_PROF_H0 smoothly over a distance
c ****** H_RAPPAZZO_Z_PROF_DZ [solar radii].
c
      if (h_rappazzo_use_z_profile) then
        h_base=h_rappazzo_z_prof_h0/fn_heat
        do i=2,nsm1
          arg=four*(zh(i)-h_rappazzo_z_prof_z0)
     &             /h_rappazzo_z_prof_dz
          f=half*(one+tanh(arg))
          heating(i)=(one-f)*h_base+f*heating(i)
        enddo
      end if
c
      return
      end
c#######################################################################
      subroutine assign_ptr_1d (from,to)
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), dimension(:), target :: from
      real(r_typ), dimension(:), pointer :: to
c
c-----------------------------------------------------------------------
c
      to=>from
c
      return
      end
c#######################################################################
      subroutine assign_ptr_3d (from,to)
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      real(r_typ), dimension(:,:,:), target :: from
      real(r_typ), dimension(:,:,:), pointer :: to
c
c-----------------------------------------------------------------------
c
      to=>from
c
      return
      end
c#######################################################################
      subroutine init_sds_pointer_status (s)
c
c-----------------------------------------------------------------------
c
c ****** Disassociate all the pointers in the SDS in structure S.
c
c-----------------------------------------------------------------------
c
c ****** This is useful when subsequently querying the association
c ****** status of these pointers (e.g., when deallocating storage).
c
c-----------------------------------------------------------------------
c
      use sds_def
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      type(sds) :: s
c
c-----------------------------------------------------------------------
c
      nullify (s%f)
c
      nullify (s%scales(1)%f)
      nullify (s%scales(2)%f)
      nullify (s%scales(3)%f)
c
      return
      end
c#######################################################################
      subroutine deallocate_sds (s)
c
c-----------------------------------------------------------------------
c
c ****** Deallocate the memory used by the SDS in structure S.
c
c-----------------------------------------------------------------------
c
      use sds_def
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      type(sds) :: s
c
c-----------------------------------------------------------------------
c
      if (associated(s%f)) deallocate (s%f)
c
      if (associated(s%scales(1)%f)) deallocate (s%scales(1)%f)
      if (associated(s%scales(2)%f)) deallocate (s%scales(2)%f)
      if (associated(s%scales(3)%f)) deallocate (s%scales(3)%f)
c
      return
      end
c#######################################################################
      subroutine wrtxt_2d (fname,scale,nx,ny,f,x,y,ierr)
c
c-----------------------------------------------------------------------
c
c ****** Write a 2D scientific data set to a text file.
c
c-----------------------------------------------------------------------
c
c ****** This routine calls routine WRTXT to write the file.
c
c-----------------------------------------------------------------------
c
      use number_types
      use sds_def
      use assign_ptr_1d_interface
      use assign_ptr_3d_interface
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      character(*) :: fname
      logical :: scale
      integer :: nx,ny
      real(r_typ), dimension(nx,ny,1) :: f
      real(r_typ), dimension(nx) :: x
      real(r_typ), dimension(ny) :: y
      integer :: ierr
      intent(in) :: fname,scale,nx,ny,f,x,y
      intent(out) :: ierr
c
c-----------------------------------------------------------------------
c
c ****** Declaration for the SDS structure.
c
      type(sds) :: s
c
c-----------------------------------------------------------------------
c
c ****** Set the structure components.
c
      s%ndim=2
      s%dims(1)=nx
      s%dims(2)=ny
      s%dims(3)=1
      s%scale=scale
      s%hdf32=.false.
      if (scale) then
        call assign_ptr_1d (x,s%scales(1)%f)
        call assign_ptr_1d (y,s%scales(2)%f)
      else
        nullify (s%scales(1)%f)
        nullify (s%scales(2)%f)
      end if
      nullify (s%scales(3)%f)
      call assign_ptr_3d (f,s%f)
c
c ****** Write the data set.
c
      call wrtxt (fname,s,ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in WRTXT_2D:'
        write (*,*) '### Could not write the 2D data set.'
        write (*,*) 'File name: ',trim(fname)
        return
      end if
c
      return
      end
c#######################################################################
      subroutine wrtxt (fname,s,ierr)
c
c-----------------------------------------------------------------------
c
c ****** Write a 1D, 2D, or 3D scientific data set to a text file.
c
c-----------------------------------------------------------------------
c
c ****** Input arguments:
c
c          FNAME   : [character(*)]
c                    Text data file name to write to.
c
c          S       : [structure of type SDS]
c                    A structure that holds the field, its
c                    dimensions, and the scales, with the
c                    components described below.
c
c ****** Output arguments:
c
c          IERR    : [integer]
c                    IERR=0 is returned if the data set was written
c                    successfully.  Otherwise, IERR is set to a
c                    nonzero value.
c
c ****** Components of structure S:
c
c          NDIM    : [integer]
c                    Number of dimensions in the data set.
c
c          DIMS    : [integer, dimension(3)]
c                    Number of points in the data set dimensions.
c                    Only DIMS(1 .. NDIM) are referenced.
c
c          SCALE   : [logical]
c                    Flag to indicate the presence of scales (axes)
c                    in the data set.  SCALE=.false. means that scales
c                    are not being supplied; SCALE=.true. means that
c                    scales are being supplied.
c
c          HDF32   : [logical]
c                    Flag that indicates the precision of the data.
c                    This flag is used to determine the format for data
c                    written to the text file.  When HDF32=.TRUE., the
c                    data is assumed to originate from a 32-bit HDF data
c                    file, and is written with 7 digits to the text file.
c                    Otherwise, the data is assumed to originate from a
c                    64-bit HDF data file, and is written with 14 digits
c                    to the text file.
c
c          SCALES  : [structure of type RP1D, dimension(3)]
c                    This array holds the pointers to the scales
c                    when SCALE=.true., and is not referenced
c                    otherwise.
c
c          F       : [real, pointer to a rank-3 array]
c                    This array holds the data set values.
c
c-----------------------------------------------------------------------
c
      use number_types
      use sds_def
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      character(*) :: fname
      type(sds) :: s
      integer :: ierr
      intent(in) :: fname,s
      intent(out) :: ierr
c
c-----------------------------------------------------------------------
c
c ****** Declarations for temporary variables.
c
      integer :: i,n
      character(32) :: fmt
c
c-----------------------------------------------------------------------
c
      ierr=0
c
c ****** Open the file for writing.
c
      call ffopen (1,fname,'rw',ierr)
c
      if (ierr.ne.0) then
        write (*,*)
        write (*,*) '### ERROR in WRTXT:'
        write (*,*) '### Could not open the text file for writing.'
        write (*,*) 'File name: ',trim(fname)
        ierr=1
        return
      end if
c
c ****** Check the number of dimensions.
c
      if (s%ndim.le.0.or.s%ndim.gt.3) then
        write (*,*)
        write (*,*) '### ERROR in WRTXT:'
        write (*,*) '### Could not write the SDS data.'
        write (*,*) 'Invalid number of dimensions.'
        write (*,*) 'NDIM = ',s%ndim
        write (*,*) 'File name: ',trim(fname)
        ierr=1
        return
      end if
c
c ****** Construct the format string for writing floating point
c ****** numbers to the output file.
c
      if (s%hdf32) then
        fmt='(5(1x,1pe13.6))'
      else
        fmt='(3(1x,1pe21.14))'
      end if
c
c ****** Write the number of dimensions.
c
      call wrint (1,1,s%ndim,ierr)
      if (ierr.ne.0) go to 900
c
c ****** Write the dimensions.
c
      do i=1,s%ndim
        call wrint (1,1,s%dims(i),ierr)
        if (ierr.ne.0) go to 900
      enddo
c
c ****** Write the scales.
c
      if (s%scale) then
        call wrint (1,1,1,ierr)
        if (ierr.ne.0) go to 900
        do i=1,s%ndim
          call wrfp (1,s%dims(i),s%scales(i)%f,fmt,ierr)
          if (ierr.ne.0) go to 900
        enddo
      else
        call wrint (1,1,0,ierr)
        if (ierr.ne.0) go to 900
      end if
c
c ****** Write the array.
c
      n=product(s%dims(1:s%ndim))
      call wrfp (1,n,s%f,fmt,ierr)
      if (ierr.ne.0) go to 900
c
      close (1)
c
      return
c
  900 continue
c
      write (*,*)
      write (*,*) '### ERROR in WRTXT:'
      write (*,*) '### Error in writing data to the text file.'
      write (*,*) 'File name: ',trim(fname)
      ierr=2
c
      return
      end
c#######################################################################
      subroutine wrint (iun,n,i,ierr)
c
c-----------------------------------------------------------------------
c
c ****** Write N words of INTEGER data from array I to the file
c ****** connected to unit IUN using a free format text write.
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      integer :: iun
      integer :: n
      integer :: i
      integer :: ierr
      intent(in) :: iun,n,i
      intent(out) :: ierr
c
c-----------------------------------------------------------------------
c
      ierr=0
c
      write (iun,*,err=100) i
c
      return
c
  100 continue
c
c ****** Error in writing the data.
c
      ierr=1
c
      return
      end
c#######################################################################
      subroutine wrfp (iun,n,f,fmt,ierr)
c
c-----------------------------------------------------------------------
c
c ****** Write N words of REAL data from array F to the file
c ****** connected to unit IUN.
c
c ****** FMT specifies the format string to use.
c
c-----------------------------------------------------------------------
c
      use number_types
c
c-----------------------------------------------------------------------
c
      implicit none
c
c-----------------------------------------------------------------------
c
      integer :: iun
      integer :: n
      real(r_typ), dimension(n) :: f
      character(*) :: fmt
      integer :: ierr
      intent(in) :: iun,n,f,fmt
      intent(out) :: ierr
c
c-----------------------------------------------------------------------
c
      ierr=0
c
      write (iun,fmt=fmt,err=100) f
c
      return
c
  100 continue
c
c ****** Error in writing the data.
c
      ierr=1
c
      return
      end
