#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

s, z, A, B, gnorm = np.loadtxt('geometry.dat', skiprows=1).T

plt.clf()

plt.subplot(2, 2, 1)
plt.plot(s, z)
plt.xlabel('s [$R_☉$]')
plt.ylabel('Altitude [$R_☉$]')

plt.subplot(2, 2, 2)
plt.plot(s, gnorm)
plt.xlabel('s [$R_☉$]')
plt.ylabel('Projected gravity [g0]')

plt.subplot(2, 2, 3)
plt.plot(s, B)
plt.yscale('log')
plt.xlabel('s [$R_☉$]')
plt.ylabel('Magnetic field [2.2 G]')

plt.subplot(2, 2, 4)
plt.plot(s, A)
plt.xlabel('s [$R_☉$]')
plt.ylabel('Relative loop area')

plt.tight_layout()
plt.savefig('geometry.pdf')
