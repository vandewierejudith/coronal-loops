#!/usr/bin/env python

import numpy as np

N_points = 10000
L = 367  # Mm
B0 = 1  # G
B1 = 10  # G
lB = L / 26  # Mm

s = np.linspace(0, L, N_points)

z = L / np.pi * np.sin(np.pi * s / L)
# gnorm = np.gradient(z, s)
gnorm = np.cos(np.pi * s / L)  # semi-circular loop analytical expression

B = B0 + B1 * (np.exp(-s/lB) + np.exp(-(L-s)/lB))
A = B[0] / B


LENGTH_UNIT = 696  # Mm
MAGNETIC_FIELD_UNIT = 2.2068908  # G
s /= LENGTH_UNIT
z /= LENGTH_UNIT
B /= MAGNETIC_FIELD_UNIT

geometry_data = np.stack([s, z, A, B, gnorm]).T

np.savetxt(
    'geometry.dat',
    geometry_data,
    fmt='%.10e',
    header='s   z   A   B   gnorm',
    )
